<!-- {#mainpage} -->
# Quickstart Guide

## Database matching tool: MSGFplus ##

## De Novo Sequencing algorithm: Novor ##

This is a pipeline to separate virus spectra from human and crap spectra.
First step is a MSGFplus run with all spectra. After that all unidentified 
spectra will be processed with a de novo algorithm.
The de novo sequences will be source for a tag generator. These tags will matched 
against multiple databases.  
The matching sequences are in silico modificated and mutated and matched again.


_This Image shows the current process of the implementation (green is already implemented). In red sections the PeptideMapper could be implemented (HartkopfF/DeNovo-Pipeline#46):_

![Workflow](https://gitlab.com/HartkopfF/Devil-Pipeline/raw/87d9f3aead9379f89939d32e6a110f681fcd76a1/flowchart/Denovo_Pipeline_20170125.png)