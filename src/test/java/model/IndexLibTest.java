package model;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.junit.Test;

import model.tag.TagIndex;
import util.Masses;
import util.Pair;

public class IndexLibTest {

	@Test
	public void testCreateIndex() throws IOException {
		ProteinLib proteinLib = new ProteinLib("res/fasta/crap.fasta");
		proteinLib.missedCleavages = 0;
		proteinLib.minLength = 0;
		proteinLib.digestProteins(); 

		int proteinId = 0, pepId = 0, tagId = 0;
		HashMap<Integer, String> proteinIndex = new HashMap<>();
		HashMap<Integer, Integer> pepId2protId = new HashMap<>();
		HashMap<Integer, Pair<Integer, Integer>> peptideIndex = new HashMap<>();
		HashMap<Integer, List<Integer>> tag2pepIndex = new HashMap<>();
		HashMap<String, Integer> tagIndex = new HashMap<>();

		// Pre-generate tag index with sequence length of 3 from an alphabet of 22 amino acids.
		Masses masses = Masses.getInstance();
		for (char a1 = 'A'; a1 <= 'Y'; a1++) {
			for (char a2 = 'A'; a2 <= 'Y'; a2++) {
				for (char a3 = 'A'; a3 <= 'Y'; a3++) {
					if (masses.containsKey(Character.toString(a1)) && masses.containsKey(Character.toString(a2)) && masses.containsKey(Character.toString(a3))) {
						tagIndex.put(Character.toString(a1) + Character.toString(a2) + Character.toString(a3), tagId++);
					}
				}
			}
		}

		// 22 x 22 x 22 --> 10648 potential tags
		assertEquals(10648, tagIndex.keySet().size());

		for (String acc : proteinLib.proteins.keySet()) {
			proteinIndex.put(++proteinId, acc);
			List<Integer> peptideIDs = proteinLib.getProtein(acc).getPeptides();
			for(int peptideID : peptideIDs) {
				List<Pair<Integer, Integer>> pairs = proteinLib.getPeptide(peptideID).getPeptide(acc);
				for (Pair<Integer, Integer> pair : pairs) {
					peptideIndex.put(++pepId, pair);
					// Peptide ID --> here unique for a particular protein.
					pepId2protId.put(pepId, proteinId);

					// Retrieve the protein sequence.
					String peptideSequence = proteinLib.pairToString(acc, pair);

					for(int i = 0; i < peptideSequence.length() - 3; i++) {
						String tag = peptideSequence.substring(i, i + 3);
						Integer index = tagIndex.get(tag);
						List<Integer> peptideIds = null;
						if (tag2pepIndex.get(index) == null) {
							peptideIds = new ArrayList<>();
						} else {
							peptideIds = tag2pepIndex.get(index);
						}
						peptideIds.add(pepId);
						tag2pepIndex.put(index, peptideIds);
					}
				}
			}
		}
		assertEquals(116, proteinIndex.size());


	}
	
	@Test
	public void testTagIndex() throws IOException {
		ProteinLib proteinLib = new ProteinLib("res/fasta/uniprot-human.fasta");
		proteinLib.missedCleavages = 0;
		proteinLib.minLength = 0;
		proteinLib.maxLength = 1000;
		proteinLib.digestProteins(); 
		
		TagIndex tagIndex = new TagIndex();
		tagIndex.buildIndex(proteinLib);
	}


}
