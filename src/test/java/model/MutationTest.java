package model;

import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import util.Pam;

import org.junit.Test;

import debugUtil.matrix;

/**
 * 
 * @author Felix Hartkopf
 *
 */

public class MutationTest {

	@Test
	public void testMutations() throws IOException { 
		Mutation mutation = new Mutation( Residue.Leu, Residue.Ile,3);
		Peptide peptide = new Peptide("PFRGAEEEDGIYRPSPAGTPLE");
		int charge = 2;
		
		//Test getSource()
		Residue tmpResidue = mutation.getSource();
		System.out.println("Test getSource(): "+ mutation.getSource()+" == Leucine => "+ tmpResidue.toString().contains("Leucine"));
		assertEquals(Residue.Leu, mutation.getSource());

		//Test getTarget()
		tmpResidue = mutation.getTarget();
		System.out.println("Test getTarget(): "+ mutation.getTarget()+" == Isoleucine => "+ tmpResidue.toString().contains("Isoleucine"));
		assertEquals(Residue.Ile, mutation.getTarget());

		//Test getMassShift()
		double tmpDouble = mutation.getMassShift();
		System.out.println("Test getMassShift(): "+ tmpDouble + "= 0 => "+ (tmpDouble == 0.0));
		assertEquals(0.0, mutation.getMassShift(), 0.0);

		//Test createMassShiftMatrix()
		double[][] massShiftMatrix = Mutation.createMassShiftMatrix(charge);
		matrix.printDoubleMatrix(massShiftMatrix);
		matrix.printIntegerMatrix(Pam.Pam250Matrix);
		
		//TestfindNearestMutation()
		Mutation closestMutation = Mutation.calcClosestMutation('A',massShiftMatrix,-66);
		System.out.println("Test findNearestMutation(): "+ Mutation.calcClosestMutation('A',massShiftMatrix,-66));
		assertEquals(Residue.Ala, closestMutation.getSource());
		assertEquals(Residue.Trp, closestMutation.getTarget());
		
		//Test getPossibleMutation()
		List<Mutation> mutations = Mutation.calcPossibleMutations(peptide.getSequence(), -15);
		System.out.println("Test getPossibleMutation(): "+ Mutation.calcPossibleMutations(peptide.getSequence(), -15));
		Mutation mutation1 = mutations.get(0);
		assertEquals(Residue.Ile, mutation1.getSource());
		assertEquals(Residue.Lys, mutation1.getTarget());

		Mutation mutation2 = mutations.get(mutations.size() - 1);
		assertEquals(Residue.Leu, mutation2.getSource());
		assertEquals(Residue.Lys, mutation2.getTarget());
	}
}
