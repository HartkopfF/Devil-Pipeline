package model;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import util.Pair;

public class ProteinLibTest {

	@Test
	public void testNoDub() throws IOException {
		ProteinLib proteinLib = new ProteinLib("res/fasta/human/human_reviewed.fasta");
		System.out.println("human_reviewed.fasta proteins: "+proteinLib.proteins.size());
		proteinLib.writeFasta("res/fasta/human/human_reviewed_noDub.fasta");

		ProteinLib proteinLibNoDup = new ProteinLib("res/fasta/human/human_reviewed_noDup.fasta");
		System.out.println("human_reviewed.fasta proteins no dublicates: "+proteinLibNoDup.proteins.size());

	}

	@Test
	public void test() throws IOException {
		ProteinLib proteinLib = new ProteinLib("res/fasta/ProteinLibTest.fasta");
		proteinLib.missedCleavages = 0;
		proteinLib.minLength = 0;
		proteinLib.digestProteins(); 
		System.out.println("ProteinLibTest class L�nge: "+proteinLib.proteins.size());
		Set<String> accessions = proteinLib.getAllAccessions();
		List<String> control =  Arrays.asList("CCDK", "PLLEK", "SHCIAEVEK", "DAIPENLPPLTADFAEDK", "DVCK", "NYQEAK", "DAFL");
		int index = 0;

		for(String accession :  accessions){
			List<Integer> peptideIDs = proteinLib.getProtein(accession).getPeptides();
			String sequence = proteinLib.getProtein(accession).getSequence();
			for(int peptideID : peptideIDs) {
				List<Pair<Integer,Integer>> pairList = proteinLib.getPeptide(peptideID).getPeptide(accession);		
				for(Pair<Integer, Integer> pair : pairList) {
					System.out.println(sequence.substring(pair.getLeft(), pair.getRight()));
					assertEquals(sequence.substring(pair.getLeft(), pair.getRight()),control.get(index));
					index++;
				}
			}		
		}
	}

	@Test
	public void testMissingSequence() throws IOException {
		ProteinLib proteinLib = new ProteinLib("res/fasta/crap.fasta");
		System.out.println("TestNoDub class L�nge: "+proteinLib.proteins.size());
		proteinLib.writeFasta("res/fasta/crap_noDub.fasta");

		ProteinLib proteinLibNoDup = new ProteinLib("res/fasta/human/human_reviewed_noDup.fasta");
		System.out.println("TestNoDub class L�nge: "+proteinLibNoDup.proteins.size());

		Set<String> accessions = proteinLibNoDup.getAllAccessions();
		for(String accession : accessions){
			if(proteinLibNoDup.getProtein(accession).getSequence()==null) {
				System.out.println(accession);
			}
		}

	}
}
