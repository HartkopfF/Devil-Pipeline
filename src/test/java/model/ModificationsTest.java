package model;

import java.io.IOException;
import org.junit.Test;


/**
 * 
 * @author Felix Hartkopf
 *
 */

public class ModificationsTest {

	@Test
	public void testMutations() throws IOException { 
		for(double i = 0.0; i<3000 ;i=i+0.1){
			Modification mod = Modification.massShiftToModification(i);
			System.out.println("####################################"); 
			System.out.println(i);
			System.out.println(mod.getShortName()); 
			System.out.println(mod.getAvgMass()); 
			System.out.println(mod.getDescription()); 
		}

	}
}
