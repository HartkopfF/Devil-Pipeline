package model.tag;

import static org.junit.Assert.*;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import model.Modification;
import model.Mutation;
import model.Residue;
import model.tag.Tag.TagType;
import util.StringUtilities;

public class TagSerializerTest {

	@SuppressWarnings("unchecked")
	@Test
	public void test() {

		// start record time
		long startTime = System.currentTimeMillis();

		// Init database with MapDB
		File DBfile = new File("testDB.db");
		DBfile.delete();



		// Create Heap in DB
		Serializer<Tag> serializer = new TagSerializer();

		DB db = DBMaker.fileDB(DBfile)//heapDB()//
				//.fileMmapEnable()
				//.fileMmapPreclearDisable()
				.allocateStartSize( 10 * 1024*1024*1024)  // 10GB
				.allocateIncrement(1 * 1024*1024)       // 512MB
				//.transactionEnable()
				.fileChannelEnable()
				.make();

		Map<Integer, Tag> tagList = (Map<Integer,Tag>) db.hashMap("map").valueSerializer(serializer).createOrOpen();

		//"TKMKGVCEVGVQAHK"
		String sequence = "TYL";
		int charge = 2;
		double tagMass = 377.195063;
		double leftMass = 250.095349;
		double rightMass = 2236.16445399999;
		double aaScore = 9.333333333;
		double mz = 1441.738092;
		Tag tag = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tag.setSpectrumId(302);
		tag.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tag.setTagType(TagType.MIDDLE);
		tag.matchedPeptides.add(12234);
		tag.matchedPeptides.add(121234);
		tag.matchedPeptides.add(-14);
		tag.matchedPeptides.add(12);
		tag.matchedPeptides.add(134);
		tag.matchedPeptides.add(-1224);
		tag.matchedPeptides.add(15);
		
		double modMass = 0;
		Modification mod = Modification.massShiftToModification(modMass);
		tag.setLeftModifications(0, mod);
		tag.setRightModifications(0, mod);
		tag.setLeftModifications(1, mod);
		tag.setRightModifications(1, mod);
		
		System.out.println("######################################");

		System.out.println(mod.getShortName());
		System.out.println(mod.getDescription());
		System.out.println(mod.getMonoMass());
		System.out.println(mod.getAvgMass());
		System.out.println(mod.getN15());
		System.out.println(mod.getType());
		System.out.println(mod.getAminoAcid());
		System.out.println(mod.getPsiMs());
		System.out.println("#####################################");


		Residue source = Residue.valueOf(Residue.convertOneToThree('S'));
		Residue target = Residue.valueOf(Residue.convertOneToThree('T'));
		int mutationIndex = 69;
		double declaredMassShift = 666.0;
		Mutation mutation = new Mutation( source,  target,  mutationIndex,  declaredMassShift);
		List<Mutation> mutations = new ArrayList<Mutation>();
		mutations.add(mutation);
		tag.setRightMutations(0, mutations);
		tag.setLeftMutations(0, mutations);
		tag.setRightMutations(1, mutations);
		tag.setLeftMutations(1, mutations);

		for(int i = 0; i < 1; i++) {
			tagList.put(i, tag);
		}

		System.out.println("Tags: "+tagList.size());
		System.out.println("Amount of left modifications: "+tag.getAllLeftModifications().size());
		System.out.println("Amount of right modifications: "+tag.getAllRightModifications().size());

		assertEquals(tagList.get(0).getSequence(),sequence);
		assertEquals(tagList.get(0).getTagMass(),tagMass,0);
		assertEquals(tagList.get(0).getLeftMass(),leftMass,0);
		assertEquals(tagList.get(0).getRightMass(),rightMass,0);
		assertEquals(tagList.get(0).getScore(),aaScore,0);
		assertEquals(tagList.get(0).getCharge(),charge);
		assertEquals(tagList.get(0).getPepMass(),mz,0);
		assertEquals(tagList.get(0).matchedPeptides.size(), 7);
		
		Modification leftMod = tagList.get(0).getLeftModifications(0);
		assertEquals(leftMod.getAminoAcid(), mod.getAminoAcid());
		assertEquals(leftMod.getAvgMass(), mod.getAvgMass(),0);
		assertEquals(leftMod.getDescription(), mod.getDescription());
		assertEquals(leftMod.getMonoMass(), mod.getMonoMass(),0);
		assertEquals(leftMod.getN15(), mod.getN15());
		assertEquals(leftMod.getShortName(), mod.getShortName());
		assertEquals(leftMod.getType(), mod.getType());
		assertEquals(leftMod.getUnimod(), mod.getUnimod());
		
		Modification rightMod = tagList.get(0).getRightModifications(0);
		assertEquals(rightMod.getAminoAcid(), mod.getAminoAcid());
		assertEquals(rightMod.getAvgMass(), mod.getAvgMass(),0);
		assertEquals(rightMod.getDescription(), mod.getDescription());
		assertEquals(rightMod.getMonoMass(), mod.getMonoMass(),0);
		assertEquals(rightMod.getN15(), mod.getN15());
		assertEquals(rightMod.getShortName(), mod.getShortName());
		assertEquals(rightMod.getType(), mod.getType());
		assertEquals(rightMod.getUnimod(), mod.getUnimod());

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time elapsed: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");

		long total = Runtime.getRuntime().totalMemory();
		long used  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Total memory: "+total/1024/1024+" MB");
		System.out.println("Used memory: "+used/1024/1024+" MB");

		db.close();

	}


}
