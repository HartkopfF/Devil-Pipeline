package model.tag;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import java.util.List;
import org.junit.Test;
import model.denovo.DeNovoHit;
import model.denovo.NovorHit;

public class TagGeneratorTest {

	@Test
	public void testGenerateTagsWithLength3() {
		DeNovoHit hit = new NovorHit(1, 0, 5969.6, 834.9901, 2, 1667.9644, 0.0013, 0.8, 49.8, "AVKQPDGLAVLMGLLK", "39-63-75-99-66-72-12-4-53-70-98-32-44-38-2-21");
		// Tag of length 3
		TagGenerator tagGenerator = new TagGenerator(hit, 3, 3);
		List<Tag> tags = tagGenerator.getTags();
		assertNotNull(tags);
		Tag firstTag = tags.get(0);
		assertEquals("AVK", firstTag.getSequence());
		Tag lastTag = tags.get(tags.size() - 1);
		assertEquals("LLK", lastTag.getSequence());
	}

	@Test
	public void testGenerateTagsWithLength4() {
		DeNovoHit hit = new NovorHit(1, 0, 5969.6, 834.9901, 2, 1667.9644, 0.0013, 0.8, 49.8, "AVKQPDGLAVLMGLLK", "39-63-75-99-66-72-12-4-53-70-98-32-44-38-2-21");
		// Tag of length 4
		TagGenerator tagGenerator = new TagGenerator(hit, 4, 4);
		List<Tag> tags = tagGenerator.getTags();
		assertNotNull(tags);
		Tag firstTag = tags.get(0);
		assertEquals("AVKQ", firstTag.getSequence());
		Tag lastTag = tags.get(tags.size() - 1);
		assertEquals("GLLK", lastTag.getSequence());
	}

	@Test
	public void testGenerateTagsWithLength5() {
		DeNovoHit hit = new NovorHit(1, 0, 5969.6, 834.9901, 2, 1667.9644, 0.0013, 0.8, 49.8, "AVKQPDGLAVLMGLLK", "39-63-75-99-66-72-12-4-53-70-98-32-44-38-2-21");
		// Tag of length 5
		TagGenerator tagGenerator = new TagGenerator(hit, 5, 5);
		List<Tag> tags = tagGenerator.getTags();
		assertNotNull(tags);
		Tag firstTag = tags.get(0);
		assertEquals("AVKQP", firstTag.getSequence());
		Tag lastTag = tags.get(tags.size() - 1);
		assertEquals("MGLLK", lastTag.getSequence());
	}

	@Test
	public void testGenerateTagsWithLength6() {
		DeNovoHit hit = new NovorHit(1, 0, 5969.6, 834.9901, 2, 1667.9644, 0.0013, 0.8, 49.8, "AVKQPDGLAVLMGLLK", "39-63-75-99-66-72-12-4-53-70-98-32-44-38-2-21");
		// Tag of length 6
		TagGenerator tagGenerator = new TagGenerator(hit, 6, 6);
		List<Tag> tags = tagGenerator.getTags();
		assertNotNull(tags);
		Tag firstTag = tags.get(0);
		assertEquals("AVKQPD", firstTag.getSequence());
		Tag lastTag = tags.get(tags.size() - 1);
		assertEquals("LMGLLK", lastTag.getSequence());
	}
}
