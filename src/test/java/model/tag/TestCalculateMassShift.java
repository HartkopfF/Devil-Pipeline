package model.tag;


import org.junit.Test;

import model.Peptide;
import model.tag.Tag.TagType;
import util.Pair;

public class TestCalculateMassShift {

	@Test
	public void test() {
		String sequence = "TYL";
		int charge = 2;
		double tagMass = 377.195063;
		double leftMass = 250.095349;
		double rightMass = 2236.16445399999;
		double aaScore = 9.333333333;
		double mz = 1441.738092;

		Tag tagA = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tagA.setSpectrumId(302);
		tagA.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tagA.setTagType(TagType.MIDDLE);	

		Peptide peptide = new Peptide("SYTYLMLPELVMMVLPELMLPWGR");
		Pair<Double, Double> massShift = peptide.calculateMassShift(tagA);
		System.out.println(peptide.calculateMassShift(tagA));
		double absError = Math.abs(massShift.getLeft())+Math.abs(massShift.getRight());
		System.out.println("absError:\t"+absError);

	}
}
