package model.tag;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import model.ProteinLib;
import model.denovo.NovorHit;
import util.StringUtilities;

public class TestTagMatching {

	@SuppressWarnings("unchecked")
	@Test
	public void test() throws StringIndexOutOfBoundsException, IOException, InterruptedException {
		long startTime = System.currentTimeMillis();

		String output = "original_ms2pip_CID-mod";
		String primaryVirus = "res/spectra/SimulatedSpectra/cRAP/original_ms2pip_CID.mgf";

		// Import Novor results
		Map<Integer, NovorHit> novorReader = NovorHit.readNovorHitsFromFile("output/Novor/"+output+".csv",primaryVirus);
		
		// Init database with MapDB
		File DBfile = new File("spectraDB.db");
		DBfile.delete();
		DB db;

		// Create Heap in DB
		Serializer<Tag> serializer = new TagSerializer();
		db = DBMaker.heapDB().make();
		
		// Generate Tags
		HTreeMap<Integer, Tag> tags = (HTreeMap<Integer,Tag>) db.hashMap("tags_raw").valueSerializer(serializer).createOrOpen();
		TagGenerator.generateTagsForList(novorReader,tags, db);
		System.out.println(tags.toString());
		System.out.println(tags.size());


		String fastaPath = "res/fasta/crap_bovin.fasta";
		System.out.println(tags.size());

		// Read fasta database
		ProteinLib proteinLib = new ProteinLib(fastaPath);
		proteinLib.missedCleavages = 1;
		proteinLib.minLength = 3;
		proteinLib.digestProteins(); 
		fastaPath = fastaPath.replace(".fasta","_noDup.fasta");
		double tolerance = 0.01;
		double meanAaScore = 0.0;
		double maxAbsError = 1.0;
		int cores = 4;

	

		// Build tag index
		HTreeMap<Integer, List<Integer>> tagIndex = (HTreeMap<Integer, List<Integer>>) db.hashMap("index").createOrOpen();
		TagIndex tagIndexer = new TagIndex();
		tagIndexer.buildIndex(db, tagIndex, proteinLib, cores);

		Map<Integer, Tag> tagList = (Map<Integer,Tag>) db.hashMap("virus").valueSerializer(serializer).createOrOpen();

		Tag.matchTagsToProteins(db, tagList,tags,proteinLib, tagIndex, tolerance, meanAaScore, 1);

		HTreeMap<Integer, Tag> perfectMatches = (HTreeMap<Integer,Tag>) db.hashMap("perfectMatches").valueSerializer(serializer).createOrOpen();
		HTreeMap<Integer, Tag> weakMatches = (HTreeMap<Integer,Tag>) db.hashMap("weakMatches").valueSerializer(serializer).createOrOpen();
		Tag.extractPerfectMatches(tagList, proteinLib, db, maxAbsError, perfectMatches, weakMatches, cores);
		db.commit();	

		// Pefect matches should be the amount of tags
		System.out.println("Amount of perfect matches: "+perfectMatches.size());
		System.out.println("Amount of weak matches: "+weakMatches.size());
		System.out.println("Amount of tags: "+tagList.size());

		for(Integer key : tagList.keySet()) {
			Tag tag = tagList.get(key);
			System.out.println(key+"\t"+tag.getSequence()+"\t"+tag.getType()+"\t"+tag.getLeftMass()+"\t"+tag.getRightMass());


		}

		/*List<ProteinHit> proteins = Tag.linkTagsToProteins(tagList,null, proteinLib);		
		ProteinHit.proteinListCoverage(proteins);

		for(ProteinHit protein : proteins) {
			System.out.println(protein.toString());

		}*/

		// Test if the amount of tags is used as input as tags that been matched
		assertEquals(tags.size(),tagList.size());

		// Test if all tags are perfect matches
		assertEquals(perfectMatches.size(),tagList.size());

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time elapsed: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");

		long total = Runtime.getRuntime().totalMemory();
		long used  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Total memory: "+total/1024/1024+" MB");
		System.out.println("Used memory: "+used/1024/1024+" MB");

	}


	@SuppressWarnings("unchecked")
	@Test
	public void testMulti() throws StringIndexOutOfBoundsException, IOException, InterruptedException {
		long startTime = System.currentTimeMillis();

		String output = "original_ms2pip_CID-mod";
		String primaryVirus = "res/spectra/SimulatedSpectra/cRAP/original_ms2pip_CID.mgf";
		int cores = 4;

		// Import Novor results
		Map<Integer, NovorHit> novorReader = NovorHit.readNovorHitsFromFile("output/Novor/"+output+".csv",primaryVirus);

		// Init database with MapDB
		File DBfile = new File("spectraDB.db");
		DBfile.delete();
		DB db;

		// Create Heap in DB
		Serializer<Tag> serializer = new TagSerializer();
		db = DBMaker.heapDB().make();
		
		// Generate Tags
		HTreeMap<Integer, Tag> tags = (HTreeMap<Integer,Tag>) db.hashMap("tags_raw").valueSerializer(serializer).createOrOpen();
		TagGenerator.generateTagsForList(novorReader,tags, db);
		System.out.println(tags.toString());
		System.out.println(tags.size());


		String fastaPath = "res/fasta/crap_bovin.fasta";
		System.out.println(tags.size());

		// Read fasta database
		ProteinLib proteinLib = new ProteinLib(fastaPath);
		proteinLib.missedCleavages = 1;
		proteinLib.minLength = 3;
		proteinLib.digestProteins(); 
		fastaPath = fastaPath.replace(".fasta","_noDup.fasta");
		double tolerance = 0.01;
		double meanAaScore = 0.0;
		double maxAbsError = 1.0;

		// Build tag index
		HTreeMap<Integer, List<Integer>> tagIndex = (HTreeMap<Integer, List<Integer>>) db.hashMap("index").createOrOpen();
		TagIndex tagIndexer = new TagIndex();
		tagIndexer.buildIndex(db, tagIndex, proteinLib, cores);

		Map<Integer, Tag> tagList = (Map<Integer,Tag>) db.hashMap("virus").valueSerializer(serializer).createOrOpen();

		Tag.matchTagsToProteins(db, tagList,tags,proteinLib, tagIndex, tolerance, meanAaScore, cores);

		HTreeMap<Integer, Tag> perfectMatches = (HTreeMap<Integer,Tag>) db.hashMap("perfectMatches").valueSerializer(serializer).createOrOpen();
		HTreeMap<Integer, Tag> weakMatches = (HTreeMap<Integer,Tag>) db.hashMap("weakMatches").valueSerializer(serializer).createOrOpen();
		Tag.extractPerfectMatches(tagList, proteinLib, db, maxAbsError, perfectMatches, weakMatches, cores);
		db.commit();


		// Pefect matches should be the amount of tags
		System.out.println("Amount of perfect matches: "+perfectMatches.size());
		System.out.println("Amount of weak matches: "+weakMatches.size());
		System.out.println("Amount of tags: "+tagList.size());

		for(Integer key : tagList.keySet()) {
			Tag tag = tagList.get(key);
			System.out.println(key+"\t"+tag.getSequence()+"\t"+tag.getType()+"\t"+tag.getLeftMass()+"\t"+tag.getRightMass());
		}

		//List<ProteinHit> proteins = Tag.linkTagsToProteins(tagList,null, proteinLib);		
		//ProteinHit.proteinListCoverage(proteins);

		//for(ProteinHit protein : proteins) {
		//	System.out.println(protein.toString());

		//}

		// Test if the amount of tags is used as input as tags that been matched
		assertEquals(tags.size(),tagList.size());

		// Test if all tags are perfect matches
		assertEquals(perfectMatches.size(),tagList.size());


		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time elapsed: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");

		long total = Runtime.getRuntime().totalMemory();
		long used  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Total memory: "+total/1024/1024+" MB");
		System.out.println("Used memory: "+used/1024/1024+" MB");

	}

}
