package model.tag;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mapdb.DB;

import model.tag.Tag.TagType;

public class TestMergeTags {

	@Test
	public void test() {
		String sequence = "TYL";
		int charge = 2;
		double tagMass = 377.195063;
		double leftMass = 250.095349;
		double rightMass = 2236.16445399999;
		double aaScore = 9.333333333;
		double mz = 1441.738092;

		Tag tagA = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tagA.setSpectrumId(302);
		tagA.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tagA.setTagType(TagType.MIDDLE);

		Tag tagB = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tagB.setSpectrumId(302);
		tagB.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tagB.setTagType(TagType.MIDDLE);

		Tag tagC = Tag.mergeTags(tagA, tagB);

		assertEquals(tagC,tagA);
		assertEquals(tagC,tagB);

	}


	@Test
	public void testMulti() throws InterruptedException {
		String sequence = "TYL";
		int charge = 2;
		double tagMass = 377.195063;
		double leftMass = 250.095349;
		double rightMass = 2236.16445399999;
		double aaScore = 9.333333333;
		double mz = 1441.738092;

		Tag tagA = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tagA.setSpectrumId(302);
		tagA.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tagA.setTagType(TagType.MIDDLE);


		Tag tagB = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tagB.setSpectrumId(302);
		tagB.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tagB.setTagType(TagType.MIDDLE);



		sequence = "LIV";
		charge = 2;
		tagMass = 477.195063;
		leftMass = 350.095349;
		rightMass = 1236.16445399999;
		aaScore = 10.333333333;
		mz = 1441.738092;

		Tag tagC = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tagC.setSpectrumId(301);
		tagC.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tagC.setTagType(TagType.MIDDLE);


		Tag tagD = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tagD.setSpectrumId(301);
		tagD.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tagD.setTagType(TagType.MIDDLE);


		Map<Integer,Tag> mapA = new HashMap<Integer,Tag>();
		mapA.put(302, tagA);
		mapA.put(301, tagC);

		Map<Integer,Tag> mapB = new HashMap<Integer,Tag>();
		mapB.put(302, tagB);
		mapB.put(301, tagD);

		DB db = null;
		@SuppressWarnings("unused")
		Map<Integer, Tag> mapC = Tag.mergeTagMaps(db, mapA, mapB, 2);

	}
}
