package debugUtil;


import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.junit.Test;

import util.ResourcesConstants;
import util.MSGFplusProcess;

public class MSGFplusTest {

	@Test
	public void test() throws IOException {
		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("devil.config");
		config.load(file);

		// MSGFplus
		String MSGFPlusPath = config.getProperty("MSGFPlus");

		MSGFplusProcess Testing = new MSGFplusProcess("baum.fasta",ResourcesConstants.CPXV,MSGFPlusPath,"CPXV");

		Testing.run();
	}

}
