package debugUtil;

import java.io.File;
import java.util.Map;

import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.Serializer;

import model.tag.Tag;
import model.tag.TagSerializer;
import model.tag.Tag.TagType;


public class MapDB {

	@Test
	public void test() {

		// Init database with MapDB
		File DBfile = new File("test.db");
		DBfile.delete();
		DB db;

		// Create Heap in DB
		Serializer<Tag> serializer = new TagSerializer();

		db = DBMaker.fileDB(DBfile)
				//.allocateStartSize( 10 * 1024*1024*1024)  // 10GB
				//.allocateIncrement(512 * 1024*1024)       // 512MB   
				.fileChannelEnable()
				.make();

		@SuppressWarnings("unchecked")
		Map<Integer, Tag> tagList = (Map<Integer,Tag>) db.hashMap("virus").valueSerializer(serializer).createOrOpen();
		//"TKMKGVCEVGVQAHK"
		String sequence = "TYL";
		int charge = 2;
		double tagMass = 377.195063;
		double leftMass = 250.095349;
		double rightMass = 2236.16445399999;
		double aaScore = 9.333333333;
		double mz = 1441.738092;
		Tag tag = new Tag(sequence, charge, tagMass, leftMass, rightMass, aaScore, mz);
		tag.setSpectrumId(302);
		tag.setDenovoSequence("SYTYLMLPELVMMVLPELMLPWGR");;
		tag.setTagType(TagType.MIDDLE);
		tag.matchedPeptides.add(12234);
		tag.matchedPeptides.add(121234);
		tag.matchedPeptides.add(14);
		tag.matchedPeptides.add(12);
		tag.matchedPeptides.add(134);
		tag.matchedPeptides.add(1224);
		tag.matchedPeptides.add(15);

		for(int i = 0; i < 1000000000; i++) {
			System.out.println(i);
			tagList.put(i, tag);
			if(i%100==0) {
				db.commit();

			}
		}
	}

}
