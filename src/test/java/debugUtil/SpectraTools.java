package debugUtil;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.Test;

import model.Peptide;

public class SpectraTools {

	@Test
	public void addMassToMS2PIP() throws FileNotFoundException, IOException {
		String path = "C:/Users/Bready/Desktop/A2SZX3_RVFV_ms2pip_HCD.mgf";
		File file = new File(path);
		int scans = 1;
		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String line;
			while ((line = br.readLine()) != null) {
				if(line.length()>0 ){
					if(line.substring(0, 5).contains("TITLE")){
						String pattern = "-[A-Z]*-";
						Pattern r = Pattern.compile(pattern);
						Matcher m = r.matcher(line);
						if (m.find( )) {
							String sequence = m.group(0).replaceAll("-", "");
							Peptide peptide = new Peptide(sequence);
							double mass = peptide.calculatePrecursorMass(2);
							System.out.println(line+" scans: "+scans);
							System.out.println("PEPMASS=" + mass);
							System.out.println("SCANS=" + scans);
							scans = scans+1;
						}
					}else if(line.substring(0, 6).contains("CHARGE")){
						System.out.println("CHARGE=2+");
					}
					else{
						System.out.println(line);
					}
				}else{
					System.out.println(line);
				}
			}
		}
	}

}
