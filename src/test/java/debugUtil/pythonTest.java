package debugUtil;


import org.junit.Test;

import python.PythonProcess;
import util.ResourcesConstants;

public class pythonTest {

	@Test
	public void test() {

		PythonProcess python = new PythonProcess(ResourcesConstants.PLOT_SUMMARY);
		python.run();

	}

	@Test
	public void plotTags() {

		PythonProcess python = new PythonProcess(ResourcesConstants.PLOT_TAGS);
		python.run();

	}

}
