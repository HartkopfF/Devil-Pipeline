package debugUtil;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.junit.Test;

import util.NovorProcess;

public class NovorTest {

	@Test
	public void test() throws IOException {
		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("devil.config");
		config.load(file);
		String NovorPath = config.getProperty("Novor");
		String primaryVirus = "res/spectra/SimulatedSpectra/cRAP/original_ms2pip_CID.mgf";
		
		// Execute Novor
		NovorProcess novorProcess = new NovorProcess(primaryVirus,NovorPath+"/lib","original_ms2pip_CID",NovorPath+"/params.txt");
		novorProcess.run();
	}

}
