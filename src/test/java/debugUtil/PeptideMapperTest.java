package debugUtil;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import org.junit.Test;

import com.compomics.util.experiment.biology.AminoAcidSequence;
import com.compomics.util.experiment.identification.amino_acid_tags.Tag;
import com.compomics.util.experiment.identification.identification_parameters.SearchParameters;
import com.compomics.util.experiment.identification.protein_inference.PeptideProteinMapping;
import com.compomics.util.experiment.identification.protein_inference.fm_index.FMIndex;
import com.compomics.util.experiment.identification.protein_sequences.SequenceFactory;
import com.compomics.util.preferences.PeptideVariantsPreferences;
import com.compomics.util.preferences.SequenceMatchingPreferences;
import com.compomics.util.waiting.WaitingHandler;

import util.ResourcesConstants;

public class PeptideMapperTest {


	@Test
	public void test() throws ClassNotFoundException, IOException, InterruptedException, SQLException{

		WaitingHandler waitingHandler = null;
		PeptideVariantsPreferences ptmSettings = new PeptideVariantsPreferences();
		SearchParameters peptideVariantsPreferences = new SearchParameters();
		peptideVariantsPreferences.setDefaultAdvancedSettings();
		AminoAcidSequence sequenceSeq = new AminoAcidSequence("FIS");
		Tag tag = new Tag(386.19545, sequenceSeq, 1250.702302);
		Double tolerance = 0.01;
		SequenceMatchingPreferences sequenceMatchingPreferences = SequenceMatchingPreferences.getStringMatching();
		File fasta = new File(ResourcesConstants.FASTA_CRAP);
		String sequence = "WVTFISLLLLFSSAYSR";

		// Parse the FASTA file
		SequenceFactory sequenceFactory = SequenceFactory.getInstance();
		sequenceFactory.loadFastaFile(fasta);

		// Index the protein sequences
		FMIndex peptideMapper = new FMIndex(waitingHandler, true, ptmSettings, peptideVariantsPreferences);

		// Map a peptide sequence to the protein sequences
		ArrayList<PeptideProteinMapping> proteinMappingPeptide = peptideMapper.getProteinMapping(sequence, sequenceMatchingPreferences);
		System.out.println("Sequence: "+proteinMappingPeptide.get(0).getPeptideSequence());
		System.out.println("Accession: "+proteinMappingPeptide.get(0).getProteinAccession());
		System.out.println("Index: "+proteinMappingPeptide.get(0).getIndex());

		// Map a sequence tag to the protein sequences		
		ArrayList<PeptideProteinMapping> proteinMappingTag = peptideMapper.getProteinMapping(tag, null, sequenceMatchingPreferences, tolerance);
		System.out.println("Sequence: "+proteinMappingTag.get(0).getPeptideSequence());
		System.out.println("Accession: "+proteinMappingTag.get(0).getProteinAccession());
		System.out.println("Index: "+proteinMappingTag.get(0).getIndex());
		System.out.println("Index: "+proteinMappingTag.get(0).getIndex());
		System.out.println("Index: "+proteinMappingTag.get(0).getIndex());

	}
}
