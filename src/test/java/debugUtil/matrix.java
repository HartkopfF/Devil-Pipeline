package debugUtil;

import util.Formatter;

public class matrix {

	/**
	 * print 2d double matrix
	 * @param matrix
	 */

	public static void printDoubleMatrix(double matrix[][]){
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.print(Formatter.roundBigDecimalDouble(matrix[i][j],2) + "	");
			}
			System.out.print("\n");
		}
	}

	/**
	 * print 2d integer matrix
	 * @param matrix
	 */

	public static void printIntegerMatrix(int matrix[][]){
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				System.out.print(matrix[i][j]+ "	");
			}
			System.out.print("\n");
		}
	}
}
