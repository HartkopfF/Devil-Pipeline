package debugUtil;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;
import java.io.*;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

/*
 *	Demonstrates HashMaps with non-standard types of objects as key or value.
 */
public class Custom_Value {


	/**
	 * MapDB uses custom serialization which stores class metadata at single place.
	 * Thanks to it is 10x more efficient than standard Java serialization.
	 *
	 * Using custom values in MapDB has three conditions:
	 *
	 *   1)  classes should be immutable. There is instance cache, background serialization etc
	 *         Modifing your classes after they were inserted into MapDB may leed to unexpected things.
	 *
	 *   2) You should implement `Serializable` marker interface. MapDB tries to stay compatible
	 *         with standard Java serialization.
	 *
	 *   3) Even your values should implement equalsTo method for CAS (compare-and-swap) operations.
	 *
	 */
	public static class Person implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 1696690097095701237L;
		final String name;
		final String city;
		public TreeMap<String, Integer> trees;

		public Person(String n, String c,TreeMap<String, Integer> trees){
			super();
			this.name = n;
			this.city = c;
			this.trees = trees;
		}

		public String getName() {
			return name;
		}

		public String getCity() {
			return city;
		}

		public TreeMap<String, Integer> getTrees() {
			return trees;
		}

		@Override
		public boolean equals(Object o) {
			if (this == o) return true;
			if (o == null || getClass() != o.getClass()) return false;

			Person person = (Person) o;

			if (city != null ? !city.equals(person.city) : person.city != null) return false;
			if (name != null ? !name.equals(person.name) : person.name != null) return false;

			return true;
		}

	}

	public static void main(String[] args) {

		// Open db in temp directory
		File f = new File("C:/Users/hartkopff/Desktop/Beispiel.db");
		f.delete();
		DB db = DBMaker.fileDB(f)
				.make();

		// Open or create table
		@SuppressWarnings("unchecked")
		Map<String,Person> dbMap = (Map<String, Person>) db.treeMap("personAndCity").keySerializer(Serializer.STRING).create();


		// Add data
		TreeMap<String, Integer> trees = new TreeMap<String, Integer>();
		trees.put("Tanne",100);
		trees.put("Fichte",200);
		trees.put("Eiche",50);

		Person bilbo = new Person("Bilbo","The Shire",trees);
		Person sauron = new Person("Sauron","Mordor",trees);
		Person radagast = new Person("Radagast","Crazy Farm",trees);

		dbMap.put("west",bilbo);
		dbMap.put("south",sauron);
		dbMap.put("mid",radagast);
		System.out.println(dbMap.size());

		// Commit and close
		db.commit();
		System.out.println(dbMap.get("mid").getCity());
		db.close();
		System.out.println("Point 0");


		// Second option for using cystom values is to use your own serializer.
		// This usually leads to better performance as MapDB does not have to
		// analyze the class structure.
		//

		class CustomSerializer implements Serializer<Person>, Serializable{

			/**
			 * 
			 */
			private static final long serialVersionUID = 1L;

			@Override
			public void serialize(DataOutput2 out, Person value) throws IOException {
				System.out.println("Serialize...");
				out.writeUTF(value.getName());
				out.writeUTF(value.getCity());

				for (Entry<String, Integer> entry : value.getTrees().entrySet()) {
					Integer number = entry.getValue();
					String name = entry.getKey();
					out.writeUTF(name);				
					out.writeInt(number);
				}
				out.writeUTF("End_trees");

			}

			@Override
			public Person deserialize(DataInput2 input, int available) throws IOException {
				String name = input.readUTF();
				String location = input.readUTF();

				TreeMap<String, Integer> trees = new TreeMap<String, Integer>();
				String line = input.readUTF();

				while(!line.equals("End_trees")) {					
					trees.put(line, input.readInt());
					line = input.readUTF();
				}

				return new Person(name, location,trees);
			}
		}


		System.out.println("Point 1");

		Serializer<Person> serializer = new CustomSerializer();

		DB db2 = DBMaker.tempFileDB().make();

		@SuppressWarnings({ "unchecked", "deprecation" })
		Map<String,Person> map2 = (Map<String, Person>) db2.hashMap("map").valueSerializer(serializer).make();

		trees = new TreeMap<String, Integer>();
		trees.put("Tanne",100);
		trees.put("Fichte",200);
		trees.put("Eiche",50);
		map2.put("North", new Person("Yet another dwarf","Somewhere",trees));


		System.out.println(map2.size());

		System.out.println(map2.get("North").getCity());
		System.out.println(map2.get("North").getTrees().get("Eiche"));
		System.out.println(map2.get("North").getTrees().get("Tanne"));
		System.out.println(map2.get("North").getTrees().get("Fichte"));
		System.out.println(map2.get("North").getName());

		db2.close();


	}


}
