package debugUtil;


import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Properties;

import org.junit.Test;


public class PropertiesTest {

	@Test
	public void test(){
		Properties config = new Properties();
		try {
			Reader file = new FileReader("devil.config");
			config.load(file);

		} catch (IOException e) {
			System.out.println("Can�t find parameters file devil.config.");
			e.printStackTrace();
		}
	}

}
