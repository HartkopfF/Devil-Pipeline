package execute;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import model.Modification;
import model.Mutation;
import model.ProteinLib;
import model.denovo.NovorHit;
import model.hit.ProteinHit;
import model.tag.Tag;
import model.tag.TagGenerator;
import model.tag.TagIndex;
import model.tag.TagSerializer;
import util.NovorProcess;
import util.StringUtilities;

public class DeNovoTerminalTest {

	@SuppressWarnings("unchecked")
	@Test
	public void testDeNovoTerminal() throws IOException, StringIndexOutOfBoundsException, InterruptedException {
		//////////////////////
		// Load config file //
		//////////////////////

		// Read terminal input
		String mgfFileName = "res/spectra/SimulatedSpectra/P03518-GP_RVFV/P03518-3spectra_HCD.mgf";
		String fastaDataBase = "res/fasta/rvfv/rvfv_P03518.fasta";
		String outputFileName = "3spectra_output";

		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("devil.config");
		config.load(file);

		// Peformance
		boolean saveMemory = Boolean.parseBoolean(config.getProperty("saveMemory"));
		int cores = Integer.parseInt(config.getProperty("cores"));

		// Novor
		double tolerance = Double.parseDouble(config.getProperty("tolerance"));
		String NovorPath = config.getProperty("Novor");
		double meanAaScore = Double.parseDouble(config.getProperty("meanAaScore"));
		double maxAbsError = Double.parseDouble(config.getProperty("maxAbsError"));

		/////////////
		// De Novo //
		/////////////

		// start record time
		long startTime = System.currentTimeMillis();

		// Execute Novor
		NovorProcess novorProcess = new NovorProcess(mgfFileName, NovorPath + "/lib", outputFileName, NovorPath + "/params.txt");
		novorProcess.run();

		// Import Novor results
		Map<Integer, NovorHit> novorReader = NovorHit.readNovorHitsFromFile("output/Novor/" + outputFileName + ".csv",
				mgfFileName);

		
		// Init database with MapDB
		File DBfile = new File("spectraDB.db");
		DBfile.delete();
		DB db;

		// Create Heap in DB
		Serializer<Tag> serializer = new TagSerializer();

		if (saveMemory) {
			db = DBMaker.fileDB(DBfile).allocateStartSize(10 * 1024 * 1024 * 1024) // 10GB
					.allocateIncrement(512 * 1024 * 1024) // 512MB
					.fileChannelEnable().make();

		} else {
			db = DBMaker.heapDB().make();
		}

		
		// Generate Tags
		HTreeMap<Integer, Tag> tags = (HTreeMap<Integer,Tag>) db.hashMap("tags_raw").valueSerializer(serializer).createOrOpen();
		TagGenerator.generateTagsForList(novorReader,tags, db);
		
		// Read fasta database
		ProteinLib proteinLib = new ProteinLib(fastaDataBase);
		fastaDataBase = fastaDataBase.replace(".fasta", "_noDup.fasta");

		// Build tag index
		HTreeMap<Integer, List<Integer>> tagIndex = (HTreeMap<Integer, List<Integer>>) db.hashMap("index").createOrOpen();
		TagIndex tagIndexer = new TagIndex();
		tagIndexer.buildIndex(db, tagIndex, proteinLib, cores);
		
		
		HTreeMap<Integer, Tag> tagList = (HTreeMap<Integer, Tag>) db.hashMap("virus").valueSerializer(serializer)
		.createOrOpen();

		Tag.matchTagsToProteins(db, tagList, tags, proteinLib, tagIndex, tolerance, meanAaScore, cores);

		// Extract perfect matches
		HTreeMap<Integer, Tag> perfectMatches = (HTreeMap<Integer,Tag>) db.hashMap("perfectMatches").valueSerializer(serializer).createOrOpen();
		HTreeMap<Integer, Tag> weakMatches = (HTreeMap<Integer,Tag>) db.hashMap("weakMatches").valueSerializer(serializer).createOrOpen();
		Tag.extractPerfectMatches(tagList, proteinLib, db, maxAbsError, perfectMatches, weakMatches, cores);
		db.commit();

		// Mutations
		tagList = Mutation.mutationsOfMatchedTags(tagList, proteinLib);
		tagList = Modification.getModificationForMatchedTags(weakMatches, proteinLib, db, cores);

		List<ProteinHit> perfectProteins = Tag.linkTagsToProteins(tagList, proteinLib);

		ProteinHit.proteinListCoverage(perfectProteins, proteinLib);

		// Export
		Tag.exportTagToTSV(perfectMatches, outputFileName + "_perfect_tags_collapsed.tsv", proteinLib);
		Tag.exportFullTSV(perfectMatches, outputFileName + "_perfect_all_tags_.tsv", proteinLib);
		ProteinHit.exportProteinsToTSV(perfectProteins, "output/ProteinHits/" + outputFileName + "_Proteins.tsv");
		Tag.exportTagToTSV(weakMatches, outputFileName + "_weak_tags_collapsed.tsv", proteinLib);
		Tag.exportFullTSV(weakMatches, outputFileName + "_weak_all_tags_.tsv", proteinLib);

		db.close();

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time elapsed: " + StringUtilities.millisToShortDHMS(elapsedTime) + " seconds");

		long total = Runtime.getRuntime().totalMemory();
		long used = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Total memory: " + total / 1024 / 1024 + " MB");
		System.out.println("Used memory: " + used / 1024 / 1024 + " MB");
	}
}
