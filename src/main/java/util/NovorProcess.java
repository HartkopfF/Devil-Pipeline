package util;

import java.io.File;
import java.io.IOException;


public class NovorProcess extends Thread {

	///////////////
	// Variables //
	///////////////

	/**
	 * The path for the parameter file.
	 */
	protected String paramPath = null;

	/**
	 * This String holds the path of the Mascot Generic File.
	 */
	protected String mgfPath = null;

	/**
	 * This String holds the path of novor.jar.
	 */
	protected String novorPath = null;

	/**
	 * This String holds the path of output.
	 */
	protected String outputFile = null;

	/////////////////
	// Constructor //
	/////////////////

	/**
	 * Constructor for executing a Novor process.
	 * @param mgfPath
	 * @param novorPath 
	 * @param outputFile
	 * @param paramPath
	 * @author Felix Hartkopf
	 */
	public NovorProcess(String mgfPath,String novorPath,String outputFile, String paramPath){
		this.mgfPath = mgfPath;
		this.novorPath = novorPath;
		this.outputFile = outputFile;
		this.paramPath = paramPath;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 *  Method to start novor task
	 */
	public void run(){
		try {
			// Create output folder

			File dir = new File("output/Novor/");
			dir.mkdirs();

			// Start Novor process
			System.out.println("Novor log is stored in output/Novor/"+outputFile+".log!");	
			ProcessBuilder builder = new ProcessBuilder("java", "-Xmx3500M", "-jar",  novorPath + "/novor.jar", "-p", paramPath, mgfPath, "-o", "output/Novor/"+outputFile+".csv", "-f");
			builder.redirectOutput(new File("output/Novor/"+outputFile+".log"));
			builder.redirectError(new File("output/Novor/"+outputFile+".log"));
			Process p = builder.start(); 
			p.waitFor();
			System.out.println("Novor finished with "+ mgfPath);	
		} 
		catch (IOException | InterruptedException e) {
			e.printStackTrace();

		}
	}

}
