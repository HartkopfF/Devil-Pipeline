package util;

public class ResourcesConstants {



	/* FASTA files */
	public final static String FASTA_ADV = "res/fasta/uniprot-adv11.fasta";
	public final static String FASTA_CPXV = "res/fasta/uniprot-cpxv.fasta";
	public final static String FASTA_HUMAN = "res/fasta/uniprot-human.fasta";
	public final static String FASTA_RVFV = "res/fasta/uniprot-rvfv_noDup.fasta";
	public final static String FASTA_CRAP = "res/fasta/crap.fasta";
	public final static String FASTA_CRAP_BOVIN = "res/fasta/crap_bovin.fasta";
	public final static String FASTA_DEBUG = "res/fasta/debug.fasta";
	public final static String FASTA_HUMAN_CRAP = "res/fasta/human_crap.fasta";
	public final static String FASTA_TEST_FILE = "res/fasta/uniprot_test.fasta";
	public final static String FASTA_PFU_FILE = "res/fasta/pfu.fasta";
	public final static String FASTA_UPS1_UPS2_FILE = "res/fasta/ups1_ups2.fasta";
	public final static String FASTA_YEAST_UPS_FILE = "res/fasta/yeast_ups.fasta";
	public final static String FASTA_ALL_HUMAN_VIRUS = "res/fasta/uniprot-virus_host_homo_sapiens_reviewed.fasta";

	/* Spectrum files */
	public final static String ADV = "res/spectra/adv/174_VD_ADV_300kDa_Mix-SP3-Trp_2�g_ID_3h_160324.mgf";
	public final static String RVFV = "res/spectra/rvfv/RVFV-0,1MOI-supernatant-HEp-24h.mgf";
	public final static String CPXV = "res/spectra/cpxv/CPXV-0,1MOI-supernatant-HEp-24h.mgf";
	public final static String ONESPECTRUM_UPS1_FILE = "res/spectra/ups1/UPS1_OneSpectrum.mgf";
	public final static String UPS1_TEST_1629 = "res/spectra/ups1/UPS1_1629Spectra.mgf";
	public final static String UPS1_TEST_117 = "res/spectra/ups1/UPS1_117Spectra.mgf";
	public final static String TEST = "res/spectra/test/testSpectra.mgf";

	/* MSGF+ output files */
	public final static String MSGF_DEBUG = "res/msgf+/debug.tsv";
	public final static String MSGF_CPXV_HUMAN_CRAP = "res/msgf+/CPXV_human_crap.tsv";
	public final static String MSGF_OUTPUT_UPS1 = "res/msgf+/UPS1_12500amol_R1.tsv";
	public final static String MSGF_OUTPUT_UPS1_FDR1 = "res/msgf+/UPS1_12500amol_FDR1.tsv";
	public final static String MSGF_OUTPUT_UPS1_117HITS = "res/msgf+/UPS1_117Spectra.tsv";
	public final static String MSGF_OUTPUT_UPS1_1629HITS = "res/msgf+/UPS1_1629Spectra.tsv";
	public final static String MSGF_OUTPUT_UPS1_1629HITS_FDR1 = "res/msgf+/UPS1_1629Spectra_FDR1.tsv";

	/*	Python scripts */
	public final static String PLOT_SUMMARY = "python/plotsummary.py";
	public final static String PLOT_TAGS = "python/plotTags.py";

}
