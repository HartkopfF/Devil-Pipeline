package model;

/**
 * 
 * This class represents the Peak interface.
 *
 * @author Thilo Muth
 */
public interface Peak {

	///////////////////////
	// Interface Methods //
	///////////////////////

	/**
	 * This method returns the m/z.
	 *
	 * @return double
	 */
	public double getMz();

	/**
	 * This method returns the intensity.
	 *
	 * @return double
	 */
	public double getIntensity();

	/**
	 * Returns charge information for this peak.
	 *
	 * @return the charge
	 */
	public int getCharge();
}
