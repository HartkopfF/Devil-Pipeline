package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.mapdb.HTreeMap;

import model.tag.Tag;
import util.Masses;
import util.Pair;
import util.Pam;

/**
 * TODO: API!
 *
 */
public class Mutation extends Object {

	///////////////
	// Variables //
	///////////////

	/**
	 * The source (original) amino acid.
	 */
	private Residue source;

	/**
	 * The mutated amino acid.
	 */
	private Residue target;

	/**
	 * The mass shift between source and target amino acid.
	 */
	private double massShift;

	/**
	 * The difference between mass shift and mass shift that is declared through this mutation.
	 */
	private double declaredMassShift;

	/**
	 * The index/position of a mutation in the source and target sequence
	 */
	private int mutationIndex;

	//////////////////
	// Constructors //
	//////////////////

	/**
	 * Constructs a empty mutation.
	 */
	public Mutation() {
	}

	/**
	 * Constructs a mutation from one amino acid (source) to another (target).
	 */
	public Mutation(Residue source, Residue target) {
		this.source = source;
		this.target = target;
		this.massShift = target.getMass() - source.getMass();
	}

	/**
	 * Constructs a mutation from one amino acid (source) to another (target).
	 */
	public Mutation(Residue source, Residue target, int mutationIndex) {
		this.source = source;
		this.target = target;
		this.massShift = target.getMass() - source.getMass();
		this.mutationIndex = mutationIndex;
	}

	/**
	 * Constructs a mutation from one amino acid (source) to another (target).
	 */
	public Mutation(Residue source, Residue target, int mutationIndex, double declaredMassShift) {
		this.source = source;
		this.target = target;
		this.massShift = target.getMass() - source.getMass();
		this.mutationIndex = mutationIndex;
		this.declaredMassShift = declaredMassShift;
	}

	/////////////////////////////
	// Setter & Getter Methods //
	/////////////////////////////

	/**
	 * Returns the mass shift.
	 * @return The mass shift.
	 */
	public double getMassShift() {
		return massShift;
	}

	/**
	 * Get rounded mass shift.
	 * @return Rounded mass shift.
	 */
	public int getRoundedMassShift() {
		return (int) Math.round(massShift);
	}

	/**
	 * Returns the source amino acid.
	 * @return The source amino acid.
	 */
	public Residue getSource() {
		return source;
	}

	/**
	 * Returns the target amino acid.
	 * @return The target amino acid.
	 */
	public Residue getTarget() {
		return target;
	}

	/**
	 * Returns the position of a mutation.
	 * @return The position of a mutation.
	 * @author Felix Hartkopf
	 */
	public int getMutationIndex() {
		return mutationIndex;
	}

	/**
	 * Sets the source amino acid.
	 * @author Felix Hartkopf
	 */
	public void setSource(Residue AA) {
		this.source = AA;
	}

	/**
	 * Sets the target amino acid.
	 * @author Felix Hartkopf
	 */
	public void setTarget(Residue AA) {
		this.target = AA;
	}

	/**
	 * Sets the position of a mutation.
	 * @author Felix Hartkopf
	 */
	public void setMutationIndex(int index) {
		this.mutationIndex = index;
	}



	/**
	 * @return the declaredMassShift
	 */
	public double getDeclaredMassShift() {
		return declaredMassShift;
	}

	/**
	 * @param declaredMassShift the declaredMassShift to set
	 */
	public void setDeclaredMassShift(double declaredMassShift) {
		this.declaredMassShift = declaredMassShift;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Overwritten toString method.
	 * @return The mutation as string representation.
	 */
	public String toString() {
		int position = getMutationIndex();
		return this.source.getThreeLetterCode()+ " ("+this.source.getOneLetterCode()+")" + " => "  + this.target.getThreeLetterCode()+ " ("+this.target.getOneLetterCode()+")" + " at position " + Integer.toString(position) + " with " + this.massShift ;
	}

	/**
	 * Returns the mass shift.
	 */
	public void calcMassShift() {
		try{
			this.massShift = target.getMass() - source.getMass();
		}catch(IllegalArgumentException e){
			System.out.println("Target and sources must be declared to calculate mass shift!");
		}
	}

	/**
	 * Overwritten equals method.
	 * @param mutation The mutation to be compared.
	 * @return True if both mutations are the same.
	 */
	public boolean equals(Mutation mutation) {
		if (this.source.getOneLetterCode() == mutation.getSource().getOneLetterCode()
				&& this.target.getOneLetterCode() == mutation.getTarget().getOneLetterCode()) {
			return true;
		}
		return false;
	}

	////////////////////
	// Static Methods //
	////////////////////

	/**
	 * This Method calculates the possible mutations for a tag in the corresponding sequence.
	 * It considers the mass shift. 
	 * @param peptide
	 * @param sideMass
	 * @return possible mutation in peptide
	 * @author Felix Hartkopf
	 */
	public static List<Mutation> calcPossibleMutations(String peptideSequence, double sideMass){

		List<Mutation> topMutations = new ArrayList<Mutation>();
		int charge = 1; //peptide.getCharge();
		double[][] massShiftMatrix = createMassShiftMatrix(charge);

		//get best substitutions for the whole peptide
		for(int i = 0; i < peptideSequence.length(); i++ ){
			Mutation tmpMutation = calcClosestMutation(peptideSequence.charAt(i), massShiftMatrix, sideMass);
			tmpMutation.setMutationIndex(i);
			topMutations.add(tmpMutation);
		}

		// Get lowest difference of all mutations
		List<Mutation> finalMutations = new ArrayList<Mutation>();
		finalMutations.add(topMutations.get(0));
		topMutations.remove(topMutations.get(0));
		double bestValue = finalMutations.get(0).declaredMassShift;
		for (Mutation tmpMutation : topMutations) {
			if (tmpMutation.getDeclaredMassShift() < bestValue) {
				bestValue = tmpMutation.getDeclaredMassShift();
				finalMutations.removeAll(finalMutations);
				finalMutations.add(tmpMutation);
			} else if (tmpMutation.getDeclaredMassShift() == bestValue) {
				finalMutations.add(tmpMutation);
			}
		}

		if(bestValue<0.2) {
			return finalMutations;
		}else {
			finalMutations.removeAll(finalMutations);
			return finalMutations;
		}
	}



	/**
	 * Returns the start of the tag via string matching
	 * @param tag
	 * @param peptide
	 * @return returns start of tag
	 * @author Felix Hartkopf
	 */
	public static int startOfTag(Tag tag, Peptide peptide){
		String tagSequence = tag.getSequence();
		String peptideSequence = peptide.getSequence();
		int start = -1;
		start = peptideSequence.indexOf(tagSequence);
		return start;
	}


	/**
	 * Method for mass shift if one amino acid is substituted with another
	 * @param original
	 * @param mutation
	 * @return  mass shift if one amino acid is substituted with another
	 * @author Felix Hartkopf
	 */

	public static double calcMassShift(String original, String mutation, int charge){
		Masses masses = Masses.getInstance();
		double massShift;
		massShift = ((masses.get(original)+charge*masses.get("Hydrogen"))/charge) - ((masses.get(mutation)+charge*masses.get("Hydrogen"))/charge);
		return massShift;
	}


	/**
	 * Method to get all possible tags with a possible mutation and which is valid on both sides of the tag
	 * @param tagList List of matched tags
	 */
	@SuppressWarnings("unchecked")
	public static HTreeMap<Integer, Tag> mutationsOfMatchedTags(HTreeMap<Integer, Tag> tagList, ProteinLib proteinLib){
		System.out.println("Calculating mutations...");
		Set<Integer> keys = tagList.keySet();
		for(int key : keys){
			Tag tmpTag = tagList.get(key);
			List<Integer> matchedPeptides = tmpTag.getPeptides();
			String tagSequence = tmpTag.getSequence();

			for(Integer peptideID : matchedPeptides) {
				Peptide tmpPeptide = proteinLib.getPeptide(peptideID);	
				String sequence = tmpPeptide.getSequence();
				Pair<Double,Double> massShift = tmpPeptide.calculateMassShift(tmpTag);
				double leftMassShift = massShift.getLeft();
				double rightMassShift = massShift.getRight();
				String[] sequenceSplit = sequence.split(tagSequence);
				if(sequenceSplit.length == 0) {
					System.out.println("Error: "+sequence+"\t"+tagSequence);
				}else if(sequenceSplit.length==2 & !sequenceSplit[0].isEmpty()){ // If is in the middle of the peptide
					String leftSeq = sequenceSplit[0];
					String rightSeq = sequenceSplit[1];
					if(Math.abs(leftMassShift)>=1.0) {
						List<Mutation> mutations = calcPossibleMutations(leftSeq, leftMassShift);
						if(mutations.size()>0) {
							tmpTag.setLeftMutations(peptideID, mutations);
						}
					}
					if(Math.abs(rightMassShift)>=1.0) {
						List<Mutation> mutations = calcPossibleMutations(rightSeq, rightMassShift);
						if(mutations.size()>0) {
							tmpTag.setRightMutations(peptideID, mutations);	
						}
					}
				}else if(sequenceSplit.length==1){ // If tag is at the beginning or the end of the peptide
					String seq = sequenceSplit[0];
					if(Math.abs(rightMassShift)>=1.0 && sequence.indexOf(tagSequence)==0) {
						List<Mutation> mutations = calcPossibleMutations(seq, rightMassShift);
						if(mutations.size()>0) {
							tmpTag.setRightMutations(peptideID, mutations);		 
						}
					}
					if(Math.abs(leftMassShift)>=1.0 && sequence.indexOf(tagSequence)>0) {
						List<Mutation> mutations = calcPossibleMutations(seq, leftMassShift);
						if(mutations.size()>0) {
							tmpTag.setLeftMutations(peptideID, mutations);
						}
					}
				}else if(sequenceSplit.length==2 & sequenceSplit[0].isEmpty()){ // If tag is at the beginning or the end of the peptide
					String rightSeq = sequenceSplit[1];
					if(Math.abs(rightMassShift)>=1.0) {
						List<Mutation> mutations = calcPossibleMutations(rightSeq, rightMassShift);
						if(mutations.size()>0) {
							tmpTag.setRightMutations(peptideID, mutations);
						}
					}
				}else{
					System.out.println("More than one match of tag in sequence! Aborted!"+"\t"+sequence+"\t"+tagSequence);
				}
			}
			tagList.put(key,tmpTag);
		}
		return tagList;
	}

	/**
	 * 
	 * @return matrix for every amino acid and resulting mass shift
	 * @param charge This defines the charge that is used
	 * @author Felix Hartkopf 
	 */
	public static double[][] createMassShiftMatrix(int charge){
		double[][] massShiftMatrix = new double[20][20];

		for(int original=0;original<=19;original++){
			for(int mutation=0;mutation<=19;mutation++){
				String oIndex = Pam.getAminoAcid(original);
				String mIndex = Pam.getAminoAcid(mutation);
				massShiftMatrix[original][mutation] = calcMassShift(oIndex,mIndex,charge);
			}
		}
		return massShiftMatrix;
	}

	/**
	 * method to calculate the best mutation for a certain mass shift
	 * TODO: Please insert the description of the parameters here.
	 * @param original
	 * @param shiftMatrix
	 * @param sideMass
	 * @return a char of the amino acid(one-letter-code) of the best mutation
	 * @author Felix Hartkopf
	 */
	public static Mutation calcClosestMutation(char original, double[][] shiftMatrix, double sideMass){

		Mutation mutation = new Mutation();
		mutation.setSource(Residue.valueOf(Residue.convertOneToThree(original)));

		//////////////////////////////////////////////
		if(original=='U'|original=='X'|original=='Z'|original=='B'){
			System.out.println("Invalid amino acid in findNearestMutation(): "+original);
			mutation.setTarget(Residue.valueOf(Residue.convertOneToThree(original)));
			mutation.setDeclaredMassShift(Double.NEGATIVE_INFINITY);
			return(mutation);		
		}
		//////////////////////////////////////////////

		int index = Pam.getIndex(original);
		double[] shiftVector = shiftMatrix[index];
		double distance = Math.abs(shiftVector[0] - sideMass);
		mutation.setTarget(Residue.valueOf(Residue.convertOneToThree(Pam.getAminoAcid(0).charAt(0))));
		mutation.setDeclaredMassShift(distance);
		mutation.calcMassShift();
		int mutationIndex = 0;

		for(int i = 0; i < shiftVector.length; i++ ){
			double idistance = Math.abs(shiftVector[i] - sideMass);
			if(idistance < distance && i != index){
				mutationIndex = i;
				distance = idistance;
				mutation.setTarget(Residue.valueOf(Residue.convertOneToThree(Pam.getAminoAcid(mutationIndex).charAt(0))));
				mutation.setDeclaredMassShift(distance);
				mutation.calcMassShift();
			}
		}
		return mutation;	
	}
}