package model;

/**
 * 
 * This class represents a spectrum peak.
 *
 * @author Thilo Muth
 */
public class SpectrumPeak implements Peak {

	///////////////
	// Variables //
	///////////////

	/**
	 * This double holds the m/z.
	 */
	private double mz;
	/**
	 * This double holds the intensity.
	 */
	private double intensity;
	/**
	 * This Integer holds the charge.
	 */
	private int charge;

	/**
	 * Empty constructor for a spectrum peak.
	 */
	public SpectrumPeak() {
	}

	//////////////////
	// Constructors //
	//////////////////

	/**
	 * Constructor gets the m/z and the intensity.
	 *
	 * @param mz
	 * @param intensity
	 */
	public SpectrumPeak(double mz, double intensity) {
		this.mz = mz;
		this.intensity = intensity;
	}

	/**
	 * Constructor gets the m/z, the intensity and the charge.
	 *
	 * @param mz
	 * @param intensity
	 * @param charge
	 */
	public SpectrumPeak(double mz, double intensity, int charge) {
		this.mz = mz;
		this.intensity = intensity;
		this.charge = charge;
	}

	/////////////////////////////
	// Setter & Getter Methods //
	/////////////////////////////

	/**
	 * Sets the charge.
	 *
	 * @param charge
	 */
	public void setCharge(int charge) {
		this.charge = charge;
	}

	/**
	 * Returns the charge.
	 *
	 * @return the charge
	 */
	public int getCharge() {
		return charge;
	}

	/**
	 * Sets the intensity.
	 *
	 * @param intensity
	 */
	public void setIntensity(double intensity) {
		this.intensity = intensity;
	}

	/**
	 * Returns the intensity.
	 *
	 * @return the intensity
	 */
	public double getIntensity() {
		return intensity;
	}

	/**
	 * Sets the m/z.
	 *
	 * @param mz
	 */
	public void setMz(double mz) {
		this.mz = mz;
	}

	/**
	 * Returns the m/z.
	 *
	 * @return the m/z
	 */
	public double getMz() {
		return mz;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Check whether this peak equals to another peak.
	 * @param peak Comparable peak.
	 * @return true if the peaks are equal
	 */
	public boolean equals(SpectrumPeak peak) {
		if(this.mz == peak.getMz() && this.intensity == peak.getIntensity()) {
			return true;
		}
		else {
			return false;
		}
	}
}
