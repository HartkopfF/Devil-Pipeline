package model;

import util.Masses;

public enum Residue {
	Ala("Alanine", 'A', Masses.A),
	Arg("Arginine", 'R', Masses.R),
	Asn("Asparagine", 'N', Masses.N),
	Asp("Aspartic_Acid", 'D', Masses.D),
	Cys("Cysteine", 'C', Masses.C),
	Glu("Glutamic_Acid", 'E', Masses.E),
	Gln("Glutamine", 'Q', Masses.Q),
	Gly("Glycine", 'G', Masses.G),
	His("Histidine", 'H', Masses.H),
	Ile("Isoleucine", 'I', Masses.I),
	Xle("Isoleucine_or_Leucine", 'J', Masses.J),
	Leu("Leucine", 'L', Masses.L),
	Lys("Lysine", 'K', Masses.K),
	Met("Methionine", 'M', Masses.M),
	Phe("Phenylalanine", 'F', Masses.F),
	Pro("Proline", 'P', Masses.P),
	Ser("Serine", 'S', Masses.S),
	Thr("Threonine", 'T', Masses.T),
	SeC("Selenocysteine", 'U', Masses.U),
	Trp("Tryptophan", 'W', Masses.W),
	Tyr("Tyrosine", 'Y', Masses.Y),
	Val("Valine", 'V', Masses.V),
	Pyl("Pyrrolysine", '0', 237.147727),
	Asx("Asparagine_or_Aspartic_Acid", 'B', 0.0),
	Glx("Glutamic_Acid_or_Glutamine", 'Z', 0.0),
	Xaa("Unknown amino acid", 'X', Masses.X);

	private final String threeLetterCode;

	private final char oneLetterCode;

	private final double mass;

	Residue(String threeLetterCode, char oneLetterCode, double mass) {
		this.threeLetterCode = threeLetterCode;
		this.oneLetterCode = oneLetterCode;
		this.mass = mass;
	}


	public String getThreeLetterCode() {
		return threeLetterCode;
	}

	public char getOneLetterCode() {
		return oneLetterCode;
	}

	public double getMass() {
		return mass;
	}

	/**
	 * This method convert oneLetterCode into threeLetterCode
	 * @param oneLetter
	 * @return
	 * @author Felix Hartkopf
	 */

	public static Residue getResidueByChar(char oneLetter){
		switch (oneLetter) {
		case 'A': return Residue.Ala;
		case 'R': return Residue.Arg;
		case 'N': return Residue.Asn;
		case 'D': return Residue.Asp;
		case 'C': return Residue.Cys;
		case 'E': return Residue.Glu;
		case 'Q': return Residue.Gln;
		case 'G': return Residue.Gly;
		case 'H': return Residue.His;
		case 'I': return Residue.Ile;
		case 'J': return Residue.Xle;
		case 'L': return Residue.Leu;
		case 'K': return Residue.Lys;
		case 'M': return Residue.Met;
		case 'F': return Residue.Phe;
		case 'P': return Residue.Pro;
		case 'S': return Residue.Ser;
		case 'T': return Residue.Thr;
		case 'U': return Residue.SeC;
		case 'W': return Residue.Trp;
		case 'Y': return Residue.Tyr;
		case 'V': return Residue.Val;
		case '0': return Residue.Pyl;
		case 'B': return Residue.Asx;
		case 'Z': return Residue.Glx;
		case 'X': return Residue.Xaa;
		default: return Residue.Xaa;
		}
	}
	
	public static String convertOneToThree(char oneLetter){
		switch (oneLetter) {
		case 'A': return "Ala";
		case 'R': return "Arg";
		case 'N': return "Asn";
		case 'D': return "Asp";
		case 'C': return "Cys";
		case 'E': return "Glu";
		case 'Q': return "Gln";
		case 'G': return "Gly";
		case 'H': return "His";
		case 'I': return "Ile";
		case 'J': return "Xle";
		case 'L': return "Leu";
		case 'K': return "Lys";
		case 'M': return "Met";
		case 'F': return "Phe";
		case 'P': return "Pro";
		case 'S': return "Ser";
		case 'T': return "Thr";
		case 'U': return "SeC";
		case 'W': return "Trp";
		case 'Y': return "Tyr";
		case 'V': return "Val";
		case '0': return "Pyl";
		case 'B': return "Asx";
		case 'Z': return "Glx";
		case 'X': return "Xaa";
		default: return "Xaa";
		}
	}


	@Override
	public String toString() {
		return threeLetterCode;
	}  
}
