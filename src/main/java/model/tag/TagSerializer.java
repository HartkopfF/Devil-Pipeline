package model.tag;

import org.mapdb.DataInput2;
import org.mapdb.DataOutput2;
import org.mapdb.Serializer;

import model.Modification;
import model.Mutation;
import model.Residue;
import model.tag.Tag.TagType;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;
import java.util.Map.Entry;


//
// Second option for using custom values is to use your own serializer.
// This usually leads to better performance as MapDB does not have to
// analyze the class structure.
//

public class TagSerializer implements Serializer<Tag>, Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Method to serialize Tag objects
	 */
	@Override
	public void serialize(DataOutput2 out, Tag tag) throws IOException {

		String sequence = tag.getSequence();
		String denovoSequence = tag.getDenovoSequence();
		double tagMass = tag.getTagMass();
		double leftMass = tag.getLeftMass();
		double rightMass = tag.getRightMass();
		int spectrumId = tag.getSpectrumId();
		double pepMass = tag.getPepMass();
		TagType tagType = tag.getType();

		int charge = tag.getCharge();
		double score = tag.getScore();
		List<Integer> peptides = tag.matchedPeptides;
		TreeMap<Integer, List<Mutation>> leftMutations = tag.getAllLeftMutations(); 
		TreeMap<Integer, List<Mutation>> rightMutations = tag.getAllRightMutations();
		TreeMap<Integer, Modification> leftModifications = tag.getAllLeftModifications();
		TreeMap<Integer, Modification> rightModifications = tag.getAllRightModifications();

		out.writeUTF(sequence);
		out.writeUTF(denovoSequence);
		out.writeDouble(tagMass);
		out.writeDouble(leftMass);
		out.writeDouble(rightMass);
		out.writeInt(spectrumId);
		out.writeDouble(pepMass);



		if(tagType.equals(TagType.N_TERMINAL)) {
			out.writeInt(0);
		}else if(tagType.equals(TagType.MIDDLE)) {
			out.writeInt(1);
		}else if(tagType.equals(TagType.C_TERMINAL)) {
			out.writeInt(2);
		}



		out.writeInt(charge);
		out.writeDouble(score);

		out.writeInt(peptides.size());
		for (Integer peptideID : peptides){
			out.writeInt(peptideID);
		}
		out.writeUTF("End_peptides");

		int counter = 1;
		int counterM = 1;
		boolean empty = true;

		for (Entry<Integer, List<Mutation>> entry : leftMutations.entrySet()) {
			empty = false;
			if(counter == 1) {
				out.writeUTF("Start_lmutations");

			}

			Integer key = entry.getKey();
			List<Mutation> value = entry.getValue();
			out.writeInt(key);
			for(Mutation mutation : value) {
				out.writeChar(mutation.getSource().getOneLetterCode());				
				out.writeChar(mutation.getTarget().getOneLetterCode());				
				out.writeDouble(mutation.getDeclaredMassShift());
				out.writeInt(mutation.getMutationIndex());
				if(counterM == value.size()) {
					out.writeUTF("stop");
				}else {
					out.writeUTF("continue");
					counterM++;
				}
			}
			counterM = 1;
			if(counter == leftMutations.size()) {
				out.writeUTF("End_lmutations");
			}else {	
				out.writeUTF("continue");
				counter++;
			}
		}
		if(empty) {
			out.writeUTF("End_lmutations");
		}

		empty = true;
		counter = 1;
		counterM = 1;
		for (Entry<Integer, List<Mutation>> entry : rightMutations.entrySet()) {
			empty = false;
			if(counter == 1) {
				out.writeUTF("Start_lmutations");
			}

			Integer key = entry.getKey();
			List<Mutation> value = entry.getValue();
			out.writeInt(key);
			for(Mutation mutation : value) {
				out.writeChar(mutation.getSource().getOneLetterCode());				
				out.writeChar(mutation.getTarget().getOneLetterCode());				
				out.writeDouble(mutation.getDeclaredMassShift());
				out.writeInt(mutation.getMutationIndex());
				if(counterM == value.size()) {

					out.writeUTF("stop");
				}else {
					out.writeUTF("continue");
					counterM++;
				}
			}

			counterM = 1;
			if(counter == rightMutations.size()) {
				out.writeUTF("End_rmutations");
			}else {	
				out.writeUTF("continue");
				counter++;
			}
		}		
		if(empty) {
			out.writeUTF("End_rmutations");
		}

		empty = true;
		counter = 1;
		for (Entry<Integer, Modification> entry : leftModifications.entrySet()) {
			empty = false;
			if(counter == 1) {
				out.writeUTF("Start_lmods");
			}
			int key = entry.getKey();
			Modification value = entry.getValue();
			out.writeInt(key);
			out.writeChars(value.getShortName());
			out.writeUTF(value.getDescription());
			out.writeDouble(value.getMonoMass());
			out.writeDouble(value.getAvgMass());
			out.writeInt(value.getN15());
			out.writeInt(value.getUnimod());
			out.writeUTF(value.getType());
			out.writeChar(value.getAminoAcid());
			out.writeUTF(value.getPsiMs());
			if(counter == rightModifications.size()) {
				out.writeUTF("End_lmods");
				out.writeUTF("End_lmods");

			}else {
				out.writeUTF("End_lmod");
				out.writeUTF("End_lmod");
				counter++;
			}
		}
		if(empty) {
			out.writeUTF("End_lmods");
		}

		empty = true;
		counter = 1;
		for (Entry<Integer, Modification> entry : rightModifications.entrySet()) {
			empty = false;
			if(counter == 1) {
				out.writeUTF("Start_rmods");
			}
			int key = entry.getKey();
			Modification value = entry.getValue();
			out.writeInt(key);
			out.writeUTF(value.getShortName());
			out.writeUTF(value.getDescription());
			out.writeDouble(value.getMonoMass());
			out.writeDouble(value.getAvgMass());
			out.writeInt(value.getN15());
			out.writeInt(value.getUnimod());
			out.writeUTF(value.getType());
			out.writeChar(value.getAminoAcid());
			out.writeUTF(value.getPsiMs());
			if(counter == rightModifications.size()) {
				out.writeUTF("End_rmods");
				out.writeUTF("End_rmods");

			}else {
				out.writeUTF("End_rmod");
				out.writeUTF("End_rmod");
				counter++;
			}

		}
		if(empty) {
			out.writeUTF("End_rmods");
		}



	}

	/**
	 * Method to deserialize Tag objects
	 */
	@Override
	public Tag deserialize(DataInput2 input, int available) throws IOException {
		String sequence = input.readUTF();
		String denovoSequence = input.readUTF();
		double tagMass = input.readDouble();
		double leftMass = input.readDouble();
		double rightMass = input.readDouble();
		int spectrumId = input.readInt();
		double pepMass = input.readDouble();
		int tmpType = input.readInt();
		TagType tagType = null;
		switch (tmpType) {
		case 0: tagType = TagType.N_TERMINAL;
		break;
		case 1: tagType = TagType.MIDDLE;
		break;
		case 2: tagType = TagType.C_TERMINAL;
		break;
		}

		int charge = input.readInt();
		double score = input.readDouble();

		Tag tag = new Tag(sequence, charge, tagMass, leftMass, rightMass, score, pepMass);
		tag.setDenovoSequence(denovoSequence);
		tag.setTagType(tagType);
		tag.setSpectrumId(spectrumId);

		int numberPeptides = input.readInt();
		for(int i = 0; i < numberPeptides; i++) {
			tag.matchedPeptides.add(input.readInt());
		}


		List<Mutation> mutationList = new ArrayList<Mutation>();
		String line = input.readUTF();
		line = input.readLine().toString();

		while(!line.equals("End_lmutations")) {

			Integer peptideID = input.readInt();
			while(!line.equals("stop")) {
				Residue source = Residue.getResidueByChar(input.readChar());
				Residue target = Residue.getResidueByChar(input.readChar());		
				double declaredMassShift = input.readDouble();
				int mutationIndex =  input.readInt();
				Mutation mutation = new Mutation(source, target, mutationIndex,declaredMassShift);
				mutationList.add(mutation);

				line = input.readUTF();
			}

			line = input.readUTF();

			tag.setLeftMutations(peptideID, mutationList);		
		}

		mutationList = new ArrayList<Mutation>();
		line = input.readLine().toString();

		while(!line.equals("End_rmutations")) {

			Integer peptideID = input.readInt();
			while(!line.equals("stop")) {
				Residue source = Residue.getResidueByChar(input.readChar());
				Residue target = Residue.getResidueByChar(input.readChar());
				double declaredMassShift = input.readDouble();
				int mutationIndex =  input.readInt();
				Mutation mutation = new Mutation(source, target, mutationIndex,declaredMassShift);
				mutationList.add(mutation);

				line = input.readUTF();
			}

			line = input.readUTF();

			tag.setRightMutations(peptideID, mutationList);		
		}

		line = input.readLine().toString();


		while(!line.equals("End_lmods")) {

			int key = input.readInt();
			String shortName = input.readUTF();
			String description = input.readUTF();
			double monoMass = input.readDouble();
			double avgMass = input.readDouble();
			int n15 = input.readInt();
			int unimod = input.readInt();
			String type = input.readUTF();
			char aminoAcid = input.readChar();
			String psiMs = input.readUTF();

			Modification mod = new Modification(shortName, description, monoMass, avgMass, n15, type, aminoAcid, unimod, psiMs);
			tag.setLeftModifications(key, mod);
			line = input.readUTF();
			line = input.readUTF();

		}

		line = input.readLine().toString();


		while(!line.equals("End_rmods")) {

			int key = input.readInt();
			String shortName = input.readUTF();
			String description = input.readUTF();
			double monoMass = input.readDouble();
			double avgMass = input.readDouble();
			int n15 = input.readInt();
			int unimod = input.readInt();
			String type = input.readUTF();
			char aminoAcid = input.readChar();
			String psiMs = input.readUTF();

			Modification mod = new Modification(shortName, description, monoMass, avgMass, n15, type, aminoAcid, unimod, psiMs);
			tag.setRightModifications(key, mod);
			line = input.readLine();
			line = input.readUTF();

		}

		return tag;
	}
}






