package model.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import org.mapdb.DB;
import org.mapdb.HTreeMap;

import model.Peptide;
import model.ProteinLib;
import util.Masses;
import util.ProgressBar;
import util.StringUtilities;

public class TagIndex {



	//////////////////
	// Constructors //
	//////////////////

	/**
	 * Small constructor
	 */
	public TagIndex() {
	}

	///////////////////
	// Class Methods //
	///////////////////

	public HashMap<Integer, List<Integer>> buildIndex(ProteinLib proteinLib){
		System.out.println("Starting indexing of tags...");

		long startTime = System.currentTimeMillis();
		HashMap<Integer, List<Integer>> index = new HashMap<Integer, List<Integer>>();
		HashMap<Integer, Peptide> peptides =  proteinLib.peptides;

		// Initialize tag HashMap
		Masses masses = Masses.getInstance();
		for (char a1 = 'A'; a1 <= 'Y'; a1++) {
			for (char a2 = 'A'; a2 <= 'Y'; a2++) {
				for (char a3 = 'A'; a3 <= 'Y'; a3++) {
					if (masses.containsKey(Character.toString(a1)) && masses.containsKey(Character.toString(a2)) && masses.containsKey(Character.toString(a3))) {
						String tagSeq = Character.toString(a1) + Character.toString(a2) + Character.toString(a3);
						List<Integer> peptidesList = new ArrayList<Integer>();
						for(Integer peptideID : peptides.keySet()) {
							Peptide peptide = peptides.get(peptideID);
							if(peptide.contains(tagSeq)) {
								peptidesList.add(peptideID);
							}
						}
						index.put(tagSeq.hashCode(), peptidesList);
						ProgressBar.printProgress(startTime, 10648, index.size());

					}
				}
			}
		}

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Indexing of tags completed...");
		System.out.println("Total indexing time: "+StringUtilities.millisToShortDHMS(elapsedTime)+"\n");
		return index;		
	}

	public void buildIndex(DB db, HTreeMap<Integer, List<Integer>> index, ProteinLib proteinLib, int cores) throws StringIndexOutOfBoundsException, IOException, InterruptedException{
		System.out.println("Starting indexing of tags...");

		long startTime = System.currentTimeMillis();

		// Creates the threadpool
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor( cores, cores, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>() );

		// Iterate trough all  possible Tags
		// Initialize tag HashMap
		Masses masses = Masses.getInstance();
		for (char a1 = 'A'; a1 <= 'Y'; a1++) {
			for (char a2 = 'A'; a2 <= 'Y'; a2++) {
				for (char a3 = 'A'; a3 <= 'Y'; a3++) {
					if (masses.containsKey(Character.toString(a1)) && masses.containsKey(Character.toString(a2)) && masses.containsKey(Character.toString(a3))) {
						String tagSeq = Character.toString(a1) + Character.toString(a2) + Character.toString(a3);
						Runnable worker = new TagIndexingWorker(tagSeq, proteinLib, index);
						threadPool.execute(worker);
					}
				}
			}
		}

		threadPool.shutdown();


		while (!threadPool.isTerminated()) {
			// Progress bar
			int finishedWorkers = 10648 - threadPool.getQueue().size()  ;
			if(finishedWorkers>0) {
				ProgressBar.printProgress(startTime, 10648, finishedWorkers);
				Thread.sleep(1000);
				db.commit();
			}
		}

		System.out.println("\nFinished all threads");

		System.out.println("Amount Tags: "+index.size());

		if (index.size() <= 0) {
			System.out.println("\nERROR: The List of tags is empty...\nPlease choose a lower amino acid based score.\n!!!Returning empty list!!!\n");
		} 

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Indexing of tags completed...");
		System.out.println("Total indexing time: "+StringUtilities.millisToShortDHMS(elapsedTime)+"\n");
	}
}
