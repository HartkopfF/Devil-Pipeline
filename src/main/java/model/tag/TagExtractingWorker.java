package model.tag;

import java.util.ArrayList;
import java.util.List;

import org.mapdb.HTreeMap;

import model.Peptide;
import model.ProteinLib;
import util.Pair;



public class TagExtractingWorker implements Runnable {

	private Integer key;
	private Tag tag;
	private ProteinLib proteinLib;
	private double maxAbsError;
	private HTreeMap<Integer, Tag> perfectMatches;
	private HTreeMap<Integer, Tag> weakMatches;


	public TagExtractingWorker(Integer key, Tag tag, ProteinLib proteinLib, double maxAbsError, HTreeMap<Integer, Tag> perfectMatches, HTreeMap<Integer, Tag> weakMatches){
		this.key = key;
		this.tag = tag;
		this.maxAbsError = maxAbsError;
		this.perfectMatches = perfectMatches;
		this.weakMatches = weakMatches;
		this.proteinLib = proteinLib;

	}

	@Override
	public void run() {
		try {
			processCommand();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void processCommand() throws InterruptedException {
		List<Integer> peptideIDs = tag.getPeptides();
		Tag perfectTag = null;
		Tag weakTag = null;

		try {
			perfectTag = tag.clone();
			weakTag = tag.clone();
		} catch (CloneNotSupportedException e) {
			System.out.println("Unable to clone Tag object.");
			e.printStackTrace();
		}

		List<Integer> perfectIDs = new ArrayList<Integer>();
		List<Integer> weakIDs = new ArrayList<Integer>();

		for(Integer peptideID : peptideIDs) {

			double absError = Double.POSITIVE_INFINITY;
			Peptide peptide = proteinLib.getPeptide(peptideID);
			Pair<Double, Double> massShift = peptide.calculateMassShift(tag);

			if(perfectTag.rightMutations.containsKey(peptideID)&!perfectTag.leftMutations.containsKey(peptideID)) { // Only mutations on the right side of the tag
				absError = Math.abs(massShift.getLeft())+Math.abs(perfectTag.rightMutations.get(peptideID).get(0).getDeclaredMassShift());

			}else if(!perfectTag.rightMutations.containsKey(peptideID)&perfectTag.leftMutations.containsKey(peptideID)){ // Only mutations on the left side of the tag
				absError = Math.abs(Math.abs(perfectTag.leftMutations.get(peptideID).get(0).getDeclaredMassShift())+Math.abs(massShift.getRight()));

			}else if(perfectTag.rightMutations.containsKey(peptideID)&perfectTag.leftMutations.containsKey(peptideID)){ // mutations on both sides
				absError = Math.abs(Math.abs(perfectTag.leftMutations.get(peptideID).get(0).getDeclaredMassShift())+Math.abs(perfectTag.rightMutations.get(peptideID).get(0).getDeclaredMassShift()));

			}else {
				absError = Math.abs(massShift.getLeft())+Math.abs(massShift.getRight());

			}

			if(absError<maxAbsError){
				perfectIDs.add(peptideID);
			}else {
				weakIDs.add(peptideID);
			}
		}
		perfectTag.matchedPeptides = perfectIDs;
		weakTag.matchedPeptides = weakIDs;
		perfectMatches.put(key, perfectTag);
		weakMatches.put(key, weakTag);
	}

	@Override
	public String toString(){
		return this.tag.toString();
	}
}