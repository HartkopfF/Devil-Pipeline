package model.tag;

import java.util.List;

import org.mapdb.HTreeMap;

import model.denovo.NovorHit;

public class TagGeneratorWorker implements Runnable {

	private NovorHit novorHit;
	private HTreeMap<Integer, Tag> tags;
	private int threadnumber;

	public TagGeneratorWorker(NovorHit novorHit, HTreeMap<Integer, Tag> tags, int threadnumber){
		this.novorHit = novorHit;
		this.tags = tags;
		this.threadnumber = threadnumber;
	}

	@Override
	public void run() {
		try {
			processCommand();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void processCommand() throws InterruptedException {
		TagGenerator tagGenerator =  new TagGenerator(novorHit, 3, 3);
		List<Tag> tagList = tagGenerator.getTags();
		for(Tag tag : tagList) {
			tags.put(tag.hashCode(), tag);
		}
	}

	@Override
	public String toString(){
		return threadnumber+"_"+novorHit.getDenovoSequence();
	}
}