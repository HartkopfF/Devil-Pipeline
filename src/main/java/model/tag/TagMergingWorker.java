
package model.tag;

import java.util.Map;

public class TagMergingWorker implements Runnable {

	private Tag tagA;
	private Tag tagB;
	public Map<Integer, Tag> tagMapA;
	Integer key;
	public TagMergingWorker(Map<Integer,Tag> tagMapA, Map<Integer,Tag> tagMapB, Integer key){
		this.tagA = tagMapA.get(key);
		this.tagB = tagMapB.get(key);
		this.tagMapA = tagMapA;
		this.key = key;

	}

	@Override
	public void run() {
		try {
			processCommand();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void processCommand() throws InterruptedException {
		
		if(tagA.equals(tagB)){
			tagA.matchedPeptides.addAll(tagA.matchedPeptides);
			tagA.leftMutations.putAll(tagB.leftMutations);
			tagA.rightMutations.putAll(tagB.rightMutations);
			tagA.leftModifications.putAll(tagB.leftModifications);
			tagA.rightModifications.putAll(tagB.rightModifications);
			tagMapA.put(key, tagA);
		}else {
			System.out.println("Tags are not the same and can�t be merged.");
		}

	}

	@Override
	public String toString(){
		return this.tagA.toString()+"_"+this.tagB.toString();
	}
}

