package model.tag;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.mapdb.HTreeMap;

import model.Peptide;
import model.ProteinLib;

public class TagIndexingWorker implements Runnable {

	private String tagSequence;
	private ProteinLib proteinLib;
	public HTreeMap<Integer, List<Integer>> index;

	public TagIndexingWorker(String tagSequence, ProteinLib proteinLib, HTreeMap<Integer, List<Integer>> index){
		this.tagSequence = tagSequence;
		this.proteinLib = proteinLib;
		this.index = index;
	}

	@Override
	public void run() {
		try {
			processCommand();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void processCommand() throws InterruptedException {
		HashMap<Integer, Peptide> peptides =  proteinLib.peptides;
		List<Integer> peptidesList = new ArrayList<Integer>();

		for(Integer peptideID : peptides.keySet()) {
			Peptide peptide = peptides.get(peptideID);
			if(peptide.contains(tagSequence)) {
				peptidesList.add(peptideID);				
			}
		}
		index.put(tagSequence.hashCode(), peptidesList);
	}

	@Override
	public String toString(){
		return tagSequence+"_"+tagSequence.hashCode();
	}
}


