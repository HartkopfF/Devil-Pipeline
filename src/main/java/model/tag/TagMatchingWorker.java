package model.tag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import model.Peptide;
import model.ProteinLib;

public class TagMatchingWorker implements Runnable {

	private Integer threadNumber;
	private ProteinLib proteinLib;
	private Tag tag;
	private double tolerance;
	private double meanAaScore;
	private Map<Integer, Tag> tagList;
	private List<Integer> peptides;

	public TagMatchingWorker(Integer threadNumber, Tag tag,  ProteinLib proteinLib, double tolerance, double meanAaScore, Map<Integer, Tag> tagList, List<Integer> peptides){
		this.threadNumber = threadNumber;
		this.tag = tag;
		this.proteinLib = proteinLib;
		this.tolerance = tolerance;
		this.meanAaScore = meanAaScore;
		this.tagList = tagList;
		this.peptides = peptides;
	}

	@Override
	public void run() {
		try {
			processCommand();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void processCommand() throws InterruptedException {
		if (tag.getScore() > meanAaScore) {
			tagList.put(threadNumber, tag);
			tag = tagList.get(threadNumber);			

			for (Integer peptideID : peptides) {

				Peptide peptide = proteinLib.getPeptide(peptideID);
				String sequence = peptide.getSequence();

				List<Integer> positions = new ArrayList<Integer>();

				for (int i = -1; (i = sequence.indexOf(tag.getSequence(), i + 1)) != -1; i++) {
					positions.add(i);
				}
				// Check if all matches are valid or within tolerance
				for (Integer position : positions) {

					Boolean leftSide = Tag.inToleranceLeftSide(tag, position, sequence, tolerance);
					Boolean rightSide = Tag.inToleranceRightSide(tag, position, sequence, tolerance);

					// Filter out matches that are only the tag
					// sequence

					if (leftSide & rightSide){
						// Calculate position in protein for digested
						// proteins

						// Set matched sequence
						tag.matchedPeptides.add(peptideID);
						
					}
					leftSide = null;
					rightSide = null;
				}
				positions = null;
			}
		}
		tagList.put(threadNumber, tag);
		tag = null;
	}

	@Override
	public String toString(){
		return this.tag.toString();
	}
}