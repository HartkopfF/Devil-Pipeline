package model.tag;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.Set;

import org.mapdb.DB;
import org.mapdb.HTreeMap;

import model.denovo.DeNovoHit;
import model.denovo.NovorHit;
import model.tag.Tag.TagType;
import util.Masses;
import util.ProgressBar;
import util.StringUtilities;

/**
 * TODO: API!
 *
 */
public class TagGenerator {

	///////////////
	// Variables //
	///////////////

	/**
	 * DeNovoHit hit.
	 */
	private DeNovoHit hit;

	/**
	 * Holds the minimum N-terminal tag length.
	 */
	private int minLength;

	/**
	 * Holds the minimum C-terminal tag length.
	 */
	private int maxLength;

	/**
	 * List of peptide tags.
	 */
	private Set<Tag> tags;

	/////////////////
	// Constructor //
	/////////////////

	/**
	 * Constructs the TagGenerator object, it takes a list of denovo hits as seed.
	 *
	 * @param hit       Denovo hit as seed for the tag generation.
	 * @param minLength  Minimum length of the tag. 
	 * @param maxLength  Maximum length of the tag.
	 */
	public TagGenerator(DeNovoHit hit, int minLength, int maxLength) {
		this.hit = hit;
		this.minLength = minLength;
		this.maxLength = maxLength;

		tags = new LinkedHashSet<Tag>();

		// Auto-generate tags.
		tags.addAll(generateTags(hit.getSequence()));
	}

	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	/**
	 * Returns the generated tags.
	 * @return Generated tags
	 */
	public List<Tag> getTags() {
		return new ArrayList<>(tags);
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Shuffles the tag sequence to account for de novo sequencing inversions.
	 * @param tag Sequence tag
	 * @return Set of 
	 */
	public Set<Tag> shuffle(Tag tag) {
		Set<Tag> shuffledSet = new LinkedHashSet<>();
		String seq = tag.getSequence();

		String shuffled1 = "", shuffled2 = "";
		shuffled1 += seq.substring(1, 2) + seq.substring(0, 1) + seq.substring(2, 3);
		shuffled2 += seq.substring(0, 1) + seq.substring(2, 3) + seq.substring(1, 2);
		shuffledSet.add(new Tag(shuffled1, tag.getCharge(), tag.getSpectrumId()));
		shuffledSet.add(new Tag(shuffled2, tag.getCharge(), tag.getSpectrumId()));
		return shuffledSet;
	}

	/**
	 * Generates a list of tags from a de novo peptide sequence.
	 */
	private Set<Tag> generateTags(String denovoSeq) {
		Set<Tag> tags = new LinkedHashSet<>();
		for (int tagLength = minLength; tagLength <= maxLength; tagLength++) {
			for (int index = 0; index <= denovoSeq.length() - tagLength; index++) {
				String tagSequence = denovoSeq.substring(index, tagLength + index);

				Tag tag = null;

				double tagMass = Masses.calculateSequenceMass(tagSequence);
				double leftMass = Masses.calculateSequenceMass(denovoSeq.substring(0, index));
				double rightMass = Masses.calculateSequenceMass(denovoSeq.substring(tagLength + index, denovoSeq.length()));

				// Calculate amino acid based score for tag
				String[] aaScoreString = hit.getAaScore().split("-");
				double aaScore = 0;
				for(int i = index; i < tagLength + index;i++){
					aaScore = aaScore + Integer.parseInt(aaScoreString[i]);
				}
				aaScore = aaScore/tagLength;

				tag = new Tag(tagSequence, hit.getCharge(), tagMass, leftMass, rightMass,aaScore, hit.getMz());
				tag.setDenovoSequence(denovoSeq);
				tag.setSpectrumId(hit.getSpectrumId());
				if(index == 0){
					tag.setTagType(TagType.N_TERMINAL);
				}else if(index == denovoSeq.length()-tagLength){
					tag.setTagType(TagType.C_TERMINAL);
				}else{
					tag.setTagType(TagType.MIDDLE);
				}
				tags.add(tag);
			}
		}
		return tags;
	}

	////////////////////
	// Static Methods //
	////////////////////

	/**
	 * Method that creates tags for a hash map of NovorHits
	 * @param novorReader
	 * @param tags 
	 * @param db 
	 */
	public static void generateTagsForList(Map<Integer, NovorHit> novorReader, HTreeMap<Integer, Tag> tags, DB db){
		int progress = 0;
		int counter = 0;
		long startTime = System.currentTimeMillis();

		for (Entry<Integer, NovorHit> entry : novorReader.entrySet()) {
			NovorHit value = entry.getValue();
			TagGenerator tagGenerator =  new TagGenerator(value, 3, 3);
			List<Tag> tagList = tagGenerator.getTags();
			for(Tag tag : tagList) {
				tags.put(counter++, tag);
			}
			ProgressBar.printProgress(startTime, novorReader.size(), ++progress);
			db.commit();
		}
	}
	/**
	 * Method that creates tags for a hash map of NovorHits
	 * @param novorReader
	 * @param tags 
	 * @param db 
	 */
	public static void generateTagsForList(Map<Integer, NovorHit> novorReader, HTreeMap<Integer, Tag> tags, DB db, int cores){
		System.out.println("Starting generation of tags...");

		int progress = 0;
		long startTime = System.currentTimeMillis();

		// Creates the threadpool
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor( cores, cores, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>() );

		for (Entry<Integer, NovorHit> entry : novorReader.entrySet()) {
			NovorHit novorHit = entry.getValue();
			Runnable worker = new TagGeneratorWorker(novorHit, tags, progress);
			threadPool.execute(worker);
		}

		threadPool.shutdown();

		while (!threadPool.isTerminated()) {
			// Progress bar
			int finishedWorkers = novorReader.size() - threadPool.getQueue().size()  ;
			if(finishedWorkers>0) {
				ProgressBar.printProgress(startTime, novorReader.size(), finishedWorkers);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.out.println("Error! Database commits can�t hibernate.");
					e.printStackTrace();
				}
				db.commit();
			}
		}

		System.out.println("\nFinished all threads");

		System.out.println("Amount Tags: "+tags.size());

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Tag generation is completed...");
		System.out.println("Total tag generation time: "+StringUtilities.millisToShortDHMS(elapsedTime)+"\n");
	}

}
