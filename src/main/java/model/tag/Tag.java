package model.tag;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.mapdb.DB;
import org.mapdb.HTreeMap;


import model.Modification;
import model.Mutation;
import model.Peptide;
import model.ProteinLib;
import model.hit.ProteinHit;
import model.hit.ProteinHit.Origin;
import util.Masses;
import util.Pair;
import util.ProgressBar;
import util.StringUtilities;

/**
 * This class holds a sequence tag, i.e. a short (e.g. 3-6 amino acids long) version of a peptide sequence.
 * 
 * TAG: NTerm-leftMass-TAG-rightMass-CTerm
 *
 * @author Thilo Muth
 */
public class Tag implements Comparable<Tag>,Serializable,Cloneable {

	///////////////
	// Variables //
	///////////////

	/**
	 * 
	 */
	private static final long serialVersionUID = -149179340807322510L;

	/**
	 * This variable holds the formatted sequence without mass gaps.
	 */
	private String sequence;

	/**
	 * This variable hold the de novo sequence. For example calculated by Novor.
	 */
	private String denovoSequence;

	/**
	 * The total mass of the tag.
	 */
	private double tagMass;

	/**
	 * The mass from the amino acid(s) left of the tag (direction of the n-terminal)
	 */
	private double leftMass;

	/**
	 * The mass from the amino acid(s) right of the tag (direction of the c-terminal)
	 */
	private double rightMass;

	/**
	 * Spectrum id.
	 */
	private int spectrumId;

	/**
	 * PepMass of spectrum.
	 */
	private double pepMass;

	/**
	 * Tag type.
	 */
	private TagType tagType;

	/**
	 * Tag charge.
	 */
	private int charge;

	/**
	 * Tag score.
	 */
	private double score;

	/**
	 * List of peptides that are a match for this tag.
	 */
	public List<Integer> matchedPeptides;

	/**
	 * List that contains possible Mutations on the left side of the tag.
	 */
	TreeMap<Integer,List<Mutation>> leftMutations;

	/**
	 * List that contains possible Mutations on the right side of the tag.
	 */
	TreeMap<Integer,List<Mutation>> rightMutations;

	/**
	 * List that contains possible Modifications on the left side of the tag.
	 */
	TreeMap<Integer,Modification> leftModifications;

	/**
	 * List that contains possible Modifications on the right side of the tag.
	 */
	TreeMap<Integer,Modification> rightModifications;

	/**
	 * TagType enum: N-terminal / C-terminal.
	 */
	public enum TagType {
		N_TERMINAL, MIDDLE, C_TERMINAL
	}

	//////////////////
	// Constructors //
	//////////////////

	/**
	 * Constructs a tag with sequence and charge.
	 * @param sequence Sequence of tag
	 * @param charge charge state of spectra
	 * @param spectrumId ID of spectrum used in mgf 
	 */
	public Tag(String sequence, int charge, int spectrumId) {
		this.sequence = sequence;
		this.charge = charge;
		this.spectrumId = spectrumId;

	}

	/**
	 * Constructs a tag with sequence, charge, start and end position of the tag in the peptide sequence.
	 * @param sequence Sequence of tag
	 * @param charge charge state of spectra
	 * @param tagMass Mass of the tag
	 * @param leftMass Mass of the left side of the peptide
	 * @param rightMass Mass of the right side of the peptide
	 * @param aaScore Mean amino acid based core of the Tag
	 * @param mz Precursor mass of the peptide
	 */
	public Tag(String sequence, int charge, double tagMass, double leftMass, double rightMass, double aaScore, double mz) {        
		this.sequence = sequence;
		this.charge = charge;
		this.tagMass = tagMass;
		this.leftMass = leftMass;
		this.rightMass = rightMass;
		this.score = aaScore;
		this.pepMass = mz;

		// Initiate lists
		this.matchedPeptides = new ArrayList<Integer>();

		// Initiate heaps
		this.rightMutations = new TreeMap<Integer,List<Mutation>>();
		this.leftMutations = new TreeMap<Integer,List<Mutation>>();
		this.rightModifications = new TreeMap<Integer,Modification>();
		this.leftModifications = new TreeMap<Integer,Modification>();

	}


	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	/**
	 * Returns the formatted sequence with gaps.
	 *
	 * @return The formatted sequence.
	 */
	public String getSequence() {
		return sequence;
	}

	/**
	 * The total mass of the tag: sum of all masses within the sequence.
	 *
	 * @return The total mass of the tag.
	 */
	public double getTotalMass() {
		double sum = tagMass+leftMass+rightMass;
		return sum;
	}

	/**
	 * The total mass of the tag: sum of all masses within the sequence.
	 *
	 * @return The total mass of the tag.
	 */
	public double getTotalMassWithCharge() {
		Masses masses = Masses.getInstance();
		double totalMass = getTotalMass();
		totalMass += masses.get("C_term");
		totalMass += masses.get("N_term") + charge * Masses.Hydrogen;
		return totalMass / charge;
	}

	/**
	 * Returns the mass of the tag itself.
	 * @return Mass of the tag itself.
	 */
	public double getTagMass() {
		return tagMass;	
	}

	/**
	 * Returns the mass left of the tag.
	 * @return Mass left of the tag.
	 */
	public double getLeftMass() {
		return leftMass;
	}

	/**
	 * Returns the mass right of the tag.
	 * @return Mass right of the tag.
	 */
	public double getRightMass() {
		return rightMass;
	}

	/**
	 * Returns the length of the tag.
	 * 
	 * @return Tag length
	 */
	public int getLength() {
		return sequence.length();
	}

	/**
	 * Returns the tag charge.
	 * @return Tag charge.
	 */
	public int getCharge() {
		return charge;
	}

	/**
	 * Sets the tag score.
	 * @param score Tag score.
	 */
	public void setScore(double score) {
		this.score = score;
	}

	/**
	 * Returns the tag score.
	 * @return Tag score.
	 */
	public double getScore() {
		return score;
	}

	/**
	 * Returns the spectrum id.
	 * @return Spectrum id.
	 */
	public int getSpectrumId() {
		return spectrumId;
	}

	/**
	 * Sets the spectrum id.
	 * @param spectrumId Spectrum id.
	 */
	public void setSpectrumId(int spectrumId) {
		this.spectrumId = spectrumId;
	}

	/**
	 * @return the pepMass
	 */
	public double getPepMass() {
		return pepMass;
	}

	/**
	 * @param pepMass the pepMass to set
	 */
	public void setPepMass(int pepMass) {
		this.pepMass = pepMass;
	}

	/**
	 * @param pepMass the pepMass to set
	 * @return 
	 */
	public List<Integer> getPeptides() {
		return this.matchedPeptides;
	}


	/**
	 * @param key accession of the protein
	 * @return the leftMutations
	 */
	public List<Mutation> getLeftMutations(Integer key) {
		return leftMutations.get(key);
	}

	/**
	 * @return all leftMutations
	 */
	public TreeMap<Integer, List<Mutation>> getAllLeftMutations() {
		return  leftMutations;
	}

	/**
	 * @param leftMutations the leftMutations to set
	 */
	public void setLeftMutations(Integer key, List<Mutation> leftMutations) {
		this.leftMutations.put(key, leftMutations);
	}

	/**
	 * @return all rightMutations
	 */
	public TreeMap<Integer, List<Mutation>> getAllRightMutations() {
		return rightMutations;
	}

	/**
	 * @param key accession of the protein
	 * @return the leftMutations
	 */
	public List<Mutation> getRightMutations(Integer key) {
		return rightMutations.get(key);
	}

	/**
	 * @param rightMutations the rightMutations to set
	 */
	public void setRightMutations(Integer key, List<Mutation> rightMutations) {
		this.rightMutations.put(key, rightMutations);
	}

	/**
	 * @return the leftModifications
	 */
	public Modification getLeftModifications(Integer key) {
		return this.leftModifications.get(key);
	}

	/**
	 * @return all leftModifications
	 */
	public TreeMap<Integer, Modification> getAllLeftModifications() {
		return leftModifications;
	}

	/**
	 * @param leftModifications the leftModifications to set
	 */
	public void setLeftModifications(Integer peptideID, Modification leftModifications) {
		this.leftModifications.put(peptideID, leftModifications);
	}

	/**
	 * @return the rightModifications
	 */
	public Modification getRightModifications(Integer key) {
		return this.rightModifications.get(key);
	}

	/**
	 * @return all rightModifications
	 */
	public TreeMap<Integer, Modification> getAllRightModifications() {
		return rightModifications;
	}

	/**
	 * @param rightModifications the rightModifications to set
	 * @param key Key of the new PTM
	 */
	public void setRightModifications(Integer key, Modification rightModifications) {
		this.rightModifications.put(key, rightModifications);
	}

	/**
	 * @return the denovoSequence
	 */
	public String getDenovoSequence() {
		return denovoSequence;
	}

	/**
	 * @param denovoSequence the denovoSequence to set
	 */
	public void setDenovoSequence(String denovoSequence) {
		this.denovoSequence = denovoSequence;
	}

	/**
	 * Returns the tag type.
	 * @return Tag type.
	 */
	public TagType getType() {
		return tagType;
	}

	public void setTagType(TagType tagType){
		this.tagType = tagType;
	}   


	///////////////////
	// Class Methods //
	///////////////////
	/**
	 * Compares the object to another tag by its score, necessary for sorting of tags by their scores.
	 * @param tag Another tag.
	 * @return Integer value
	 */
	public int compareTo(Tag tag) {
		if (this.score > tag.getScore()) {
			return -1;
		} else if (this.score < tag.getScore()) {
			return 1;
		} else {
			// Only same-score and same-sequence tags are considered as equal.
			if (this.sequence.equalsIgnoreCase(tag.getSequence())) {
				return 0;
			} else {
				return -1;
			}
		}
	}

	/**
	 * Method to calculate hamming distance and finally the ratio between hamming distance and length of sequence.
	 * @param sequence
	 * @author Felix Hartkopf
	 * @return hammingRatio
	 */
	public double hammingRatio(String sequence) {
		int hammingDist = 0;
		double hammingRatio = 0;
		if(this.denovoSequence.length()==sequence.length()){
			for (int x = 0; x < this.denovoSequence.length(); x++) { //both are of the same length
				if (this.denovoSequence.charAt(x) != sequence.charAt(x)) {
					hammingDist += 1;
				}
			}
			hammingRatio = 1- hammingDist/(double) this.denovoSequence.length();
		}		

		return hammingRatio;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				append(rightMass).
				append(spectrumId).
				append(sequence).
				append(leftMass).
				toHashCode();
	}    

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof Tag))
			return false;
		if (obj == this)
			return true;

		Tag tag = (Tag) obj;
		return new EqualsBuilder().
				append(spectrumId, tag.spectrumId).
				append(sequence, tag.sequence).
				append(leftMass, tag.leftMass).
				append(rightMass, tag.rightMass).
				isEquals();
	}

	@Override
	public String toString() {
		return this.leftMass+"_"+this.sequence+"_"+this.rightMass;
	}


	@Override
	protected Tag clone() throws CloneNotSupportedException {
		return (Tag) super.clone();
	}
	////////////////////
	// Static Methods //
	////////////////////

	/**
	 * 
	 * @param tags List of Tag objects
	 * @param fastaPath Path to fasta file
	 * @param tolerance Tolerance used while matching
	 * @param meanAaScore Minimum for filtering by mean amino acid based scores
	 * @throws StringIndexOutOfBoundsException
	 * @throws IOException
	 * @author Felix Hartkopf
	 * @param maxAbsError 
	 * @param proteinLib2 
	 * @throws InterruptedException 
	 */

	@SuppressWarnings("unchecked")
	public static Map<Integer, Tag> matchTagsToProteins(DB db, Map<Integer,Tag> tagList, HTreeMap<Integer, Tag> tags, ProteinLib proteinLib, HTreeMap<Integer, List<Integer>> tagIndex, double tolerance, double meanAaScore, int cores) throws StringIndexOutOfBoundsException, IOException, InterruptedException{
		System.out.println("Please wait... Matching tags to proteins...");		

		int progress = 1;
		long startTime = System.currentTimeMillis();

		// Creates the threadpool
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor( cores, cores, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>() );

		// Iterate trough all Tag objects
		Set<Integer> keys = (Set<Integer>) tags.keySet();
		for (Integer key : keys) {
			Tag tmpTag = tags.get(key);
			List<Integer> peptides = tagIndex.get(tmpTag.getSequence().hashCode());
			Runnable worker = new TagMatchingWorker(progress-1, tmpTag,  proteinLib, tolerance, meanAaScore, tagList, peptides);
			threadPool.execute(worker);
			progress++;



		}

		threadPool.shutdown();
		int commitThreshold = 500;
		int commitThresholdStep = 500;

		while (!threadPool.isTerminated()) {
			// Progress bar
			int finishedWorkers = tags.size() - threadPool.getQueue().size()  ;
			if(finishedWorkers>0) {
				ProgressBar.printProgress(startTime, tags.size(), finishedWorkers);
				Thread.sleep(500);
			}
			if(finishedWorkers > commitThreshold){
				db.commit();
				commitThreshold+=commitThresholdStep;
			}

		}

		System.out.println("\nFinished all threads");

		System.out.println("Amount Tags: "+tags.size());

		if (tagList.size() <= 0) {
			System.out.println("\nERROR: The List of tags is empty...\nPlease choose a lower amino acid based score.\n!!!Returning empty list!!!\n");
		} 

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Completed matching tags.");
		System.out.println("Total tag matching time: "+StringUtilities.millisToShortDHMS(elapsedTime)+"\n");

		return tagList;

	}

	/**
	 * Check if left side is in tolerance
	 * @param tmpTag
	 * @param position
	 * @param sequence
	 * @return true or false
	 * @author Felix Hartkopf
	 */
	public static Boolean inToleranceLeftSide(Tag tmpTag, int position,String sequence,double tolerance){

		double leftMass = tmpTag.getLeftMass();
		double toleranceMass = leftMass*tolerance;
		Boolean inTolerance = false;
		String seqSplit = sequence.substring(0,position);

		if(Masses.calculateSequenceMass(seqSplit)<leftMass+toleranceMass & Masses.calculateSequenceMass(seqSplit)>leftMass-toleranceMass) {
			inTolerance = true;
		}

		return inTolerance;
	}

	/**
	 * Check if right side is in tolerance
	 * @param tmpTag
	 * @param position
	 * @param sequence
	 * @return true or false
	 * @author Felix Hartkopf
	 */
	public static Boolean inToleranceRightSide(Tag tmpTag, int position,String sequence,double tolerance){

		double rightMass = tmpTag.getRightMass();
		double toleranceMass = rightMass*tolerance;
		Boolean inTolerance = false;
		String seqSplit = sequence.substring(position+tmpTag.getLength());

		if(Masses.calculateSequenceMass(seqSplit)<rightMass+toleranceMass & Masses.calculateSequenceMass(seqSplit)>rightMass-toleranceMass) {
			inTolerance = true;
		}

		return inTolerance;

	}

	/**
	 * Method exports Tag to tsv file
	 * @param tsvName Name of output TSV file
	 * @throws IOException
	 * @author Felix Hartkopf
	 * @param tagList 
	 */
	public static void exportTagToTSV(Map<Integer, Tag> tagList, String tsvName, ProteinLib proteinLib) throws IOException {
		// Create output folder
		File dir = new File("output/Tag/");
		dir.mkdirs();

		File file = new File("output/Tag/"+tsvName);
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));


		bw.write("sequence"+"\t"+"tagMass"+"\t"+"leftMass"+"\t"+"rightMass"+"\t"+"tagType"+"\t"+"charge"+"\t"+"score"+"\t"+"spectrumId"+"\t"+"number of peptides"+"\t"+"matched peptides"+"\n");
		for(Integer tagID : tagList.keySet()){
			Tag tmpTag = tagList.get(tagID);

			String sequence = tmpTag.getSequence();
			double tagMass = tmpTag.getTagMass();
			double leftMass = tmpTag.getLeftMass();
			double rightMass = tmpTag.getRightMass();
			int matchCount = tmpTag.matchedPeptides.size();
			TagType tagType = tmpTag.getType();
			int charge = tmpTag.getCharge();
			double score = tmpTag.getScore();
			int spectrumId = tmpTag.getSpectrumId();


			bw.write(sequence+"\t"+tagMass+"\t"+leftMass+"\t"+rightMass+"\t"+tagType+"\t"+charge+"\t"+score+"\t"+spectrumId+"\t"+matchCount+"\t");

			for(Integer peptideID : tmpTag.matchedPeptides) {
				bw.write(proteinLib.getPeptide(peptideID).toString()+"|");
			}
			bw.write("\n");
		}
		bw.close();
	}

	/**
	 * Method exports Tag to tsv file
	 * @param tsvName Name of TSV file
	 * @throws IOException
	 * @author Felix Hartkopf
	 * @param tagList 
	 */
	public static void exportFullTSV(Map<Integer, Tag> tagList, String tsvName, ProteinLib proteinLib) throws IOException {
		System.out.println("Exporting all tags...");
		// Create output folder
		File dir = new File("output/Tag/");
		dir.mkdirs();

		File file = new File("output/Tag/"+tsvName);
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));

		bw.write("left mass"+"\t"+"tag mass"+"\t"+"right mass"+"\t"+"precursor"+"\t"+"charge"+"\t"+"precursor/charge"+"\t"+"left mass shift"+"\t"+"right mass shift"+"\t"+"sequence"+"\t"+"peptide info"+"\t"+"matched sequence"+"\t"+"de novo sequence"+"\t"+"peptide length"+"\t"+"accessions"+"\t"+"spectrum ID"+"\t"+"protein"+"\t"+"left mutations"+"\t"+"left mass difference"+"\t"+"right mutations"+"\t"+"right mass difference"+"\t"+"left modification"+"\t"+"right modification"+"\n");
		Set<Integer> tagIDs = tagList.keySet();
		for(Integer tagID : tagIDs) {
			Tag tmpTag = tagList.get(tagID);
			List<Integer> peptides = tmpTag.getPeptides();
			String tagSeq = tmpTag.getSequence();
			int spectrumId = tmpTag.getSpectrumId();

			for(Integer peptideID : peptides) {
				Peptide tmpPeptide = proteinLib.getPeptide(peptideID);	
				List<Mutation> leftMutations = tmpTag.getLeftMutations(peptideID);
				List<Mutation> rightMutations = tmpTag.getRightMutations(peptideID);
				Pair<Double,Double> massShift = tmpPeptide.calculateMassShift(tmpTag);
				double leftMassShift = massShift.getLeft();
				double rightMassShift = massShift.getRight();

				// General tag information
				bw.write(tmpTag.getLeftMass()+"\t"+tmpTag.tagMass+"\t"+tmpTag.getRightMass()+"\t"+tmpTag.pepMass+"\t"+tmpTag.charge+"\t"+tmpTag.pepMass*tmpTag.charge+"\t"+leftMassShift+"\t"+rightMassShift+"\t"+tagSeq+"\t"+tmpPeptide.toString()+"\t"+tmpPeptide.getSequence()+"\t"+tmpTag.denovoSequence+"\t"+tmpPeptide.getLength()+"\t"+tmpPeptide.getAccessions()+"\t"+spectrumId+"\t"+peptideID+"\t");

				// Mutations
				if(leftMutations != null){
					for(Mutation tmpMut:leftMutations){
						bw.write(tmpMut.toString()+" | ");
					}
					bw.write("\t"+leftMutations.get(0).getDeclaredMassShift());
				}else{
					bw.write("\t"+"");
				}

				bw.write("\t");

				if(rightMutations != null){
					for(Mutation tmpMut:rightMutations){
						bw.write(tmpMut.toString()+" | ");
					}
					bw.write("\t"+rightMutations.get(0).getDeclaredMassShift());
				}else{
					bw.write("\t"+"");
				}


				// Modifications

				if(tmpTag.getLeftModifications(peptideID) != null){
					bw.write("\t"+tmpTag.getLeftModifications(peptideID).toString());
				}else{
					bw.write("\t");
				}

				if(tmpTag.getRightModifications(peptideID) != null){
					bw.write("\t"+tmpTag.getRightModifications(peptideID).toString()+"\n");
				}else{
					bw.write("\t"+"\n");
				}

			}
		}
		bw.close();
	}


	/**
	 * Method to extract perfect matches Tags from list of matched tags
	 * @param tagList List of Tags
	 * @param maxAbsError Threshold for the absError
	 * @return List with list of perfect matches followed by weak matches that need further processing
	 * @throws IOException
	 */
	public static List<List<Integer>> extractPerfectMatches(Map<Integer, Tag> tagList, double maxAbsError, ProteinLib proteinLib) throws IOException {
		System.out.println("Extract perfect matches from tags ...");

		List<Integer> perfectMatches = new ArrayList<Integer>();
		List<Integer> weakMatches = new ArrayList<Integer>();

		int progress = 0;
		long startTime = System.currentTimeMillis();

		for (Entry<Integer, Tag> entry : tagList.entrySet()){

			// Progress bar
			progress++;
			ProgressBar.printProgress(startTime, tagList.size(), progress);
			Tag tag = entry.getValue();

			double bestAbsError = Double.POSITIVE_INFINITY;
			List<Integer> matchedPeptides = tag.getPeptides();

			for(Integer peptideID : matchedPeptides) {
				Peptide tmpPeptide = proteinLib.getPeptide(peptideID);	
				Pair<Double,Double> massShift = tmpPeptide.calculateMassShift(tag);
				double leftMassShift = massShift.getLeft();
				double rightMassShift = massShift.getRight();
				double absError = Math.abs(leftMassShift)+Math.abs(rightMassShift);

				if(absError<bestAbsError){
					bestAbsError = absError;
				}
			}

			if(bestAbsError<maxAbsError ){
				perfectMatches.add(entry.getKey());
			}else{
				weakMatches.add(entry.getKey());
			}

		}
		List<List<Integer>> results = new ArrayList<List<Integer>>();
		results.add(perfectMatches);
		results.add(weakMatches);
		return(results); 
	}

	/**
	 * Method to extract perfect matches Tags from list of matched tags
	 * @param tagList List of Tags
	 * @param db 
	 * @param maxAbsError Threshold for the absError
	 * @param weakMatches2 
	 * @param perfectMatches2 
	 * @throws IOException
	 * @throws InterruptedException 
	 */
	public static void extractPerfectMatches(Map<Integer, Tag> tagList, ProteinLib proteinLib , DB db, double maxAbsError, HTreeMap<Integer, Tag> perfectMatches, HTreeMap<Integer, Tag> weakMatches, int cores) throws IOException, InterruptedException {
		System.out.println("Extract perfect matches from tags ...");

		long startTime = System.currentTimeMillis();

		// Creates the threadpool
		ExecutorService threadPool = Executors.newFixedThreadPool(cores);

		for (Entry<Integer, Tag> entry : tagList.entrySet()){

			// Merging Tags
			Integer key = entry.getKey();
			Tag tag = entry.getValue();
			Runnable worker = new TagExtractingWorker(key, tag, proteinLib, maxAbsError, perfectMatches, weakMatches);
			threadPool.execute(worker);
		}

		threadPool.shutdown();

		while (!threadPool.isTerminated()) {
			Thread.sleep(500);
			// Progress bar
			if(perfectMatches.size()+weakMatches.size()>0) {
				ProgressBar.printProgress(startTime, 2*tagList.size(), perfectMatches.size()+weakMatches.size());
			}
		}
		System.out.println("\nFinished all threads");

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Completed extracting tags.");
		System.out.println("Total tag extracting time: "+StringUtilities.millisToShortDHMS(elapsedTime));
		System.out.println("Amount of weak matching tags: "+weakMatches.size());
		System.out.println("Amount of perfect matching tags: "+perfectMatches.size());
	}


	/**
	 * Method to link tags to specific proteins.
	 * @param tagList List of tags that should be processed.
	 * @author Felix Hartkopf
	 * @param perfectMatches 
	 * @return 
	 */
	public static List<ProteinHit> linkTagsToProteins(Map<Integer, Tag> tagList, ProteinLib proteinLib){
		System.out.println("Linking tags to proteins...");
		TreeMap<String,ProteinHit> proteins = new TreeMap<String,ProteinHit>();
		// Iterate Tags
		for(Integer key : tagList.keySet()) {
			Tag tag = tagList.get(key);
			List<Integer> matchedPeptides = tag.getPeptides();
			List<String> matchedProteins = new ArrayList<String>();
			for(Integer peptideID : matchedPeptides) {
				matchedProteins.addAll(proteinLib.getPeptide(peptideID).getAccessions());
			}
			// Iterate matched proteins
			for(String accession : matchedProteins){
				if(!proteins.containsKey(accession)){
					ProteinHit tmpProt  = new ProteinHit(proteinLib.getProtein(accession).getHeader(),proteinLib.getProtein(accession).getSequence());
					tmpProt.setOrigin(Origin.DENOVO);
					proteins.put(accession, tmpProt);
				}
				proteins.get(accession).addTag(tag.getSpectrumId()+"_"+tag.getSequence(), tag);

			}	
		}
		List<ProteinHit> exportList = new ArrayList<ProteinHit>(proteins.values());
		return(exportList);
	}

	/**
	 * Method to merge two Tags.
	 * @author Felix Hartkopf
	 * @param tagA
	 * @param tagB
	 * @return merged Tag
	 */
	public static Tag mergeTags(Tag tagA, Tag tagB){

		if(tagA.equals(tagB)){
			tagA.leftMutations.putAll(tagB.leftMutations);
			tagA.rightMutations.putAll(tagB.rightMutations);
			tagA.leftModifications.putAll(tagB.leftModifications);
			tagA.rightModifications.putAll(tagB.rightModifications);
		}else {
			System.out.println("Tags are not the same and can�t be merged.");
		}

		return tagA;

	}

	/**
	 * Method to merge two Map<Integer, Tag> of Tags.
	 * @author Felix Hartkopf
	 * @param tagMapA
	 * @param tagMapB
	 * @param cores Amount of used cores/threads
	 * @return merged Tag Map<Integer, Tag>
	 * @throws InterruptedException 
	 */
	public static Map<Integer,Tag> mergeTagMaps(DB db, Map<Integer,Tag> tagMapA, Map<Integer,Tag> tagMapB, int cores) throws InterruptedException{
		long startTime = System.currentTimeMillis();
		long progress = 1;

		// Creates the threadpool
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor( cores, cores, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>() );

		// Iterate trough all Tag objects
		System.out.println("Adding tags to task list for merging...");
		Set<Integer> keys = tagMapA.keySet();
		for (int key : keys){
			// Merging Tags
			Runnable worker = new TagMergingWorker(tagMapA, tagMapB, key);
			threadPool.execute(worker);
			// Progress bar
			ProgressBar.printProgress(startTime, tagMapA.size(), progress++);
			System.out.println(worker.toString());


		}

		threadPool.shutdown();

		int commitThreshold = 500;
		int commitThresholdStep = 500;

		while (!threadPool.isTerminated()) {
			int finishedWorkers = tagMapA.size() - threadPool.getQueue().size();

			if(finishedWorkers > commitThreshold){
				db.commit();
				commitThreshold+=commitThresholdStep;
			}

		}
		System.out.println("\nFinished all threads");

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Completed merging tags.");
		System.out.println("Total tag merging time: "+StringUtilities.millisToShortDHMS(elapsedTime));


		return tagMapA;
	}

	boolean isMiddle() {
		return this.tagType == TagType.MIDDLE;
	}
	boolean isCTerminal() {
		return this.tagType == TagType.C_TERMINAL;
	}
	boolean isNTerminal() {
		return this.tagType == TagType.N_TERMINAL;
	}





}
