package model.hit;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import model.Peptide;
import model.ProteinLib;
import model.tag.Tag;

import com.compomics.util.protein.Header;

import analysis.ProteinAnalysis;

/**
 * <b>ProteinHit</b>
 * <p>
 * This class represents an identified protein hit.
 * It contains information about accession, description and a list of containing peptide hits.
 * </p>
 *
 * @author T.Muth
 */
public class ProteinHit {

	///////////////
	// Variables //
	///////////////

	/**
	 * Protein accession.
	 */
	private String accession;

	/**
	 * Protein description.
	 */
	private String description;

	/**
	 * Protein family.
	 */
	private String family;

	/**
	 * Tag type.
	 */
	private Origin origin;

	/**
	 * The species of the protein
	 */
	private String species;

	/**
	 * Peptide hits for the protein.
	 */
	private TreeMap<String, PeptideHit> peptideHits;

	/**
	 * Tags that match the protein.
	 */
	private TreeMap<String, Tag> tags;

	/**
	 * The amino acid sequence of the protein.
	 */
	private String sequence;

	/**
	 * The molecular weight.
	 */
	private double molWeight = -1;

	/**
	 * The coverage of the peptides matched on the protein sequence.
	 */
	private double coverage = -1;

	/**
	 * The coverage sequence of the peptides matched on the protein sequence.
	 */
	private String coverageSequence = "";

	/**
	 * DataType enum: Database / DeNovo.
	 */
	public enum Origin {
		DATABASE, DENOVO
	}

	/////////////////
	// Constructor //
	/////////////////

	/**
	 * basic Constructor for a protein hit.
	 *
	 * @param accession Protein accession
	 * 
	 */
	public ProteinHit(String accession) {
		this.accession = accession;
		this.tags = new  TreeMap<String, Tag>();
		this.peptideHits = new TreeMap<String, PeptideHit>();
	}

	/**
	 * small Constructor for a protein hit.
	 *
	 * @param header Protein header
	 * @param sequence Protein sequence string
	 * 
	 */
	public ProteinHit(String header, String sequence) {
		this.accession = Header.parseFromFASTA(header).getAccession();
		if(this.accession==null){
			this.accession = header; 
		}
		this.description = Header.parseFromFASTA(header).getDescription();
		if(this.description==null){
			this.description = header; 
		}
		this.family = descriptionDecoder(header)[0];
		this.species = descriptionDecoder(header)[1];
		this.sequence = sequence;
		this.peptideHits = new TreeMap<String, PeptideHit>();
		this.tags = new  TreeMap<String, Tag>();
	}

	/**
	 * Constructor for a protein hit with peptide hits.     *
	 * @param header Protein header
	 * @param sequence Protein sequence string
	 * @param peptideHit Peptide identification hit
	 *
	 */
	public ProteinHit(String header, String sequence, PeptideHit peptideHit) {
		this.accession = Header.parseFromFASTA(header).getAccession();
		this.description = Header.parseFromFASTA(header).getDescription();
		this.family = descriptionDecoder(header)[0];
		this.species = descriptionDecoder(header)[1];
		this.sequence = sequence;
		this.peptideHits = new TreeMap<String, PeptideHit>();
		this.peptideHits.put(peptideHit.getSequence(), peptideHit);
		this.tags = new  TreeMap<String, Tag>();
	}

	/**
	 * Constructor for a protein hit with tags.
	 *
	 * @param header Protein header
	 * @param sequence Protein sequence string
	 * @param tag Object of the class Tag
	 * @param sequence This holds the sequence of the protein
	 * @param header This variable holds the header of the protein
	 *
	 */
	public ProteinHit(String header, String sequence, Tag tag) {
		this.accession = Header.parseFromFASTA(header).getAccession();
		this.description = Header.parseFromFASTA(header).getDescription();
		this.family = descriptionDecoder(header)[0];
		this.species = descriptionDecoder(header)[1];
		this.sequence = sequence;
		this.tags = new TreeMap<String, Tag>();
		this.tags.put(tag.getSequence(), tag);
	}

	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	/**
	 * Returns the accession.
	 *
	 * @return accession The accession of a protein.
	 */
	public String getAccession() {
		return accession;
	}

	/**
	 * Returns the protein family.
	 *
	 * @return family The protein family.
	 */
	public String getFamily() {
		return family;
	}

	/**
	 * Returns the description.
	 *
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * Sets the description
	 *
	 * @param description
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * Returns the species of the protein
	 *
	 * @return species
	 */
	public String getSpecies() {
		return species;
	}

	/**
	 * Sets the species of the protein
	 *
	 * @param species
	 */
	public void setSpecies(String species) {
		this.species = species;
	}


	/**
	 * Gets the protein's amino acid sequence.
	 *
	 * @return The protein sequence.
	 */
	public String getSequence() {
		return sequence;
	}

	/**
	 * Sets the protein's amino acid sequence
	 *
	 * @param sequence The sequence of the protein.
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	/**
	 * Returns a list of peptide hits.
	 *
	 * @return The list of peptide hits.
	 */
	public TreeMap<String, PeptideHit> getPeptideHits() {
		return peptideHits;
	}

	/**
	 * Returns the peptide hits as list.
	 *
	 * @return The peptide hits as list.
	 */
	public List<PeptideHit> getPeptideHitList() {
		return new ArrayList<PeptideHit>(peptideHits.values());
	}

	/**
	 * Sets the list of peptide hits.
	 *
	 * @param peptideHits The list of peptide hits to set.
	 */
	public void setPeptideHits(TreeMap<String, PeptideHit> peptideHits) {
		this.peptideHits = peptideHits;
	}

	/**
	 * Adds one peptide to the protein hit.
	 *
	 * @param peptidehit
	 */
	public void addPeptideHit(PeptideHit peptidehit) {
		peptideHits.put(peptidehit.getSequence(), peptidehit);
	}

	/**
	 * Convenience method to retrieve a unique peptide hit.
	 */
	public PeptideHit getSinglePeptideHit() {
		return peptideHits.firstEntry().getValue();
	}

	/**
	 * Returns the calculated molecular protein weight.
	 *
	 * @return molecular protein weight
	 */
	public double getMolWeight() {
		if (molWeight < 0.0) {
			molWeight = ProteinAnalysis.calculateMolecularWeight(this);
		}
		return molWeight;
	}


	/**
	 * Getter method of the coverage
	 * @return coverage
	 * @author Felix Hartkopf
	 */
	public double getCoverage() {
		return coverage;
	}

	/**
	 * Setter method of the coverage
	 * @param coverage
	 * @author Felix Hartkopf
	 */
	public void setCoverage(double coverage) {
		this.coverage = coverage;
	}

	/**
	 * Getter method of the coverage sequence
	 * @return the coverageSequence
	 * @author Felix Hartkopf
	 */
	public String getCoverageSequence() {
		return coverageSequence;
	}

	/**
	 * Setter method of the coverage sequence
	 * @param coverageSequence The coverageSequence to set	 
	 * @author Felix Hartkopf
	 */
	public void setCoverageSequence(String coverageSequence) {
		this.coverageSequence = coverageSequence;
	}



	/**
	 * Method that return the amount of PSMs that are matches so the PeptideHit that are matched to this Protein
	 * @return the amountPSMs
	 * 
	 */
	public int getAmountPSMs() {
		Set<Integer> spectrumIDs = new TreeSet<>();
		int spectralCount = 0;

		if(this.origin == Origin.DATABASE) {
			for(Entry<String, PeptideHit> entry : peptideHits.entrySet()) {
				// @TODO This method part needs to be checked
				PeptideHit peptide = entry.getValue();
				spectralCount = spectralCount + peptide.getAmountPSMs();
			}
		}else if(this.origin == Origin.DENOVO){
			for(Entry<String, Tag> entry : tags.entrySet()) {
				Tag tag = entry.getValue();
				spectrumIDs.add(tag.getSpectrumId());
			}
		}

		return spectrumIDs.size();
	}

	/**
	 * Method that return the amount of sequences that are matches so the tags that are matched to this Protein
	 * @return the amountPSMs
	 * 
	 */
	public int getAmountTags(ProteinLib proteinLib) {
		int spectralCount = 0;
		int numberTags = 0;

		for(Entry<String, Tag> entry : tags.entrySet()) {
			Tag tag = entry.getValue();
			List<Integer> matchedPeptides = tag.getPeptides();
			for(Integer key :matchedPeptides){
				Peptide peptide = proteinLib.getPeptide(key);
				peptide.getAccessions();
				if(peptide.contains(accession)){
					numberTags++;
				}
			}
			spectralCount = spectralCount + numberTags;
		}

		return spectralCount;
	}

	/**
	 * @param key This holds the key for the tag
	 * @param tag the tags to set
	 */
	public void addTag(String key, Tag tag) {
		this.tags.put(key, tag);	
	}

	/**
	 * @return the tags
	 */
	public TreeMap<String, Tag> getTags() {
		return tags;
	}

	/**
	 * Method that returns all PSMs of a protein.
	 * @return the amountPSMs
	 * 
	 */
	public TreeMap<String, PSM> getAllPSMs() {
		TreeMap<String, PSM> PSMs = new TreeMap<String, PSM>();
		for(Entry<String, PeptideHit> entry : peptideHits.entrySet()) {
			PeptideHit peptide = entry.getValue();
			PSMs.putAll(peptide.getSpectrumMatches());
		}
		return PSMs;
	}

	/**
	 * Returns the tag type.
	 * @return Tag type.
	 */
	public Origin getOrigin() {
		return origin;
	}

	/**
	 * 
	 * @param origin
	 */
	public void setOrigin(Origin origin){
		this.origin = origin;
	}   

	/**
	 * 
	 */
	@Override
	public String toString() {
		return accession+"|"+family+"|"+species+"|"+coverage+"|"+peptideHits.size()+"|"+tags.size();
	}
	////////////////////
	// Static Methods //
	////////////////////

	/**
	 * Method that extracts protein family and species of Protein
	 * @param description
	 * @return Array of Strings with {family,species}
	 * @author Felix Hartkopf
	 */
	public static String[] descriptionDecoder(String description){
		String[] strings = null;
		String regex = "[a-zA-Z0-9]*_+[a-zA-Z0-9]*";

		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(description);

		if (matcher.find())
		{
			String match = matcher.group(0);
			strings = Pattern.compile("_", Pattern.LITERAL).split(match);
		}

		return strings;
	}

	/**
	 * Method to calculate the coverage for a List of ProteinHits
	 * @param proteinList
	 * @author Felix Hartkopf
	 */
	public static void proteinListCoverage(List<ProteinHit> proteinList, ProteinLib proetinLib){
		System.out.println("Calculating coverage...");
		Iterator<ProteinHit> proteinIterator = proteinList.iterator();
		while (proteinIterator.hasNext()) {
			ProteinHit tmpProtein = proteinIterator.next();
			proteinCoverage(tmpProtein, proetinLib);
		}

	}


	/**
	 * Method to calculate coverage for one protein(ProteinHit) and the linked peptides.
	 * @param protein
	 * @author Felix Hartkopf
	 */
	public static void proteinCoverage(ProteinHit protein, ProteinLib proetinLib){
		String proteinSequence = protein.getSequence().toLowerCase();
		String accession = protein.getAccession();

		if(protein.getOrigin()==Origin.DENOVO){
			TreeMap<String,Tag> tags = protein.getTags();
			Set<String> keys = tags.keySet();
			// Iterate tags of protein
			for(String key : keys){
				Tag tmpTag = tags.get(key);
				List<Integer> matchedPeptides = tmpTag.getPeptides();

				// Iterate hits of tag
				for(Integer peptideID : matchedPeptides){
					Peptide peptide = proetinLib.getPeptide(peptideID);
					Set<String> accessions = peptide.getAccessions();
					if(accessions.contains(accession)){
						String peptideSeq = peptide.getSequence().toUpperCase();
						int start = proteinSequence.toLowerCase().indexOf(peptideSeq.toLowerCase());
						int end = start + peptideSeq.length();		
						try {
							proteinSequence = proteinSequence.substring(0,start)+peptide.getSequence()+proteinSequence.substring(end);
						}
						catch(StringIndexOutOfBoundsException exception) {
							System.out.println("Error during coverage calculation. "+peptideSeq+"\t"+proteinSequence );
						}

					}
				}
			}

		}else if(protein.getOrigin()==Origin.DATABASE){
			List<PeptideHit> peptideList = protein.getPeptideHitList();

			for(int pos = 0; pos < proteinSequence.length();pos++){
				Iterator<PeptideHit> peptideIterator = peptideList.iterator();
				while (peptideIterator.hasNext()) {
					PeptideHit tmpPeptide = peptideIterator.next();
					int start = tmpPeptide.getStart(protein.getAccession());
					int end = tmpPeptide.getEnd(protein.getAccession());

					if(start==pos){
						String replaceString = proteinSequence.substring(start, end+1).toUpperCase(); 
						proteinSequence = proteinSequence.substring(0,start)+replaceString+proteinSequence.substring(end+1);
						peptideIterator.remove();
					}
				}
			}
		}

		double coverage = calculateCoverage(proteinSequence);
		protein.setCoverage(coverage);
		protein.setCoverageSequence(proteinSequence);

	}

	/**
	 * Method that calculates coverage out of a String. Upper case characters are a match
	 * and lower case characters are a mismatch.
	 * @param proteinSequence
	 * @return double between 0.0 and 1.0. -1 is the for empty strings
	 * @author Felix Hartkopf
	 */
	public static double calculateCoverage(String proteinSequence){
		double coverage = -1.0;

		double count = 0;
		for(int i = 0; i < proteinSequence.length(); i++){
			if(Character.isUpperCase(proteinSequence.charAt(i))){
				count++;
			}
		}
		coverage = count/proteinSequence.length();

		return coverage;
	}

	/**
	 * Method to link all PeptidHits to the matching ProteinsHits
	 * @param peptideList
	 * @param fastaPath
	 * @return list of ProteinHit objects
	 * @throws StringIndexOutOfBoundsException
	 * @throws IOException
	 * @author Felix Hartkopf
	 */

	public static List<ProteinHit> linkPeptideHitsToProteinHits(List<PeptideHit> peptideList,String fastaPath) throws StringIndexOutOfBoundsException, IOException{
		System.out.println("Please wait... Importing FASTA database "+fastaPath+"...");

		List<ProteinHit> proteinList = new ArrayList<ProteinHit>();

		FileReader fastaFile = new FileReader(fastaPath);

		BufferedReader in = new BufferedReader(fastaFile);
		String line;
		String header = null;
		while((line=in.readLine())!=null){ // one loop is one protein

			String sequence = "";

			if(line.startsWith(">")){ // header 
				header = line;
			}


			while(!line.startsWith(">")){ // sequence
				sequence = sequence + line;
				line=in.readLine();
				if(line==null){
					break;
				}
			}


			// Linking Peptides to current Protein
			Iterator<PeptideHit> peptideIterator = peptideList.iterator();
			TreeMap<String, PeptideHit> matchList = new TreeMap<String, PeptideHit>();
			while (peptideIterator.hasNext()) {
				PeptideHit tmpPeptide = peptideIterator.next();
				String peptideSeq = tmpPeptide.getSequence();
				int match = sequence.indexOf(peptideSeq);
				if (match != -1) {
					String key = Header.parseFromFASTA(header).getAccession();
					if(key==null){
						key = header; 
					}
					tmpPeptide.setStart(key,match);
					tmpPeptide.setEnd(key,match+peptideSeq.length()-1);
					matchList.put(tmpPeptide.getSequence(), tmpPeptide);
				}
			}

			// Initialize ProteinHit and add it to list of proteins
			if(matchList.size()>0){
				ProteinHit tmpProtein = new ProteinHit(header, sequence);
				tmpProtein.setOrigin(Origin.DATABASE);
				tmpProtein.setPeptideHits(matchList);
				proteinList.add(tmpProtein);
			}

			if(line!=null){ // next header
				if(line.startsWith(">")){
					header = line;
				}
			}	
		}
		in.close();
		return proteinList;
	}

	/**
	 * Method exports ProteinList to tsv file
	 * @param proteinList
	 * @throws IOException
	 * @author Felix Hartkopf
	 */
	public static void exportProteinsToTSV(List<ProteinHit> proteinList, String tsvPath) throws IOException {
		System.out.println("Exporting on protein level...");
		// Create output folder
		File dir = new File("output/ProteinHits/");
		dir.mkdirs();

		File file = new File(tsvPath);
		BufferedWriter bw = new BufferedWriter(new FileWriter(file));
		Iterator<ProteinHit> proteinIterator = proteinList.iterator();
		bw.write("accession"+"\t"+"description"+"\t"+"family"+"\t"+"species"+"\t"+"coverage"+"\t"+"spectralCountPSM"+"\t"+"countTags"+"\t"+"sequence"+"\t"+"coverageSequence"+"\t"+"peptides"+"\t"+"peptideSequences"+"\t"+"#peptides"+"\n");
		while(proteinIterator.hasNext()){
			ProteinHit tmpProtein = proteinIterator.next();
			String accession = tmpProtein.getAccession();
			String description = tmpProtein.getDescription();
			String family = tmpProtein.getFamily();
			String species = tmpProtein.getSpecies();
			String sequence = tmpProtein.getSequence();
			int spectralCount = tmpProtein.getAmountPSMs();
			int countTags = tmpProtein.getTags().size();
			double coverage = tmpProtein.getCoverage();
			String coverageSequence = tmpProtein.getCoverageSequence();
			TreeMap<String, PeptideHit> peptideHits = tmpProtein.getPeptideHits();

			if(coverage>0){
				bw.write(accession+"\t"+description+"\t"+family+"\t"+species+"\t"+coverage+"\t"+spectralCount+"\t"+countTags+"\t"+sequence+"\t"+coverageSequence+"\t");

				for(Entry<String, PeptideHit> entry : peptideHits.entrySet()) {
					PeptideHit value = entry.getValue();	  
					bw.write(value.getSequence()+"_at_"+value.getStart(accession)+":"+value.getEnd(accession)+"|");
				}

				bw.write("\t");

				for(Entry<String, PeptideHit> entry : peptideHits.entrySet()) {
					PeptideHit value = entry.getValue();	  
					bw.write(value.getSequence()+" ");
				}

				int countPeptide = peptideHits.size();
				bw.write("\t"+countPeptide+"\n");
			}	
		}
		bw.close();
	}


	/**
	 * Method that returns all PSMs of a list of proteins.
	 * @return the amountPSMs
	 * 
	 */
	public static TreeMap<String, PSM> getAllPSMsOfList(List<ProteinHit> proteinList) {
		TreeMap<String, PSM> PSMs = new TreeMap<String, PSM>();
		for(int protein= 0; protein<proteinList.size();protein++) {
			PSMs.putAll(proteinList.get(protein).getAllPSMs());
		}
		return PSMs;
	}

	/**
	 * Method that returns all PSMs of a list of proteins.
	 * @param proteinList List of objects of the class ProteinHit
	 * @param newSpecies String that overwrites the current species
	 * 
	 */
	public static void unifySpecies(List<ProteinHit> proteinList,String newSpecies) {
		for(int protein= 0; protein<proteinList.size();protein++) {
			proteinList.get(protein).setSpecies(newSpecies);
		}
	}

}




