package model.hit;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 * <b>PeptideHit</b>
 * <p>
 * This class represents a peptide hit.
 * </p>
 *
 * @author Thilo Muth
 */
public class PeptideHit {

	///////////////
	// Variables //
	///////////////

	/**
	 * The peptide sequence.
	 */
	private String sequence;

	/**
	 * The start of the peptide sequence in the protein.
	 */
	private TreeMap<String, Integer> start;

	/**
	 * The end of the peptide sequence in the protein.
	 */
	private TreeMap<String, Integer> end;

	/**
	 * The peptide spectrum match for this peptide hit.
	 */
	private TreeMap<String, PSM> spectrumMatches;

	/////////////////
	// Constructor //
	/////////////////    

	/**
	 * PeptideHit constructor, taking the sequence as only parameter.
	 *
	 * @param sequence      The String sequence.
	 * @param psm The peptide spectrum match
	 */
	public PeptideHit(String sequence, PSM psm) {
		this.sequence = sequence;
		this.spectrumMatches = new TreeMap<String, PSM>();
		this.start = new TreeMap<String, Integer>();
		this.end = new TreeMap<String, Integer>();
		if (psm != null)
			this.spectrumMatches.put(psm.getSpectrumTitle(), psm);
	}

	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	/**
	 * Returns the peptide sequence.
	 *
	 * @return the sequence
	 */
	public String getSequence() {
		return sequence;
	}

	/**
	 * Sets the peptide sequence
	 *
	 * @param sequence the peptide sequence
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}

	/**
	 * Convenience method to retrieve a unique PSM.
	 *
	 * @return The list of PSMs.
	 */
	public PSM getSingleSpectrumMatch() {
		return spectrumMatches.firstEntry().getValue();
	}

	/**
	 * Returns the spectrum matches as tree map.
	 *
	 * @return The spectrum matches.
	 */
	public TreeMap<String, PSM> getSpectrumMatches() {
		return spectrumMatches;
	}

	/**
	 * Returns the spectrum matches as list.
	 * @return The spectrum matches list.
	 */
	public List<PSM> getSpectrumMatchesList() {
		return new ArrayList<PSM>(spectrumMatches.values());
	}

	/**
	 * Sets the spectrum matches.
	 *
	 * @param spectrumMatches The spectrum matches to set.
	 */
	public void setSpectrumMatches(TreeMap<String, PSM> spectrumMatches) {
		this.spectrumMatches = spectrumMatches;
	}

	/**
	 * Returns the peptide start position. Key is the accession.
	 *
	 * @return the peptide start position.
	 */
	public int getStart(String key) {
		return start.get(key);
	}

	/**
	 * Returns the peptide end. Key is the accession.
	 *
	 * @return the end
	 */
	public int getEnd(String key) {
		return end.get(key);
	}

	/**
	 * Sets the start position of the peptide.
	 *
	 * @param start The start position of the peptide.
	 */
	public void setStart(String key, int start) {
		this.start.put(key,start);
	}

	/**
	 * Sets the end position of the peptide.
	 *
	 * @param end The end position of the peptide.
	 */
	public void setEnd(String key, int end) {
		this.end.put(key,end);
	}



	/**
	 * Sets the start position of the peptide.
	 *
	 * @param start The start position of the peptide.
	 */
	public void setStart(int start) {
		this.start.put("default",start);
	}

	/**
	 * Sets the end position of the peptide.
	 *
	 * @param end The end position of the peptide.
	 */
	public void setEnd(int end) {
		this.end.put("default",end);
	}

	/**
	 * Returns the peptide start position. Key is "default".
	 *
	 * @return the peptide start position.
	 */
	public int getStart() {
		return start.get("default");
	}

	/**
	 * Returns the peptide end. Key is "default".
	 *
	 * @return the end
	 */
	public int getEnd() {
		return end.get("default");
	}

	/**
	 * Method that return the amount of PSMs that are matches so the Peptidehit
	 * @return the amountPSMs
	 * 
	 */
	public int getAmountPSMs() {
		return spectrumMatches.size();
	}

	/**
	 * Add PSM object to Peptide object
	 * @param psm
	 * @author Felix Hartkopf
	 */
	public void addPSM(PSM psm){
		this.spectrumMatches.put(psm.getSpectrumTitle(), psm);
	}

	///////////////////
	// Class Methods //
	///////////////////

	//@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PeptideHit other = (PeptideHit) obj;
		if (sequence == null) {
			if (other.sequence != null)
				return false;
		} else if (!sequence.equals(other.sequence))
			return false;
		return true;
	}

	////////////////////
	// Static Methods //
	////////////////////

	/**
	 * Link all PSMs of a MSGFplus result file to Peptide objects
	 * @param msgfPSMs
	 * @return List of Peptides
	 * @throws IOException
	 * @author Felix Hartkopf
	 */
	public static List<PeptideHit> linkPSMtoPeptide(List<PSM> msgfPSMs, double fdr) throws IOException { 

		List<PeptideHit> peptideList = new ArrayList<PeptideHit>();
		List<PSM> unidentifiedPSMs = new ArrayList<PSM>();


		for(int psmIndex = 0 ; psmIndex < msgfPSMs.size();psmIndex=0 ){
			boolean found = false;
			PSM tmpPSM = msgfPSMs.get(psmIndex);
			if(tmpPSM.getQvalue()<fdr){
				PeptideHit tmpPeptide = new PeptideHit(msgfPSMs.get(psmIndex).getPeptide(), msgfPSMs.get(psmIndex)); 

				for(int dbIndex = 0 ; dbIndex < peptideList.size(); dbIndex++ ){
					PeptideHit tmpPeptideDB = peptideList.get(dbIndex);

					if (tmpPeptide.equals(tmpPeptideDB)){
						tmpPeptideDB.addPSM(tmpPSM);
						msgfPSMs.remove(tmpPSM);
						found = true;
						break;
					}
				}
				if(!found){
					peptideList.add(tmpPeptide);
					msgfPSMs.remove(tmpPSM);
				}
			}else{
				unidentifiedPSMs.add(tmpPSM);
				msgfPSMs.remove(tmpPSM);
			}

		}

		return peptideList;
	}

}

