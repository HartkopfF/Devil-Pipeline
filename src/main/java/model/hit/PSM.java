package model.hit;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Arrays;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

/**
 * <b>PSM</b>
 * <p>
 * This class holds information about a peptide-spectrum match (PSM).
 * It contains spectrum title, number of matched peaks, score and mass error sum.
 * </p>
 *
 * @author Thilo Muth
 */
public class PSM implements Comparable<PSM> {

	///////////////
	// Variables //
	///////////////

	/**
	 * The Spectrum ID.
	 */
	private int spectrumId;

	/**
	 * The spectrum title.
	 */
	private String spectrumTitle;

	/**
	 * Original scanId from mgf file
	 */
	private int scanID;

	/**
	 * The precursor m/z. 
	 */
	private double precursorMz;

	/**
	 * The ion charge.
	 */
	private int charge;

	/**
	 * The peptide sequence.
	 */
	private String peptide;

	/**
	 * The protein accessions.
	 */
	private Set<String> proteins;

	/**
	 * The PSM score.
	 */
	private float score;

	/**
	 * Flag if the hit is decoy or not.
	 */
	private boolean decoy;

	/**
	 * The number of matched peaks (optional).
	 */
	private int numMatchedPeaks;

	/**
	 * The q-value of the PSM (optional)
	 */
	private float qvalue;

	/////////////////
	// Constructor //
	/////////////////

	/**
	 * Constructs a peptide-to-spectrum match.
	 *
	 * @param spectrumId The spectrum identifier
	 * @param spectrumTitle The spectrum title
	 * @param precursorMz The precursor m/z
	 * @param charge         The ion charge.
	 * @param score The chosen score.
	 * @param decoy Condition whether PSM is a decoy hit or not.
	 */
	public PSM(int spectrumId, String spectrumTitle, double precursorMz, int charge, String peptide, Set<String> proteins, float score, boolean decoy) {
		this.spectrumId = spectrumId;
		this.spectrumTitle = spectrumTitle;
		this.precursorMz = precursorMz;
		this.charge = charge;
		this.peptide = peptide;
		this.proteins = proteins;
		this.score = score;
		this.decoy = decoy;
		int scanNumberIndex = spectrumTitle.toLowerCase().indexOf("scan");
		String intValue = spectrumTitle.substring(scanNumberIndex,spectrumTitle.length()).replaceAll("[^0-9]", " "); 
		int scanNumber =  Integer.parseInt(Arrays.asList(intValue.trim().split(" ")).get(0)) ;
		this.scanID = scanNumber;
	}

	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	/**
	 * Returns the spectrum id.
	 * @return The spectrum id.
	 */
	public int getSpectrumId() {
		return spectrumId;
	}

	/**
	 * Returns the spectrum title.
	 * @return The spectrum title.
	 */
	public String getSpectrumTitle() {
		return spectrumTitle;
	}

	/**
	 * Returns the precursor m/z.
	 * @return The precursor m/z.
	 */
	public double getPrecursorMz() {
		return precursorMz;
	}

	/**
	 * Returns the charge.
	 * @return The PSM charge.
	 */
	public int getCharge() {
		return charge;
	}

	/**
	 * Returns the peptide sequence.
	 * @return The peptide sequence.
	 */
	public String getPeptide() {
		return peptide;
	}

	/**
	 * Returns the protein accession.
	 * @return The protein accession.
	 */
	public List<String> getProteins() {
		return new ArrayList<>(proteins);
	}	

	/**
	 * Returns the score.
	 * @return the score
	 */
	public float getScore() {
		return score;
	}  

	/**
	 * Returns the q-value of the PSM.
	 * @return q-value of the PSM
	 */
	public float getQvalue() {
		return qvalue;
	}

	/**
	 * Sets the q-value of the PSM.
	 * @param qvalue q-value of the PSM
	 */
	public void setQvalue(float qvalue) {
		this.qvalue = qvalue;
	}

	/**
	 * Returns the number of matched peaks.
	 * @return the number of matched peaks
	 */
	public int getNumberofMatchedPeaks() {
		return numMatchedPeaks;
	}

	/**
	 * returns scanID from mgf file
	 * @return
	 */
	public int getScanID() {
		return scanID;
	}

	/**
	 * set scanID from mgf file
	 * @param scanID
	 */
	public void setScanID(int scanID) {
		this.scanID = scanID;
	}

	/**
	 * Sets the number of matched peaks.
	 * @param numMatchedPeaks The number of matched peaks.
	 */
	public void setNumberofMatchedPeaks(int numMatchedPeaks) {
		this.numMatchedPeaks = numMatchedPeaks;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Flag for PSM being decoy or not.
	 * @return decoy flag
	 */
	public boolean isDecoy() {
		return decoy;
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 31). // two randomly chosen prime numbers
				append(spectrumId).
				append(peptide).
				toHashCode();
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof PSM))
			return false;
		if (obj == this)
			return true;

		PSM psm = (PSM) obj;
		return new EqualsBuilder().
				append(spectrumId, psm.spectrumId).
				append(peptide, psm.peptide).
				isEquals();
	}

	@Override
	public int compareTo(PSM psm) {
		if (this.qvalue < psm.getQvalue()) {
			return -1;
		} else if (this.qvalue > psm.getQvalue()) {
			return 1;
		} else {
			// For equal q-values: sort by the score value.
			if (this.score > psm.getScore()) {
				return -1;
			} else if (this.score < psm.getScore()) {
				return 1;
			}
		}
		return 0;
	}
}
