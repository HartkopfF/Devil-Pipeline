package model;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class Protein {

	/**
	 * Fasta header
	 */
	private String header;

	/**
	 * Sequence of the protein
	 */
	private String sequence;

	/**
	 * Peptide IDs of peptides
	 */
	private List<Integer> peptides;

	/** 
	 * Name of fasta file
	 */
	private Integer databaseID;

	public Protein(String header, String sequence, Integer databaseID){
		this.header = header;
		this.sequence = sequence; 
		this.databaseID = databaseID;
		this.peptides = new ArrayList<Integer>();
	}

	/**
	 * Getter method for sequence of a protein.
	 * @return sequence
	 */
	public String getSequence() {
		return this.sequence;
	}

	/**
	 * Getter method for the header of a protein.
	 * @return header
	 */
	public String getHeader() {
		return this.header;
	}

	/**
	 * Getter method for peptideIDs
	 * @return peptidesIDs
	 */
	public List<Integer> getPeptides() {
		return peptides;
	}
	
	/**
	 * Adder method for peptideIDs
	 * @param peptideID 
	 */
	public void addPeptide(Integer peptideID) {
		peptides.add(peptideID);
	}
	
	/**
	 * Getter method for databaseID
	 * @return peptidesIDs
	 */
	public Integer getDatabase() {
		return databaseID;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Protein other = (Protein) obj;
		if (sequence == null) {
			if (other.sequence != null)
				return false;
		} else if (!sequence.equals(other.sequence))
			return false;
		return true;
	}
	
	public String toString() {
		String accession = header;
		String[] splitHeader = header.split(Pattern.quote("|"));
		if(splitHeader.length==3){
			accession = splitHeader[1];
		}
		return accession+"_database:"+this.getDatabase()+"_Peptides:"+peptides.size();	
	}
}
