package model;
import java.util.List;

import org.mapdb.HTreeMap;

import model.tag.Tag;
import util.Pair;

public class ModificationWorker implements Runnable {


	private int tagID;
	private ProteinLib proteinLib;
	private HTreeMap<Integer, Tag> tagList;

	public ModificationWorker(ProteinLib proteinLib, HTreeMap<Integer, Tag> tagList, int tagID){
		this.proteinLib = proteinLib;
		this.tagList = tagList;
		this.tagID = tagID;
	}

	@Override
	public void run() {
		try {
			processCommand();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	private void processCommand() throws InterruptedException {
		Tag tag = tagList.get(tagID);
		List<Integer> matchedPeptides = tag.getPeptides();

		for(Integer peptideID : matchedPeptides) {
			Peptide tmpPeptide = proteinLib.getPeptide(peptideID);	

			// Get mass shift
			Pair<Double,Double> massShift = tmpPeptide.calculateMassShift(tag);
			double leftMass = massShift.getLeft();
			double rightMass = massShift.getRight();

			// Get possible modification		    	
			Modification leftMod = Modification.massShiftToModification(leftMass);
			Modification rightmod = Modification.massShiftToModification(rightMass);

			// put modification in Tag object 
			tag.setLeftModifications(peptideID, leftMod);
			tag.setRightModifications(peptideID, rightmod);
			
		}
		tagList.put(tagID, tag);
	}

	@Override
	public String toString(){
		return tagID+"_"+tagList.get(tagID).getSequence()+"_"+tagList.get(tagID).getSpectrumId();
	}
}