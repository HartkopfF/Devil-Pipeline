package model;

import java.util.NavigableMap;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.io.File;
import java.io.IOException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.mapdb.DB;
import org.mapdb.HTreeMap;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import model.tag.Tag;
import util.ProgressBar;
import util.StringUtilities;

import org.w3c.dom.Node;
import org.w3c.dom.Element;


/**
 * TODO: API!
 *
 */
public class Modification {

	///////////////
	// Variables //
	///////////////

	/**
	 * Short name of PTM, e.g. C+57: 
	 */
	private String shortName;

	/**
	 * Longer descriptive name of PTM.
	 */
	private String description;

	/**
	 * Monoisotopic mass difference of the PTM.
	 */
	private double monoMass;

	/**
	 * Average mass difference of the PTM.
	 */
	private double avgMass;

	/**
	 * Amount of Nitrogen-15 in the PTM.
	 */
	private int n15;

	/**
	 * Unimod identifier the PTM.
	 */
	private int unimod;

	/**
	 * Type of the PTM.
	 */
	private String type;

	/**
	 * One affected amino acids.
	 */
	private char aminoAcid;

	/**
	 * PSI-MS of PTM.
	 */
	private String psiMs;

	/////////////////
	// Constructor //
	/////////////////

	public Modification(String shortName, String description, double monoMass,double avgMass, int n15 , String type,char aminoAcid, int unimod, String psiMs ) {
		this.shortName = shortName;
		this.description = description;
		this.monoMass = monoMass;
		this.avgMass = avgMass;
		this.aminoAcid = aminoAcid;
		this.n15 = n15;
		this.type = type;
		this.unimod = unimod;
		this.psiMs = psiMs;
	}

	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	public String getShortName() {
		return shortName;
	}

	public String getDescription() {
		return description;
	}

	public double getMonoMass() {
		return monoMass;
	}

	public double getAvgMass() {
		return avgMass;
	}

	public char getAminoAcid() {
		return aminoAcid;
	}

	public int getN15() {
		return n15;
	}

	public String getType() {
		return type;
	}

	public int getUnimod() {
		return unimod;
	}

	public String getPsiMs() {
		return psiMs;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Overwritten toString method.
	 * @return The modification as string representation.
	 */
	public String toString() {
		return this.description+"_with_"+this.avgMass+"_at_"+this.aminoAcid;
	}

	/**
	 * Method to get the nearest modification for a mass shift
	 * @param massShift
	 * @return 
	 * @author Felix Hartkopf
	 */
	public static Modification massShiftToModification(double massShift){
		NavigableMap<Double, Modification> map = initModTreeMap();
		double key = 111111;
		if(map.lastKey()<massShift){
			key = map.lastKey();
		}else if(map.firstKey()>massShift){
			key = map.firstKey();
		}else{
			double above = map.ceilingKey(massShift);
			double below = map.floorKey(massShift);
			key = massShift - below > above - massShift ? above : below;
		}
		if(key==111111){
			System.out.println("Error in massShiftToModification()!");
			return null;
		}
		return map.get(key);
	}

	////////////////////
	// Static Methods //
	////////////////////
	/**
	 * Method that creates a library of Modifications.
	 * @return
	 * @author Felix Hartkopf
	 */
	public static NavigableMap<Double, Modification> initModTreeMap(){
		NavigableMap<Double, Modification> map = new TreeMap<>();

		String[][] mods = readModificationsFromXML("");

		for(int i = 0; i < mods.length; i++){
			Modification tmpModifictaion = new Modification(
					mods[i][1],
					mods[i][0],
					Double.parseDouble(mods[i][2]),
					Double.parseDouble(mods[i][3]),
					Integer.parseInt(mods[i][4]),
					mods[i][5],
					mods[i][6].charAt(0),
					Integer.parseInt(mods[i][7]),
					mods[i][8]);			
			map.put(tmpModifictaion.getAvgMass(), tmpModifictaion );
		}
		return map;
	}
	
	/**
	 * Method to get all possible tags with a possible modifications and which is valid on both sides of the tag
	 * @param tagList List of matched tags
	 */
	public static HTreeMap<Integer, Tag> getModificationForMatchedTags(HTreeMap<Integer, Tag> tagList, ProteinLib proteinLib, DB db, int cores){
		System.out.println("Calculating modifications...");
		long startTime = System.currentTimeMillis();

		// Creates the threadpool
		ThreadPoolExecutor threadPool = new ThreadPoolExecutor( cores, cores, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>() );

		@SuppressWarnings("unchecked")
		Set<Integer> keys = tagList.keySet();
		
		for(Integer tagID : keys){
			Runnable worker = new ModificationWorker(proteinLib, tagList, tagID);
			threadPool.execute(worker);
		}
				
		threadPool.shutdown();

		while (!threadPool.isTerminated()) {
			// Progress bar
			int finishedWorkers = tagList.size() - threadPool.getQueue().size()  ;
			if(finishedWorkers>0) {
				ProgressBar.printProgress(startTime, tagList.size(), finishedWorkers);
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {
					System.out.println("Error! Database commit can�t hibernate.");
					e.printStackTrace();
				}
				db.commit();
			}
		}

		System.out.println("\nFinished all threads");

		System.out.println("Amount Tags: "+tagList.size());

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Modification matching is completed...");
		System.out.println("Total modification matching time: "+StringUtilities.millisToShortDHMS(elapsedTime)+"\n");
		return tagList;
	}

	/**
	 * Method to read XML file with modifications
	 * @param pathXML path to XML file
	 */
	public static String[][] readModificationsFromXML(String pathXML){
		File file;

		if(pathXML == ""){
			file = new File("res/modifications/denovogui_mods.xml");
		}else{
			file = new File(pathXML);
		}

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document doc = db.parse(file);
			Element rootElement = doc.getDocumentElement();
			NodeList nList = rootElement.getElementsByTagName("MSModSpec");
			String[][] mods = new String[nList.getLength()][9];
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					mods[temp][0] = eElement.getElementsByTagName("MSModSpec_name").item(0).getTextContent();
					mods[temp][1] = ((Element) eElement.getElementsByTagName("MSModSpec_mod").item(0)).getElementsByTagName("MSMod").item(0).getAttributes().getNamedItem("value").getTextContent();
					mods[temp][2] = eElement.getElementsByTagName("MSModSpec_monomass").item(0).getTextContent();
					mods[temp][3] = eElement.getElementsByTagName("MSModSpec_monomass").item(0).getTextContent();
					mods[temp][4] = eElement.getElementsByTagName("MSModSpec_n15mass").item(0).getTextContent();
					mods[temp][5] = ((Element) eElement.getElementsByTagName("MSModSpec_type").item(0)).getElementsByTagName("MSModType").item(0).getAttributes().getNamedItem("value").getTextContent();
					if(((NodeList) eElement.getElementsByTagName("MSModSpec_residues")).getLength()>0) {
						mods[temp][6] = ((Element) eElement.getElementsByTagName("MSModSpec_residues").item(0)).getElementsByTagName("MSModSpec_residues_E").item(0).getTextContent();
					}else {
						mods[temp][6] = "X";
					}
			
					if(eElement.getElementsByTagName("MSModSpec_unimod").getLength()>0){
						mods[temp][7] = eElement.getElementsByTagName("MSModSpec_unimod").item(0).getTextContent();
					}else{
						mods[temp][7] = "0";
					}

					if(eElement.getElementsByTagName("MSModSpec_psi-ms").getLength()>0){
						mods[temp][8] = eElement.getElementsByTagName("MSModSpec_psi-ms").item(0).getTextContent();
					}else{
						mods[temp][8] = "0";
					}



				}		
			}

			return mods;

		} catch (ParserConfigurationException e) {
			System.out.println("There is something wrong with the modifications.xml.");
			e.printStackTrace();
		} catch (SAXException e) {
			System.out.println("There is something wrong with the modifications.xml.");
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("There is something wrong with the modifications.xml.");
			e.printStackTrace();
		}
		return null;		
	}
}
