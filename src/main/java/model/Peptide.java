package model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import model.tag.Tag;
import util.Masses;
import util.Pair;

/**
 * <b>Peptide</b>
 * <p>
 * Model class describing a peptide with sequence string and charge.
 * </p>
 * 
 * @author Felix Hartkopf
 *
 */
public class Peptide {

	///////////////
	// Variables //
	///////////////

	/**
	 * Positions in proteins of this peptide
	 */
	public HashMap<String,List<Pair<Integer,Integer>>> positions;

	/**
	 * Sequence of this peptide
	 */
	String sequence;
	
	/**
	 * Sequence of this peptide
	 */
	String mutated;

	//////////////////
	// Constructors //
	//////////////////

	/**
	 * Construct a peptide. 
	 * @param sequence
	 */
	public Peptide(String accession, String sequence, Pair<Integer, Integer> pair) {
		this.sequence = sequence;
		this.positions = new HashMap<String,List<Pair<Integer,Integer>>>();
		List<Pair<Integer,Integer>> pairList = new ArrayList<Pair<Integer,Integer>>();
		pairList.add(pair);
		this.positions.put(accession, pairList);
	}

	/**
	 * Construct a peptide. 
	 * @param sequence
	 */
	public Peptide(String sequence) {
		this.sequence = sequence;
	}


	/////////////////////////////
	// Setter & Getter Methods //
	/////////////////////////////

	/**
	 * Returns the peptide sequence as string.
	 * @return Peptide sequence string
	 */
	public String getSequence() {
		return sequence;
	}


	/**
	 * Returns the length of the peptide.
	 * @return the length of peptide
	 */
	public int getLength() {
		return sequence.length();
	}

	/**
	 * Method to add positions of possible peptides to library. 
	 * @param accession Accession of the protein.
	 * @param pair Positions of the peptides in the protein.
	 * @author Felix Hartkopf
	 */
	public void addPosition(String accession, Pair<Integer, Integer> pair ){
		if(this.positions.containsKey(accession)) {
			this.positions.get(accession).add(pair);
		}else {
			List<Pair<Integer,Integer>> pairList = new ArrayList<Pair<Integer,Integer>>();
			pairList.add(pair);
			this.positions.put(accession, pairList);
		}
	}

	/**
	 * Getter method for a peptide position for a accession
	 * @return 
	 */
	public List<Pair<Integer, Integer>> getPeptide(String accession){
		return this.positions.get(accession);
	}
	
	/**
	 * Getter method for a accession of all proetins
	 * @return 
	 */
	public Set<String> getAccessions(){
		return positions.keySet();
	}


	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Calculates the precursor mass.
	 * @return
	 */
	public double calculatePrecursorMass(int charge) {
		Masses masses = Masses.getInstance();
		double precursorMass = Masses.calculateSequenceMass(sequence);
		precursorMass += masses.get("C_term") + masses.get("N_term") + charge * Masses.Hydrogen;
		return precursorMass / charge;
	}

	/**
	 * Calculates the left mass shift of a tag and a peptide.
	 * Only splits at first occurrence.
	 *   Peptide: 	ABCDE
	 *   Tag: 		 BCD
	 *   LeftSide: 	A
	 *   RightSide: 	E
	 * @return
	 */
	public Pair<Double,Double> calculateMassShift(Tag tag) {
		String[] seqSplit = this.sequence.split(tag.getSequence(),2);
		Pair<Double, Double> pair = null;

		if(seqSplit.length == 2){
			double leftMassShift = Masses.calculateSequenceMass(seqSplit[0])-tag.getLeftMass();
			double rightMassShift = Masses.calculateSequenceMass(seqSplit[1])-tag.getRightMass();
			pair = new Pair<Double, Double>(leftMassShift,rightMassShift);
		}else if(seqSplit.length == 1){
			if(this.sequence.indexOf(tag.getSequence())==0){
				double rightMassShift = Masses.calculateSequenceMass(seqSplit[0])-tag.getRightMass();
				pair = new Pair<Double, Double>(0.0,rightMassShift);
			}else if(this.sequence.indexOf(tag.getSequence())==this.sequence.length()-tag.getSequence().length()){
				double leftMassShift = Masses.calculateSequenceMass(seqSplit[0])-tag.getLeftMass();
				pair = new Pair<Double, Double>(leftMassShift,0.0);
			}
		}else if(seqSplit.length == 0 ){
			System.out.println("Complete match of tag sequence in peptide sequence...\t"+this.sequence+"\t"+tag.getSequence());
			pair = new Pair<Double, Double>(0.0,0.0);
		}


		return pair;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Peptide other = (Peptide) obj;
		if (sequence == null) {
			if (other.sequence != null)
				return false;
		} else if (!sequence.equals(other.sequence))
			return false;
		return true;
	}

	@Override
	public int hashCode(){
		return Objects.hashCode(this.sequence);
	}

	public String toString() {
		return this.hashCode()+"_sequence:"+this.getSequence()+"_length:"+this.getLength()+"_proteins:"+positions.size();	
	}

	public Boolean contains(String subSequence) {
		if(this.sequence.contains(subSequence)) {
			return true;
		}else {
			return false;
		}
	}
}
