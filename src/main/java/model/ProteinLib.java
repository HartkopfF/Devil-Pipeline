package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.regex.Pattern;

import util.Digest;
import util.Pair;
import util.Pam;
import util.StringUtilities;

/**
 * This class holds the content of a FASTA file filled with proteins.
 *
 * @author Felix Hartkopf
 */
public class ProteinLib {

	///////////////
	// Variables //
	///////////////

	/**
	 * HashMap that holds the sequences of proteins with the accession as key.
	 */
	public HashMap<String,Protein> proteins;

	/**
	 * HashMap that holds peptides with hash key.
	 */
	public HashMap<Integer, Peptide> peptides;

	/**
	 * Minimum length of a digested peptide.
	 */
	public Integer minLength;

	/**
	 * Maximum of a digested peptide.
	 */
	public Integer maxLength;

	/**
	 * Number of allowed missed cleavages.
	 */
	public Integer missedCleavages;

	/**
	 * Databases
	 */
	private HashMap<Integer,String> databases;

	/**
	 * is digested
	 */
	Boolean isDigested = false;

	//////////////////
	// Constructors //
	//////////////////

	/**
	 * Minimal constructor to build a protein library
	 * @param fastaPath
	 * @author Felix Hartkopf
	 * @throws IOException 
	 */
	public ProteinLib(String fastaPath) throws IOException {
		System.out.println("Importing databases...");
		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("devil.config");
		config.load(file);

		// Load Properties
		this.missedCleavages = Integer.parseInt(config.getProperty("missedCleavages"));
		this.minLength = Integer.parseInt(config.getProperty("minLength"));
		this.maxLength = Integer.parseInt(config.getProperty("maxLength"));

		this.databases = new HashMap<Integer,String>();

		this.proteins = new HashMap<String,Protein>();
		this.peptides = new HashMap<Integer,Peptide>();

		readFastaFile(fastaPath);
	}

	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	/**
	 * Method to add a protein to the library.
	 * @param accession Accession of the protein.
	 * @param sequence Sequence of the protein.
	 * @author Felix Hartkopf
	 */
	public void addProtein(String accession, Protein protein){
		proteins.put(accession, protein);
		this.isDigested = false;
	}

	/**
	 * Method to add a HasMap of proteins to the library.
	 * @param proteins Proteins to be added.
	 * @author Felix Hartkopf
	 */
	public void addProteins(HashMap<String,String> proteins){
		proteins.putAll(proteins);
		this.isDigested = false;
	}

	/**
	 * Method to add a HasMap of headers to the library.
	 * @param proteins Proteins to be added.
	 * @author Felix Hartkopf
	 */
	public void addPeptides(HashMap<String,List<Pair<Integer,Integer>>> peptides){
		peptides.putAll(peptides);
	}

	/**
	 * Method to get all accessions of proteins in the library.
	 * @return Set of accessions.
	 * @author Felix Hartkopf
	 */
	public Set<String> getAllAccessions(){
		Set<String> accessions = proteins.keySet();
		return accessions;		
	}

	/**
	 * Method to return amount of proteins in library.
	 * @return Amount of proteins
	 * @author Felix Hartkopf
	 */
	public int size(){
		return proteins.size();
	}



	/**
	 * Method to get all positions of peptides in library of one protein. 
	 * @param accession Accession of the protein.
	 * @author Felix Hartkopf
	 * @return List of Pair that are the start and the end of a peptide
	 */
	public Peptide getPeptide(Integer peptideID){		
		return peptides.get(peptideID);
	}

	/**
	 * Method to convert a pair in actual amino acid sequence.
	 * @param accession Accession of the protein.
	 * @param pair Pair of the start and the end of the peptide.
	 * @author Felix Hartkopf
	 * @return The sequence of the peptide.
	 */
	public String pairToString(String accession, Pair<Integer, Integer> pair){	
		String sequence = proteins.get(accession).getSequence().substring(pair.getLeft(), pair.getRight());
		return sequence;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Method to digest all proteins in the library and store peptides.
	 * 
	 * @author Felix Hartkopf
	 */
	public HashMap<String,List<Pair<Integer,Integer>>> digestProteins(){

		System.out.println("Digesting proteins...");		

		// start record time
		long startTime = System.currentTimeMillis();

		HashMap<String,List<Pair<Integer,Integer>>> peptides = new HashMap<String,List<Pair<Integer,Integer>>>();

		// Iterate through proteins
		HashMap<String,Protein> proteins = this.proteins;
		Iterator<Entry<String, Protein>> iterator = proteins.entrySet().iterator();
		int countPeptides = 0;

		while (iterator.hasNext()) {
			Map.Entry<String, Protein> entry = (Map.Entry<String, Protein>)iterator.next();
			String accession = entry.getKey();
			Protein protein = entry.getValue();

			// Digest protein
			List<String> peptidesList = Digest.performTrypticCleavage(protein.getSequence(), this.minLength, this.maxLength, this.missedCleavages);

			// Get start and end positions
			for(String peptideSeq :peptidesList){
				Integer start = protein.getSequence().indexOf(peptideSeq);
				Integer end = start + peptideSeq.length();
				Pair<Integer,Integer> pair = new Pair<Integer,Integer>(start,end);
				Peptide peptide = new Peptide(accession, peptideSeq, pair);

				if(this.peptides.containsKey(peptide.hashCode()) & this.peptides.size()>0) {
					this.peptides.get(peptide.hashCode()).addPosition(accession, pair);
					protein.addPeptide(peptide.hashCode());
				}else {
					this.peptides.put(peptide.hashCode(), peptide);
					protein.addPeptide(peptide.hashCode());
					countPeptides++;
				}
			}	
		}

		this.isDigested = true;

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time for digesting proteins: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");		
		System.out.println("Peptides stored in protein library: "+countPeptides);	 
		return peptides; 
	}


	/**
	 * Method to write ProteinLib to a fasta file.
	 * @param filePath
	 * @author Felix Hartkopf
	 */
	public void writeFasta(String filepath){
		Set<String> keys = this.proteins.keySet();
		Path file = Paths.get(filepath);
		List<String> lines = new ArrayList<String>() ;

		// Create lines 
		for(String key : keys){
			lines.add(this.proteins.get(key).getHeader());
			List<String> plitSeq = new ArrayList<String>(Arrays.asList(proteins.get(key).getSequence().split("(?<=\\G.{60})")));
			lines.addAll(plitSeq);
		}

		// Write lines to File
		try {
			Files.write(file, lines, Charset.forName("UTF-8"));
		} catch (IOException e) {
			System.out.print("Can�t write to file "+filepath+".");
			e.printStackTrace();
		}

	}


	/**
	 * Method to write ProteinLib to a peptide file in MS2PIP format.
	 * @param filePath
	 * @author Felix Hartkopf
	 */
	public void writeMS2PIP(String filepath){
		Set<Integer> keys = this.peptides.keySet();
		// Create output folder
		File dir = new File("output/digested/");
		dir.mkdirs();
		File fout = new File(filepath);

		// Write lines to File
		try {
			System.out.println("Writing peptides in MS2PIP format to "+filepath+".");
			FileOutputStream fos = new FileOutputStream(fout);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(fos));
			bw.write("spec_id modifications peptide charge\n");
			int number = 0;
			for(Integer key : keys){
				Peptide tmpPeptides = this.peptides.get(key);
				String line = number+"_"+tmpPeptides.mutated+"_"+tmpPeptides.getSequence()+" "+"-"+" "+tmpPeptides.getSequence()+" "+2+"\n";
				bw.write(line);
				number++;
			}
			System.out.println("Writing of peptides complete.");
			bw.close();
		} catch (IOException e) {
			System.out.println("Can�t write to file "+filepath+".");
			e.printStackTrace();
		}

	}

	/**
	 * Method to mutate peptides in silico for simulation.
	 * @param filePath
	 * @author Felix Hartkopf
	 */
	public void inSilicoPeptideMutation(double mutationRate){
		Set<Integer> keys = this.peptides.keySet();

		System.out.println("Mutating peptides...");
		int number = 0;
		for(Integer key : keys){
			double randomDouble = Math.random();
			
			if(mutationRate>randomDouble) {
				System.out.println("Peptide is mutated with a chance of "+randomDouble);
				Peptide tmpPeptide = this.peptides.get(key);
				Random rn = new Random();
				int randomIndex = rn.nextInt(tmpPeptide.getLength());
				int randomAminoAcid = rn.nextInt(19);
				String target = Pam.getAminoAcid(randomAminoAcid);
				String oldSeq = tmpPeptide.sequence;
				while(target.charAt(0) == oldSeq.charAt(randomIndex)){
					randomAminoAcid = rn.nextInt(19);
					target = Pam.getAminoAcid(randomAminoAcid);
				}
				tmpPeptide.sequence = tmpPeptide.sequence.substring(0,randomIndex)+target+tmpPeptide.sequence.substring(randomIndex+1,tmpPeptide.getLength());
				if(tmpPeptide.mutated==null) {
					tmpPeptide.mutated = randomIndex+":"+oldSeq.charAt(randomIndex)+"->"+target;
				}else {
					tmpPeptide.mutated = tmpPeptide.mutated+";"+randomIndex+":"+oldSeq.charAt(randomIndex)+"->"+target;
				}
				System.out.println("randomIndex: "+randomIndex);
				System.out.println("randomAminoAcid: "+randomAminoAcid);
				System.out.println("oldAminoAcid: "+oldSeq.charAt(randomIndex));
				System.out.println("newAminoAcid: "+target);
				System.out.println("oldSeq: "+oldSeq);
				System.out.println("newSeq: "+tmpPeptide.sequence+"\n");
				this.peptides.put(key, tmpPeptide);
				number++;
			}
		}
		System.out.println("Mutation of "+number+" out of "+this.peptides.size()+" peptides is complete.");
		System.out.println("Observed mutation rate: "+(double) number/(double) this.peptides.size());

	}

	/**
	 * Reads fasta file and stores proteins in library
	 * @param fastaPath
	 * @author Felix Hartkopf
	 */
	public void readFastaFile(String fastaPath){

		System.out.println("Importing proteins...");		
		// start record time
		long startTime = System.currentTimeMillis();

		databases.put(databases.size(), fastaPath);
		int proteinCount = this.proteins.size();

		try {
			FileReader fastaFile;

			fastaFile = new FileReader(fastaPath);
			BufferedReader in = new BufferedReader(fastaFile);

			String line;
			String header = null;

			// one loop is one protein
			while((line=in.readLine())!=null){ 

				String sequence = "";

				// header of protein
				if(line.startsWith(">")){ 
					header = line;
				}

				// sequence of protein
				while(!line.startsWith(">")){ 
					sequence = sequence + line;
					line=in.readLine();
					if(line==null){
						break;
					}
				}

				// Get accession
				String accession = header;
				String[] splitHeader = header.split(Pattern.quote("|"));
				if(splitHeader.length==3||splitHeader.length==2){
					accession = splitHeader[1];
				}

				// Protein constructor
				Protein protein = new Protein(header, sequence, this.databases.size());

				// Add protein to library
				proteins.put(accession, protein);

				// next header
				if(line!=null&&line.startsWith(">")){ 
					header = line;
				}	
			}

			in.close();

		} catch (FileNotFoundException e) {
			System.out.println("Fasta file not found. Please check path.");		
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println("Error while reading document.");		
			e.printStackTrace();
		}

		this.isDigested = false;

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time for importing proteins: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");		
		System.out.println("Proteins added to protein library: "+(proteins.size()-proteinCount));
		System.out.println("Proteins stored in protein library: "+proteins.size());

	}

	/**
	 * Getter for protein.
	 * @param accession
	 * @return protein with the given accession.
	 */
	public Protein getProtein(String accession) {
		return this.proteins.get(accession);
	}

	/**
	 * Method to remove protein duplicates from ProteinLib
	 * 
	 * @author Felix Hartkopf
	 */
	public void removeDuplicates(){
		Set<String> keys = this.proteins.keySet();
		for(String key : keys) {
			Protein protein = proteins.get(key);
			for(String otherKey : keys) {
				if(key!=otherKey){
					if(protein.equals(proteins.get(otherKey))) {
						proteins.remove(otherKey);
					}
				}	
			}
		}
		this.isDigested = false;
	}



	public String toString() {
		String output = "\n#########################";
		output = output+"\n# ProteinLib Properties #";
		output = output+"\n#########################\n";
		output = output+"minLength:\t"+this.minLength+"\n";
		output = output+"maxLength:\t"+this.maxLength+"\n";
		output = output+"missedCleavages:\t"+this.missedCleavages+"\n";
		output = output+"databases:\t"+"\n";
		for(int i = 0; i < this.databases.size(); i++) {
			output = output+i+"\t"+this.databases.get(i)+"\n";
		}
		output = output+"proteins:\t"+this.proteins.size()+"\n";
		output = output+"peptides:\t"+this.peptides.size()+"\n";
		output = output+"isDigested:\t"+this.isDigested+"\n\n";

		return output;		
	}




	////////////////////
	// Static Methods //
	////////////////////
}

