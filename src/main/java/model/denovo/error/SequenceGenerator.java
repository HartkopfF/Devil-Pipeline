package model.denovo.error;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

public class SequenceGenerator {

	///////////////
	// Variables //
	///////////////

	/**
	 * Original peptide de novo sequence.
	 */
	private String denovoSequence;

	/**
	 * List of generated sequences.
	 */
	private List<String> generatedSequences;


	/////////////////
	// Constructor //
	/////////////////

	public SequenceGenerator(String denovoSequence) {
		this.denovoSequence = denovoSequence;
		this.scanSequence();
	}

	/////////////////////////////
	// Getter & Setter Methods //
	/////////////////////////////

	/**
	 * Returns a list of generated sequences. 
	 * @return List of generated sequences.
	 */
	public List<String> getGeneratedSequences() {
		return generatedSequences;
	}

	///////////////////
	// Class Methods //
	///////////////////

	/**
	 * Scans the de novo sequence and checks whether it contains certain amino acids or di-peptides that serve as potential error candidates. 
	 */
	private void scanSequence() {
		generatedSequences = new ArrayList<String>();

		if (denovoSequence.contains("I")) {
			String newSequence = denovoSequence.replaceAll("I", "L");
			generatedSequences.add(newSequence);
		} 
		if (denovoSequence.contains("L")) {
			String newSequence = denovoSequence.replaceAll("L", "I");
			generatedSequences.add(newSequence);
		} 

		if (denovoSequence.contains("Q")) {
			generatedSequences.addAll(generateCandidates('Q', "K"));
			generatedSequences.addAll(generateCandidates('Q', "GA"));
		} 
		if (denovoSequence.contains("K")) {
			generatedSequences.addAll(generateCandidates('K', "Q"));
			generatedSequences.addAll(generateCandidates('K', "GA"));
		}

		if (denovoSequence.contains("R")) {
			generatedSequences.addAll(generateCandidates('R', "GV"));
		}

		if (denovoSequence.contains("N")) {
			generatedSequences.addAll(generateCandidates('N', "GG"));
		}

		if (denovoSequence.contains("W")) {
			generatedSequences.addAll(generateCandidates('W', "GE"));
			generatedSequences.addAll(generateCandidates('W', "DA"));
			generatedSequences.addAll(generateCandidates('W', "SV"));
		}

		if (denovoSequence.contains("AS")) {
			String newSequence = denovoSequence.replaceAll("AS", "TG");
			String newSequence2 = denovoSequence.replaceAll("AS", "GT");
			generatedSequences.add(newSequence);
			generatedSequences.add(newSequence2);
		}

		if (denovoSequence.contains("GG")) {
			String newSequence = denovoSequence.replaceAll("GG", "N");
			generatedSequences.add(newSequence);
		}
		if (denovoSequence.contains("GA")) {
			String newSequence = denovoSequence.replaceAll("GA", "Q");
			generatedSequences.add(newSequence);
		}
		if (denovoSequence.contains("GA")) {
			String newSequence = denovoSequence.replaceAll("GA", "K");
			generatedSequences.add(newSequence);
		}
	}

	/**
	 * Generates a list of sequences in which the source amino acid is alternately replaced by the target amino acid.
	 * @param source Source amino acid
	 * @param target Target amino acid
	 * @return List of generated sequence candidates
	 */
	public List<String> generateCandidates(Character source, String target) {
		int nTotalOcc = StringUtils.countMatches(denovoSequence, source.toString());

		// Candidate list.
		List<String> candidates = new ArrayList<>();

		int occ = 0;
		while (occ < nTotalOcc) {
			int count = 0;
			String candidate = "";
			for (int i = 0; i < denovoSequence.length(); i++) {
				if (denovoSequence.charAt(i) == source) {
					if (count == occ) {
						candidate += target;
					} else {
						candidate += source;
					}
					count++;
				} else {
					candidate += denovoSequence.charAt(i);
				}
			}
			candidates.add(candidate);
			occ++;
		}
		return candidates;
	}
}
