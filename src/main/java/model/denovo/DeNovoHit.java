package model.denovo;

public interface DeNovoHit {

	///////////////////////
	// Interface Methods //
	///////////////////////

	public int getSpectrumId();
	public int getCharge();
	public String getSequence();
	public String getAaScore();
	public double getMz();
}
