package io.parser;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import model.hit.PSM;
import model.hit.ProteinHit;

public class IdParser {

	////////////////////
	// Static Methods //
	////////////////////
	/**
	 * Parses database identifications and returns a mapping for scan numbers to peptide sequences.
	 * @param filePath
	 * @return
	 * @throws IOException
	 */
	public static Map<Integer, String> parsePeptideIdsDataSet3(String filePath) throws IOException {
		Map<Integer, String> ids = new HashMap<Integer, String>();
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		String line = null;
		while ((line = br.readLine()) != null) {
			if (!line.startsWith("Group")) {
				String[] split = line.split(";");
				try {
					int scanNumber = Integer.valueOf(split[21]);
					String sequence = split[23];
					ids.put(scanNumber, sequence);
				} catch (NumberFormatException ex) {
					int scanNumber = Integer.valueOf(split[23]);
					String sequence = split[25];
					ids.put(scanNumber, sequence);
				}

			}
		}
		br.close();

		return ids;
	}

	public static Map<String, String> parsePeptideIdsMarxDataSet(String filePath) throws IOException {
		Map<String, String> ids = new HashMap<String, String>();
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		String line = null;
		while ((line = br.readLine()) != null) {
			if (!line.startsWith("Raw")) {
				String[] split = line.split("\t");
				String rawAndScanNumber = split[0] + "_" + split[2];
				String sequence = split[5];
				ids.put(rawAndScanNumber, sequence);
			}
		}
		br.close();

		return ids;
	}

	public static Map<Integer, String> parseMGSFOutput(String filePath) throws IOException {
		Map<Integer, String> ids = new HashMap<Integer, String>();
		BufferedReader br = new BufferedReader(new FileReader(filePath));
		String line = null;
		while ((line = br.readLine()) != null) {
			if (!line.startsWith("#SpecFile")) {
				String[] split = line.split("\t");
				int index = Integer.valueOf(split[1].substring(6));
				String sequence = split[9].replaceAll("\\+57.021", "");
				ids.put(index, sequence);
			}
		}		
		br.close();
		return ids;
	}

	public static List<PSM> parseMSGFToPSMs(String filePath) throws IOException {
		List<PSM> psms = new ArrayList<PSM>();

		BufferedReader br = new BufferedReader(new FileReader(filePath));
		String line = null;
		while ((line = br.readLine()) != null) {
			if (!line.startsWith("#SpecFile")) {
				String[] split = line.split("\t");

				int index = Integer.valueOf(split[1].substring(6));
				Set<String> proteins = new HashSet<String>();

				String[] temp = split[10].split(";");
				for (String string : temp) {
					String[] split2 = ProteinHit.descriptionDecoder(string);
					proteins.add(split2[0]);
				}
				// PSM object
				PSM psm = new PSM(index, split[3], Double.valueOf(split[5]), Integer.valueOf(split[8]), split[9].replaceAll("\\+57.021", ""), proteins, Float.valueOf(split[11]), false);
				psm.setQvalue(Float.valueOf(split[15]));
				psms.add(psm);
			}
		}		
		br.close();
		return psms;
	}

	public static List<PSM> parseIDsToPSMs(String filePath) throws IOException {
		List<PSM> psms = new ArrayList<PSM>();

		BufferedReader br = new BufferedReader(new FileReader(filePath));
		String line = null;
		while ((line = br.readLine()) != null) {
			if (!line.startsWith("#ID")) {
				String[] split = line.split("\t");

				int spectrumId = Integer.valueOf(split[1]);
				Set<String> proteins = new HashSet<String>();

				String[] temp = split[6].split(";");
				for (String string : temp) {
					proteins.add(string);
				}
				// PSM object
				PSM psm = new PSM(spectrumId, split[2], Double.valueOf(split[3]), Integer.valueOf(split[4]), split[5], proteins, Float.valueOf(split[7]), false);
				psms.add(psm);
			}
		}		
		br.close();
		return psms;
	}
}
