package execute;

import java.io.IOException;

import model.ProteinLib;

public class DatabaseDigester {

	public static void main(String[] args) {
		// Read terminal input
		String primaryDB = args[0];
		double mutationsrate = Double.parseDouble(args[1]);
		// Read fasta database
		ProteinLib proteinLib;
		try {
			String output = args[2];
			proteinLib = new ProteinLib(primaryDB);
			proteinLib.digestProteins();
			System.out.print(proteinLib);
			proteinLib.writeMS2PIP(output);
			proteinLib.inSilicoPeptideMutation(mutationsrate);
			proteinLib.inSilicoPeptideMutation(mutationsrate);
			proteinLib.writeMS2PIP(output.replaceAll(".peprec", "_mutated_"+mutationsrate+".peprec"));
		} catch (IOException e) {
			System.out.println("Please specify fasta database to digest...");
			e.printStackTrace();
		}

	}

}
