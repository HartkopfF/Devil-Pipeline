package execute;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import org.mapdb.DB;
import org.mapdb.DBMaker;
import org.mapdb.HTreeMap;
import org.mapdb.Serializer;

import model.Modification;
import model.Mutation;
import model.ProteinLib;
import model.denovo.NovorHit;
import model.hit.ProteinHit;
import model.tag.Tag;
import model.tag.TagGenerator;
import model.tag.TagIndex;
import model.tag.TagSerializer;
import util.NovorProcess;
import util.StringUtilities;

public class DeNovoTerminal {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) throws IOException, StringIndexOutOfBoundsException, InterruptedException {

		//////////////////////
		// Load config file //
		//////////////////////
		System.out.println("Loading config file...");

		// Read terminal input
		String mgfFileName = args[0];
		String fastaDataBase = args[1];
		String outputFileName = args[2];

		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("devil.config");
		config.load(file);

		// Peformance
		boolean saveMemory = Boolean.parseBoolean(config.getProperty("saveMemory"));
		int cores = Integer.parseInt(config.getProperty("cores"));

		// Novor
		double tolerance = Double.parseDouble(config.getProperty("tolerance"));
		String NovorPath = config.getProperty("Novor");
		double meanAaScore = Double.parseDouble(config.getProperty("meanAaScore"));
		double maxAbsError = Double.parseDouble(config.getProperty("maxAbsError"));

		// Databases paths
		String humanPath = config.getProperty("humanPath");
		String allVirusPath = config.getProperty("allVirusPath");
		String cRAPPath = config.getProperty("cRAPPath");

		// Databases 
		boolean humanDB = Boolean.parseBoolean(config.getProperty("human"));
		boolean allVirusDB = Boolean.parseBoolean(config.getProperty("allVirus"));
		boolean cRAPDB = Boolean.parseBoolean(config.getProperty("cRAP"));

		// Mutations and modifications
		boolean mutations = Boolean.parseBoolean(config.getProperty("mutations"));
		boolean modifications = Boolean.parseBoolean(config.getProperty("modifications"));

		/////////////
		// De Novo //
		/////////////

		// start record time
		long startTime = System.currentTimeMillis();

		// Execute Novor
		NovorProcess novorProcess = new NovorProcess(mgfFileName,NovorPath+"/lib",outputFileName,NovorPath+"/params.txt");
		novorProcess.run();

		// Import Novor results
		System.out.println("Importing Novor results...");
		Map<Integer, NovorHit> novorReader = NovorHit.readNovorHitsFromFile("output/Novor/"+outputFileName+".csv",mgfFileName);

		// Init database with MapDB
		File DBfile = new File("spectraDB.db");
		DBfile.delete();
		DB db;

		// Create Heap in DB
		Serializer<Tag> serializer = new TagSerializer();

		if(saveMemory) {
			db = DBMaker.fileDB(DBfile)
					.allocateStartSize( 10 * 1024*1024*1024)  // 10GB
					.allocateIncrement(512 * 1024*1024)       // 512MB   
					.fileChannelEnable()
					.make();			
		}else {
			db = DBMaker.heapDB()
					.make();			
		}

		// Generate Tags
		HTreeMap<Integer, Tag> tags = (HTreeMap<Integer,Tag>) db.hashMap("tags_raw").valueSerializer(serializer).createOrOpen();
		TagGenerator.generateTagsForList(novorReader,tags, db,cores);

		// Read fasta database
		ProteinLib proteinLib = new ProteinLib(fastaDataBase);
		if(humanDB){
			proteinLib.readFastaFile(humanPath);
		}
		if(cRAPDB){
			proteinLib.readFastaFile(cRAPPath);
		}
		if(allVirusDB){
			proteinLib.readFastaFile(allVirusPath);
		}
		proteinLib.digestProteins();
		System.out.print(proteinLib);

		// Build tag index
		HTreeMap<Integer, List<Integer>> tagIndex = (HTreeMap<Integer, List<Integer>>) db.hashMap("index").createOrOpen();
		TagIndex tagIndexer = new TagIndex();
		tagIndexer.buildIndex(db, tagIndex, proteinLib, cores);

		HTreeMap<Integer, Tag> tagList = (HTreeMap<Integer,Tag>) db.hashMap("tags").valueSerializer(serializer).createOrOpen();

		// Match tags to peptides 
		Tag.matchTagsToProteins(db, tagList, tags, proteinLib, tagIndex, tolerance, meanAaScore, cores);	

		// Mutations
		if(mutations) {
			tagList = Mutation.mutationsOfMatchedTags(tagList, proteinLib);
		}

		// Modifications
		if(modifications){
			tagList = Modification.getModificationForMatchedTags(tagList, proteinLib, db, cores);
		}

		// Extract perfect matches
		HTreeMap<Integer, Tag> perfectMatches = (HTreeMap<Integer,Tag>) db.hashMap("perfectMatches").valueSerializer(serializer).createOrOpen();
		HTreeMap<Integer, Tag> weakMatches = (HTreeMap<Integer,Tag>) db.hashMap("weakMatches").valueSerializer(serializer).createOrOpen();
		Tag.extractPerfectMatches(tagList, proteinLib, db, maxAbsError, perfectMatches, weakMatches, cores);
		db.commit();

		List<ProteinHit> perfectProteins = Tag.linkTagsToProteins(perfectMatches, proteinLib);
		ProteinHit.proteinListCoverage(perfectProteins, proteinLib);

		// Export
		Tag.exportTagToTSV(perfectMatches, outputFileName+"_perfect_tags_collapsed.tsv", proteinLib);
		Tag.exportTagToTSV(weakMatches, outputFileName+"_weak_tags_collapsed.tsv", proteinLib);

		Tag.exportFullTSV(perfectMatches, outputFileName+"_perfect_all_tags_.tsv", proteinLib);
		Tag.exportFullTSV(weakMatches, outputFileName+"_weak_all_tags_.tsv", proteinLib);

		ProteinHit.exportProteinsToTSV(perfectProteins,"output/ProteinHits/"+outputFileName+"_Proteins.tsv");

		db.close();

		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time elapsed: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");

		long total = Runtime.getRuntime().totalMemory();
		long used  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Total memory: "+total/1024/1024+" MB");
		System.out.println("Used memory: "+used/1024/1024+" MB");
	}



}

