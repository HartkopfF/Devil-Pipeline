package execute;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.List;
import java.util.Properties;

import io.parser.IdParser;
import model.ProteinLib;
import model.hit.PSM;
import model.hit.PeptideHit;
import model.hit.ProteinHit;
import python.PythonProcess;
import util.ResourcesConstants;
import util.MSGFplusProcess;
import util.StringUtilities;

public class DatabaseTerminal {


	public static void main(String[] args) throws IOException{		

		//////////////////////
		// Load config file //
		//////////////////////

		// Read terminal input
		String primaryVirus = args[0];
		String primaryVirusDB = args[1];
		String output = args[2];


		// Read config file
		Properties config = new Properties();
		Reader file = new FileReader("devil.config");
		config.load(file);

		// MSGFplus
		double fdr = Double.parseDouble(config.getProperty("fdr"));
		String MSGFPlusPath = config.getProperty("MSGFPlus");

		// Databases paths
		String humanPath = config.getProperty("humanPath");
		String allVirusPath = config.getProperty("allVirusPath");
		String cRAPPath = config.getProperty("cRAPPath");

		// Databases 
		boolean humanDB = Boolean.parseBoolean(config.getProperty("human"));
		boolean allVirusDB = Boolean.parseBoolean(config.getProperty("allVirus"));
		boolean cRAPDB = Boolean.parseBoolean(config.getProperty("cRAP"));

		// Overwrite cRAP species
		boolean cRAPOverwrite = Boolean.parseBoolean(config.getProperty("cRAPOverwrite"));

		// Overwrite all virus species
		boolean allVirusOverwrite = Boolean.parseBoolean(config.getProperty("allVirusOverwrite"));

		///////////////////////////////
		// Preprocessing of Virus DB //
		///////////////////////////////

		ProteinLib proteinLib_virus = new ProteinLib(primaryVirusDB);
		proteinLib_virus.removeDuplicates();
		proteinLib_virus.writeFasta(primaryVirusDB+"noDub");
		System.out.println("Proteins with duplicates: "+proteinLib_virus.proteins.size());
		primaryVirusDB = primaryVirusDB.replace(".fasta","_noDup.fasta");
		///////////////////////////
		// Link PSMs to Peptides //
		///////////////////////////

		// start record time
		long startTime = System.currentTimeMillis();

		System.out.println("###############################");
		System.out.println("# Processing primary virus... #");
		System.out.println("###############################\n");

		MSGFplusProcess virus = new MSGFplusProcess(primaryVirusDB,primaryVirus,MSGFPlusPath,"virus_"+output);
		virus.run();

		System.out.println("Please wait... Importing MSGFplus results... ");
		List<PSM> msgfPSMs = IdParser.parseMSGFToPSMs("output/MSGFplus/virus_"+output+".tsv");

		System.out.println("Please wait... Linking PSMs to Peptides...");
		List<PeptideHit> peptideList = PeptideHit.linkPSMtoPeptide(msgfPSMs, fdr);

		System.out.println("Please wait... Linking Peptides to Proteins... ");
		List<ProteinHit> proteinList = ProteinHit.linkPeptideHitsToProteinHits( peptideList,primaryVirusDB);

		//ProteinHit.proteinListCoverage(proteinList);

		if(humanDB){

			System.out.println("################################");
			System.out.println("# Processing human proteins... #");
			System.out.println("################################\n");

			ProteinLib proteinLib_human = new ProteinLib(humanPath);
			proteinLib_human.removeDuplicates();
			proteinLib_human.writeFasta(humanPath+"noDub");
			System.out.println("Proteins with duplicates: "+proteinLib_human.proteins.size());
			humanPath = humanPath.replace(".fasta","_noDup.fasta");

			MSGFplusProcess human = new MSGFplusProcess(humanPath,primaryVirus,MSGFPlusPath,"human_"+output);
			human.run();

			System.out.println("Please wait... Importing MSGFplus results... ");
			List<PSM> humanmsgfPSMs = IdParser.parseMSGFToPSMs("output/MSGFplus/human_"+output+".tsv");

			System.out.println("Please wait... Linking PSMs to Peptides... ");
			List<PeptideHit> humanpeptideList = PeptideHit.linkPSMtoPeptide(humanmsgfPSMs, fdr);

			System.out.println("Please wait... Linking Peptides to Proteins... ");
			List<ProteinHit> humanproteinList = ProteinHit.linkPeptideHitsToProteinHits( humanpeptideList,humanPath);

			//ProteinHit.proteinListCoverage(humanproteinList);

			proteinList.addAll(humanproteinList);

		}

		if(cRAPDB){

			System.out.println("######################");
			System.out.println("# Processing cRAP... #");
			System.out.println("######################\n");

			ProteinLib proteinLib_cRAP = new ProteinLib(cRAPPath);
			proteinLib_cRAP.removeDuplicates();
			proteinLib_cRAP.writeFasta(cRAPPath+"noDub");
			System.out.println("Proteins with duplicates: "+proteinLib_cRAP.proteins.size());
			cRAPPath = cRAPPath.replace(".fasta","_noDup.fasta");

			MSGFplusProcess cRAP = new MSGFplusProcess(cRAPPath,primaryVirus,MSGFPlusPath,"cRAP_"+output);
			cRAP.run();

			System.out.println("Please wait... Importing MSGFplus results... ");
			List<PSM> crapmsgfPSMs = IdParser.parseMSGFToPSMs("output/MSGFplus/cRAP_"+output+".tsv");

			System.out.println("Please wait... Linking PSMs to Peptides... ");
			List<PeptideHit> cRAPpeptideList = PeptideHit.linkPSMtoPeptide(crapmsgfPSMs, fdr);

			System.out.println("Please wait... Linking Peptides to Proteins... ");
			List<ProteinHit> cRAPproteinList = ProteinHit.linkPeptideHitsToProteinHits( cRAPpeptideList,cRAPPath);

			if(cRAPOverwrite){
				ProteinHit.unifySpecies(cRAPproteinList, "cRAP");
			}

			//ProteinHit.proteinListCoverage(cRAPproteinList);

			proteinList.addAll(cRAPproteinList);

		}

		if(allVirusDB){

			System.out.println("###################################");
			System.out.println("# Processing all human viruses... #");
			System.out.println("###################################\n");

			ProteinLib proteinLib_allVirus = new ProteinLib(allVirusPath);
			proteinLib_allVirus.removeDuplicates();
			proteinLib_allVirus.writeFasta(allVirusPath+"noDub");
			System.out.println("Proteins with duplicates: "+proteinLib_allVirus.proteins.size());
			allVirusPath = allVirusPath.replace(".fasta","_noDup.fasta");

			MSGFplusProcess allVirus = new MSGFplusProcess(allVirusPath,primaryVirus,MSGFPlusPath,"allVirus_"+output);
			allVirus.run();

			System.out.println("Please wait... Importing MSGFplus results... "+allVirusPath);
			List<PSM> allVirusmsgfPSMs = IdParser.parseMSGFToPSMs("output/MSGFplus/allVirus_"+output+".tsv");

			System.out.println("Please wait... Linking PSMs to Peptides... "+allVirusPath);
			List<PeptideHit> allViruspeptideList = PeptideHit.linkPSMtoPeptide(allVirusmsgfPSMs, fdr);

			System.out.println("Please wait... Linking Peptides to Proteins... "+allVirusPath);
			List<ProteinHit> allVirusproteinList = ProteinHit.linkPeptideHitsToProteinHits( allViruspeptideList,allVirusPath);

			if(allVirusOverwrite){
				ProteinHit.unifySpecies(allVirusproteinList, "all human virus");
			}

			//ProteinHit.proteinListCoverage(allVirusproteinList);

			proteinList.addAll(allVirusproteinList);



		}


		////////////
		// Export //
		////////////

		// export to tsv file
		ProteinHit.exportProteinsToTSV(proteinList,"output/ProteinHits/virus_"+output+".tsv");


		//Write summary
		PythonProcess python = new PythonProcess(ResourcesConstants.PLOT_SUMMARY);
		python.run();



		// debug output
		long stopTime = System.currentTimeMillis();
		long elapsedTime = stopTime - startTime;

		System.out.println("Total time elapsed: "+StringUtilities.millisToShortDHMS(elapsedTime)+" seconds");
		if(proteinList.size()>0){
			System.out.println("Time per peptide: "+StringUtilities.millisToShortDHMS(elapsedTime/proteinList.size())+" milliseconds");
			System.out.println("Time per protein: "+StringUtilities.millisToShortDHMS(elapsedTime/proteinList.size())+" milliseconds");
		}

		long total = Runtime.getRuntime().totalMemory();
		long used  = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
		System.out.println("Total memory: "+total+" MB");
		System.out.println("Used memory: "+used+" MB");

	}



}
