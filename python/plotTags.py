import os 
import numpy as np
import plotly 
import plotly.plotly as py
from plotly.graph_objs import *
import plotly.graph_objs as go


plotly.tools.set_credentials_file(username='Bready', api_key='lq9dgn9vi5')
plotly.tools.set_config_file(world_readable=False, sharing='private')


###########
# Methods #
###########

# Reads data and stores it in a Array
def readTags(path):
    print("############################# \n# Reading tags from file... #\n#############################")
    print("Reading data in "+path)
    # Create and open file
    f = open(path, "r")
    # Split file at every new line
    lines = f.read().split("\n")
    # Empty list
    summary = []
    del lines[0]
    # Split every line at \t
    for line in lines:
        if line != "": 
            row = line.split("\t")
            summary = summary + [row]
    print("Reading successfull \n")        
    return(summary)

# Method to extract mass shift for left and right side
def createMassShiftVectors(data):
    print("################################ \n# Create mass shift vectors... #\n################################")
    left = []
    right = []
    for row in data:
        matchedSeq = row[9].split("|")
        for match in matchedSeq:
            if(len(match) > 0):
                match = match.split("_")[5]
                match = match.split(":")
                left = left + [match[0]]
                right = right + [match[1]]
    print("Created mass shift vectors successfull \n")        
    return(left,right)


# Create a histogram
def histogram(data,path):
    print("\t- histogram")
    
    
    outputFolder = path+"histogram/"
    
    if not os.path.exists(outputFolder):
        os.makedirs(outputFolder)
        
    leftMassShift = go.Histogram(
        x=data[0],
        histnorm='count',
        name='right mass shift',
        autobinx=False,
        xbins=dict(
            start=0,
            end=400,
            size=0.075
            ),
        marker=dict(
            color='red',
            line=dict(
                color='grey',
                width=0
            )
        ),
        opacity=0.75
    )
    rightMassShift = go.Histogram(
        x=data[1],
        name='left mass shift',
        autobinx=False,
        xbins=dict(
            start=0,
            end=400,
            size=0.05
        ),
        marker=dict(
        color='blue'
        ),
        opacity=0.75
    )
    data = [leftMassShift, rightMassShift]
    layout = go.Layout(
        title='Sampled Results',
        xaxis=dict(
            title='m/z'
        ),
        yaxis=dict(
            title='Count'
        ),
        barmode='overlay',
        bargap=0.25,
        bargroupgap=0.3
    )
    fig = go.Figure(data=data, layout=layout)
    plotly.offline.plot(fig,filename = outputFolder+"ADV_MassShift.html")
    return(0)

########    
# Main #
########
dir_path = os.path.dirname(os.path.realpath(__file__))

print(dir_path)
# Path of data
path = "../output/Tag/"

# Import data
data = readTags(path+"ADV_MassShift.tsv")

data = np.transpose(data)

# Create mass shift vectors
# data = createMassShiftVectors(data)

# Build histogram
histogram(data,path)


