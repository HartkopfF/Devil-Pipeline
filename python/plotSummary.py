import numpy as np
import matplotlib.pyplot as plt
import os
import csv
import sys
import ast
import math
import time

#########################
# Decrease maxInt value #
#########################

maxInt = sys.maxsize
decrement = True

while decrement:
	# decrease the maxInt value by factor 10 
	# as long as the OverflowError occurs.

	decrement = False
	try:
		csv.field_size_limit(maxInt)
	except OverflowError:
		maxInt = int(maxInt/10)
		decrement = True

###########
# Methods #
###########

# Method that return unique strings from list
def getUnique(seq):
	tmpSet = set(seq)
	return list(tmpSet)

# Writes summary of all result in ProteinHits directory 
def writeSummary(path):
	print("###################################### \n# Creating summary table for data... #\n######################################")

	files = os.listdir(path)
	print(files)
	if "summary.tsv" in files: files.remove("summary.tsv")
	if "barplots" in files: files.remove("barplots")
	print(files)

	
	summary = []
	row = 0
	
	for file in files:
		#Read
		print(path+file)
		with open(path+file, 'r') as csvfile:
			csvReader = csv.reader(csvfile, delimiter='\t', quoting=csv.QUOTE_NONE)
			next(csvfile)
			species = list()
			proteinCount = 0
			peptideCount = 0
			spectralCount = 0
			for row in csvReader:
				proteinCount += 1
				peptideCount = peptideCount + int(row[11])
				spectralCount = spectralCount + int(row[5])
				speciesString = row[3]
				species.append(speciesString)
		speciesCount = []
		uniqueSpecies = getUnique(species)
		for n in uniqueSpecies:
			speciesCount.append(species.count(n))
		summary.append([file,proteinCount,peptideCount,spectralCount,uniqueSpecies,speciesCount])
	
		#Write
		print("Writing summary to summary.tsv")
		with open(path+'summary.tsv', 'w') as csvOutput:
			csvWriter = csv.writer(csvOutput, delimiter='\t', lineterminator='\n')
			header = ["sampleID","#protein hits","#peptide hits","#spectral count","species","#species"]
			csvWriter.writerow(header)
			for row in summary:
				csvWriter.writerow(row)
	print("Summary.tsv successfull created in "+ path+"\n")
	return(uniqueSpecies)

# Reads summary of writeSummary() and stores it in a Array
def readSummary(path):
	print("###################################### \n# Reading summary table from file... #\n######################################")
	print("Reading summary.tsv in "+path)
	# Create and open file
	file = path+"summary.tsv"
	f = open(file, "r")
	# Split file at every new line
	lines = f.read().split("\n")
	# Empty list
	summary = []
	
	# Split every line at \t
	for line in lines:
		if line != "": 
			row = line.split("\t")
			summary = summary + [row]
	summary = speciesConverter(summary)
	print("Reading successfull \n")		
	return(summary)

# Method that converts a String representing a list in the type list
def speciesConverter(summary):
	for row in range(1,len(summary)):
		summary[row][4] = ast.literal_eval(summary[row][4])
		summary[row][5] = ast.literal_eval(summary[row][5])
	return(summary)

# Create a Bar chart of one line in the summary
def createBarChart(summaryRow,path):
	print("\t-Pie chart")
	# The slices will be ordered and plotted counter-clockwise.
	labels = summaryRow[4]
	sizes = summaryRow[5]
	sizesLog = [math.log(float(i)) for i in summaryRow[5]]
	n = len(labels)

	index = np.arange(n)
	bar_width = 0.5
	outputFolder = path+"barplots/"
	
	if not os.path.exists(outputFolder):
		os.makedirs(outputFolder)

	plt.figure(figsize=(16, 9), dpi=100)
	plt.bar(index, sizes, bar_width,color='r')
	plt.xlabel('Species')
	plt.ylabel('Frequency')
	plt.title('Frequency of species in sample '+summaryRow[0])
	plt.xticks(index + (bar_width/2), labels)
	plt.savefig(outputFolder+summaryRow[0][0:-4]+".png")
	
	
	plt.figure(figsize=(16, 9), dpi=100)
	plt.bar(index, sizesLog, bar_width,color='r')
	plt.xlabel('Species')
	plt.ylabel('Frequency_log')
	plt.title('Logarithsm frequency of species in sample '+summaryRow[0])
	plt.xticks(index + (bar_width/2), labels)
	plt.savefig(outputFolder+summaryRow[0][0:-4]+"_log.png")

	return(0)

def plotSummaryPerRow(summary,path):
	print("################################## \n# Creating plots for summary ... #\n##################################")
	for row in range(1,len(summary)):
		print("Creating plots for "+summary[row][0])
		createBarChart(summary[row],path)
	return(0)	
########	
# Main #
########

# Path of data
path = "output/ProteinHits/"

# Creating summary.tsv
uniqueSpecies = writeSummary(path)

# Reading summary.tsv
summary = readSummary(path)

# Plot pie chart of every dataset
plotSummaryPerRow(summary,path)